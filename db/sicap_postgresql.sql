/*
Se debe tener creada la base de datos en PostgreSQL para luego poder importar este archicvo. 
*/

DROP TABLE IF EXISTS departamento, fallas_empl, cargo_empl, empleado, usuario, horario, estacion, es, justificacion, feriados, asignacion, horario_departamento, observacion, fotos CASCADE;

DROP TYPE IF EXISTS tipo_empl,tipo_bono, tipo_mar, tipo_feriado, tipo_horario, tipo_es, tipo_asis, tipo_trabajo, tipo_justi, tipo_turno, estatus, contrato, tipo_dias, status_noti CASCADE;

CREATE TYPE tipo_asis AS ENUM('Si','No');

CREATE TYPE tipo_es as ENUM('Entrada','Salida');

CREATE TYPE tipo_horario AS ENUM('Normal','12x12','Pernocta','Otros');

CREATE TYPE tipo_dias AS ENUM('L a V','X a D','1D');

CREATE TYPE tipo_justi AS ENUM('Falta Justificada', 'Falta Injustificada','Observación','Salida antes de tiempo');

CREATE TYPE tipo_turno AS ENUM('Día','Noche','Libre');

CREATE TYPE estatus AS ENUM('Activo','Inactivo');

CREATE TYPE contrato AS ENUM('Fijo','Contratado');

CREATE TYPE tipo_bono AS ENUM('Si','No');


/*CREATE TYPE status_noti AS ENUM('Leido','No leido');*/

CREATE TYPE status_noti AS ENUM('Leido','No leido');


/*CREATE TYPE tipo_mar AS ENUM('Entrada','Salida');*/
/*CREATE TYPE tipo_feriado AS ENUM('Movible','FIjo') ;*/


CREATE TABLE horario(
  horario_id serial,
  hora_entrada TIME NOT NULL,
  hora_salida TIME NOT NULL,
  nombre VARCHAR(40) NOT NULL,
  descripcion TEXT NOT NULL,
  tipo tipo_horario NOT NULL,
  dias tipo_dias NULL, 
  PRIMARY KEY (horario_id)
);

CREATE TABLE departamento (
  departamento_id SERIAL,
  nombre VARCHAR(70) NOT NULL,
  supervisor VARCHAR(50) NULL,
  coordinador VARCHAR(50) NULL,
  descripcion TEXT NULL,
  PRIMARY KEY (departamento_id)
  );

CREATE TABLE cargo_empl (
  cargo_id SERIAL,
  cargo VARCHAR(50) NOT NULL,
  descripcion TEXT NULL,
  PRIMARY KEY (cargo_id)
);

CREATE TABLE estacion(
  estacion_id SERIAL,
  nombre VARCHAR(45) NOT NULL,
  altura VARCHAR(10) NULL,
  descripcion TEXT  NULL,
  PRIMARY KEY (estacion_id)
  );

CREATE TABLE empleado(
  empleado_id SERIAL,
  codigo_empl CHAR(10) NOT NULL,
  nombre VARCHAR(60) NOT NULL,
  cedula VARCHAR(11) NOT NULL,
  fecha_nac DATE NULL,
  estatus estatus NOT NULL,
  fecha_ingreso DATE NULL,
  tipo_contrato contrato NULL,
  imagen TEXT NULL,
  UNIQUE (codigo_empl, cedula),
  PRIMARY KEY (empleado_id),
  departamento_id smallint NOT NULL REFERENCES departamento(departamento_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  cargo_id smallint NOT NULL REFERENCES cargo_empl(cargo_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  estacion_id SMALLINT NULL REFERENCES estacion(estacion_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    );

CREATE TABLE es(
  es_id SERIAL,
  fecha DATE NOT NULL,
  hora TIME NOT NULL,
  dia VARCHAR(10) NOT NULL,
  tipo tipo_es NOT NULL,
  bono_noc tipo_bono NOT NULL,
  tiempo_extra DECIMAL(4,1) NOT NULL,
  tiempo_extra_noche DECIMAL(4,1) NOT NULL,
  PRIMARY KEY (es_id),
  UNIQUE (fecha,empleado_id,hora),
  empleado_id SMALLINT NOT NULL REFERENCES empleado (empleado_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE TABLE usuario(
  usuario_id SERIAL,
  usuario VARCHAR(20) NOT NULL,
  contrasena VARCHAR(50) NOT NULL,
  email VARCHAR(45) NOT NULL,
  niveles char(2),
  PRIMARY KEY (usuario_id),
  UNIQUE (usuario),
  UNIQUE (email),
  empleado_id smallint NOT NULL REFERENCES empleado(empleado_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    );

CREATE TABLE justificacion(
  codigo_justi SERIAL,
  descripcion TEXT NOT NULL,
  tipo tipo_justi NOT NULL,
  asistencia tipo_asis NULL,
  fecha DATE NOT NULL,
  nombre_supervisor VARCHAR(60) NOT NULL,
  PRIMARY KEY (codigo_justi),
  empleado_id SMALLINT NULL REFERENCES empleado (empleado_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE TABLE feriados(
  feriado_id SERIAL,
  dia CHAR(2) NOT NULL,
  mes CHAR(2) NOT NULL,
  ano CHAR(4) NULL,
  descripcion TEXT NOT NULL,
  PRIMARY KEY (feriado_id));

CREATE TABLE asignacion(
  asignacion_id SERIAL,
  fecha DATE NOT NULL,
  pernocta tipo_asis NULL,
  trabajo tipo_asis NOT NULL,
  dia VARCHAR(15) NULL,
  turno tipo_turno NOT NULL,
  PRIMARY KEY (asignacion_id),
  UNIQUE(fecha,empleado_id),
  empleado_id SMALLINT NOT NULL REFERENCES empleado (empleado_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  horario_id SMALLINT NOT NULL REFERENCES horario (horario_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    );

CREATE TABLE horario_departamento(
  horario_id SMALLINT NOT NULL REFERENCES horario(horario_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  departamento_id SMALLINT NOT NULL REFERENCES departamento(departamento_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  PRIMARY KEY (horario_id, departamento_id)
);

CREATE TABLE fallas_empl(
  falla_id SERIAL,
  fecha DATE NOT NULL,
  hora TIME NOT NULL,
  dia VARCHAR(10) NOT NULL,
  tipo tipo_es NOT NULL,
  descripcion TEXT NULL,
  status status_noti NOT NULL,
  status_supervisor status_noti NOT NULL,
  PRIMARY KEY (falla_id),
  UNIQUE (fecha,empleado_id,hora),
  empleado_id SMALLINT NOT NULL REFERENCES empleado (empleado_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE

    );
CREATE TABLE fotos(
  id_fotos SERIAL,
  foto VARCHAR NOT NULL,  /* foto BYTEA NOT NULL*/
  PRIMARY KEY (id_fotos),
  es_id SMALLINT NOT NULL REFERENCES es (es_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    );