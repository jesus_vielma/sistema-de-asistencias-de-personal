-- -----------------------------------------------------
-- Schema sicap
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `sicap` ;

-- -----------------------------------------------------
-- Schema sicap
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sicap` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `sicap` ;

-- -----------------------------------------------------
-- Table `sicap`.`departamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`departamento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `supervisor` VARCHAR(50) NULL,
  `coordinador` VARCHAR(50) NULL,
  `descripcion` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sicap`.`cargo_empl`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`cargo_empl` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cargo` VARCHAR(50) NULL,
  `descripcion` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sicap`.`estacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`estacion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `altura` VARCHAR(10) NULL,
  `descripcion` TEXT   NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sicap`.`empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`empleado` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `codigo_empl` CHAR(10) NOT NULL,
  `nombre` VARCHAR(60) NOT NULL,
  `cedula` VARCHAR(11) NOT NULL,
  `fecha_nac` DATE NOT NULL,
  `departamento_id` INT NOT NULL,
  `cargo_id` INT NOT NULL,
  `estacion_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `codigo_empl_UNIQUE` (`codigo_empl` ASC),
  INDEX `fk_empleado_departamento1_idx` (`departamento_id` ASC),
  INDEX `fk_empleado_cargo1_idx` (`cargo_id` ASC),
  INDEX `fk_empleado_estacion1_idx` (`estacion_id` ASC),
  UNIQUE INDEX `cedula_UNIQUE` (`cedula` ASC),
  CONSTRAINT `fk_empleado_departamento1`
    FOREIGN KEY (`departamento_id`)
    REFERENCES `sicap`.`departamento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_empleado_cargo1`
    FOREIGN KEY (`cargo_id`)
    REFERENCES `sicap`.`cargo_empl` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_empleado_estacion1`
    FOREIGN KEY (`estacion_id`)
    REFERENCES `sicap`.`estacion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sicap`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`usuario` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `usuario` VARCHAR(20) NOT NULL,
  `contraseña` VARCHAR(50) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `empleado_id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `usuario_UNIQUE` (`usuario` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_usuario_empleado_idx` (`empleado_id` ASC),
  CONSTRAINT `fk_usuario_empleado`
    FOREIGN KEY (`empleado_id`)
    REFERENCES `sicap`.`empleado` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sicap`.`horario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`horario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `hora_entrada` TIME NOT NULL,
  `hora_salida` TIME NOT NULL,
  `descripcion` TEXT NOT NULL,
  `tipo` ENUM('Normal','12x12','Pernocta') NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sicap`.`es`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`es` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATE NOT NULL,
  `hora` TIME NOT NULL,
  `dia` VARCHAR(10) NOT NULL,
  `tipo` ENUM('Entrada','Salida') NOT NULL,
  `tiempo_extra` VARCHAR(10) NOT NULL,
  `empleado_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `fecha_UNIQUE` (`fecha` ASC),
  INDEX `fk_es_empleado1_idx` (`empleado_id` ASC),
  UNIQUE INDEX `hora_UNIQUE` (`hora` ASC),
  UNIQUE INDEX `empleado_id_UNIQUE` (`empleado_id` ASC),
  CONSTRAINT `fk_es_empleado1`
    FOREIGN KEY (`empleado_id`)
    REFERENCES `sicap`.`empleado` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sicap`.`justificacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`justificacion` (
  `codigo_justi` INT NOT NULL AUTO_INCREMENT,
  `descripcion` TEXT NOT NULL,
  `tipo` ENUM('Falta Justificada', 'Falta Injustificada','Observación','Salida antes de tiempo') NOT NULL,
  `asistencia` ENUM('Si','No') NULL,
  `fecha` DATE NOT NULL,
  `empleado_id` INT NOT NULL,
  PRIMARY KEY (`codigo_justi`),
  INDEX `fk_observacion_empleado1_idx` (`empleado_id` ASC),
  CONSTRAINT `fk_observacion_empleado1`
    FOREIGN KEY (`empleado_id`)
    REFERENCES `sicap`.`empleado` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sicap`.`feriados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`feriados` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `dia` VARCHAR(3) NOT NULL,
  `mes` VARCHAR(3) NOT NULL,
  `año` YEAR NULL,
  `descripcion` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sicap`.`asignacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`asignacion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATE NOT NULL,
  `pernocta` ENUM('Si','No') NULL,
  `trabajo` ENUM('Si','No') NOT NULL,
  `dia` VARCHAR(15) NOT NULL,
  `turno` ENUM('Día','Noche','Libre') NOT NULL,
  `empleado_id` INT NOT NULL,
  `horario_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_asignacion_horario1_idx` (`horario_id` ASC),
  INDEX `fk_asignacion_empleado1_idx` (`empleado_id` ASC),
  CONSTRAINT `fk_asignacion_horario1`
    FOREIGN KEY (`horario_id`)
    REFERENCES `sicap`.`horario` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_asignacion_empleado1`
    FOREIGN KEY (`empleado_id`)
    REFERENCES `sicap`.`empleado` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sicap`.`horario_departamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sicap`.`horario_departamento` (
  `horario_id` INT NOT NULL,
  `departamento_id` INT NOT NULL,
  PRIMARY KEY (`horario_id`, `departamento_id`),
  INDEX `fk_horario_has_departamento_departamento1_idx` (`departamento_id` ASC),
  INDEX `fk_horario_has_departamento_horario1_idx` (`horario_id` ASC),
  CONSTRAINT `fk_horario_has_departamento_horario1`
    FOREIGN KEY (`horario_id`)
    REFERENCES `sicap`.`horario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_horario_has_departamento_departamento1`
    FOREIGN KEY (`departamento_id`)
    REFERENCES `sicap`.`departamento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;