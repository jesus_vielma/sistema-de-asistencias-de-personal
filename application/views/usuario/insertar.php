    <?php
$selec_empleados = array(''=>'- Seleccione -');
    foreach($Empleados as $empleado)
    {
        $selec_empleados[$empleado->empleado_id] = $empleado->nombre;
    }

?>
    
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Nuevo usuario</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('cargo')?>">Usuario</a>
                        </li>
                        <li class="active">
                            <strong>Crear Usuario</strong>
                        </li>
                    </ol>
                </div>
                <!--                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div> -->
            </div>

            <div class="wrapper wrapper-content">
                
                <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3>Insertar usuario</h3>
                    </div>
                    
                    <div class="panel-body">
                    
                    <?=form_open('usuario/insertar/2',array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>
                    <?=validation_errors();?>
                        <div class="form-group">
                            <?=form_label('Usuario:','usuario')?>
                            <?=form_input(array('id'=>'usuario','name'=>'usuario','class'=>'form-control','value'=>set_value('usuario'),'required'=>'required'))?>
                            <span class"help-block">El usuario debe tener entre 5 y 19 caracteres</span>
                        </div>
                        
                        <div class="form-group">
                            <?=form_label('Contraseña:','contrasena')?>
                            <?=form_password(array('id'=>'contrasena','name'=>'contrasena','class'=>'form-control','value'=>set_value('contrasena'),'required'=>'required'))?>
                        </div>
                        
                        <div class="form-group">
                            <?=form_label('Repita contraseña:','confcontrasena')?>
                            <?=form_password(array('id'=>'confcontrasena','name'=>'confcontrasena','class'=>'form-control','value'=>set_value('confcontrasena'),'required'=>'required'))?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Email:','email')?>
                            <?=form_input(array('id'=>'email','name'=>'email','value'=>set_value('email'),'class'=>'form-control','value'=>set_value('email'),'required'=>'required'))?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Niveles:','niveles')?>
                            <?=form_dropdown('niveles',array(''=>'- Seleccione -','1'=>'Coordinador, Supervisa toda la documentación inherente de los empleados de la empresa.','2'=>'Supervisor; posee acceso a los horarios, justificativos, controles de asistencia, reportes entre otros de los trabajadores de su departamneto a cargo.','3'=>'Encargado de que el sistema funcione correctamente y de crear nuevos usuarios para el mismo.'), set_value('empleado_id'),'class="form-control" id="empleado" required="required"')?>
                        </div>
 
                        
                        <div class="form-group">
                            <?=form_label('Empleado:','empleado_id')?>
                            <?=form_dropdown('empleado_id',$selec_empleados, set_value('empleado_id'),'class="form-control" id="empleado" required="required"')?>              
                        </div>
                        
 
                       
 
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3">
								
                                <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>
            
