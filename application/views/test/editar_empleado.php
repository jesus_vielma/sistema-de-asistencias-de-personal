<?php
$select_cargos = array(''=>'- Seleccione -');
foreach($cargos as $cargo)
{
    $select_cargos[$cargo->cargo_id] = $cargo->cargo;
}

$S_departamentos = array(''=>'- Seleccione -');
foreach($departamentos as $departamento)
{
    $S_departamentos[$departamento->departamento_id] = $departamento->nombre;
}

$S_estaciones = array(
  ''  => 'Seleccione',
  '1' => 'Barinitas',
  '2' => 'La Montaña',
  '3' => 'La Aguada',
  '4' => 'Loma Redona',
  '5' => 'Pico Espejo'
);
?>

<html>
    <head>
        <title>Editar Empleado</title>
    </head>
    <body>
        <h1>Editar un empleado</h1>
        <?=validation_errors()?>
        <?=form_open('empleado/editar/'.$fila[0]->empleado_id.'/2')?>
            <table>
                <tr>
                    <td>Codigo del empleado</td>
                    <td><?=$fila[0]->codigo_empl?></td>
                </tr>
                <tr>
                    <td><?=form_label('Nombre:','nombre')?></td>
                    <td><?=form_input(array('id'=>'nombre','name'=>'nombre','value'=>(set_value('nombre')!='' ? set_value('nombre') : $fila[0]->nombre)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Cedula:','cedula')?></td>
                    <td><?=form_input(array('id'=>'cedula','name'=>'cedula','value'=>(set_value('cedula')!='' ? set_value('cedula') : $fila[0]->cedula)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Fecha de Nacimiento:','fecha_nac')?></td>
                    <td><?=form_input(array('id'=>'fecha_nac','name'=>'fecha_nac','value'=>(set_value('fecha_nac')!='' ? set_value('fecha_nac') : $fila[0]->fecha_nac)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Estatus en la empresa:','estado')?></td>
                    <td><?=form_dropdown('estatus',array(''=>'- Seleccione -','Activo'=>'Activo','Inactivo'=>'Inactivo'),(set_value('estatus')!='' ? set_value('estatus') : $fila[0]->estatus))?></td>
                </tr>
                
                <tr>
                    <td><?=form_label('Cargo:','cargo')?></td>
                    <td><?=form_dropdown('cargo_id',$select_cargos,(set_value('cargo_id')!='' ? set_value('cargo_id') : $fila[0]->cargo_id))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Departamento:','departamento')?></td>
                    <td><?=form_dropdown('departamento_id',$S_departamentos,(set_value('departamento_id')!='' ? set_value('departamento_id') : $fila[0]->departamento_id))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Estacion:','estacion_id')?></td>
                    <td><?=form_dropdown('estacion_id',$S_estaciones,(set_value('estacion_id')!='' ? set_value('estacion_id') : $fila[0]->estacion_id))?></td>
                </tr>
                <tr>
                    <?=form_hidden('codigo_empl',$fila[0]->codigo_empl)?>
                    <?=form_hidden('empleado_id',$fila[0]->empleado_id)?>
                    <td><?=form_submit('Actualizar','Actualizar')?></td>
                </tr>
            </table>
        <?=form_close()?>
        </script>
    </body>
</html>