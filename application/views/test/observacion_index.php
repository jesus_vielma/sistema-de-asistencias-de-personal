<html>
    <head>
        <title>Observacion Index</title>
    </head>
    <body>
        <?php if($filas != 0):?>
            <h1>Ultimos agregados</h1>
            <table border=1>
                <tr>
                    <th>Codigo</th>
                    <th>Descripcion</th>
                    <th>Tipo</th>
                    <th>Asistencia</th>
		    <th colspan=3>Acciones</th>
                </tr>
                <?php foreach($filas as $fila):?>
                    <tr>
                       <td><?=$fila->codigo_justi?></td>
                       <td><?=$fila->descripcion?></td>
		       <td><?=$fila->tipo?></td>
                       <td><?=$fila->asistencia?></td>
		       <td><a href="<?=site_url('observacion/detalle/'.$fila->codigo_justi)?>">Detalle</a></td>
		       <td><a href="<?=site_url('observacion/editar/'.$fila->codigo_justi)?>">Editar</a></td>
		       <td><a href="<?=site_url('observacion/borrar/'.$fila->codigo_justi)?>">Borrar</a></td>
                    </tr>
                <?php endforeach;?>
            </table>
        <?php else: ?>
            <h3>No hay Observaciones en la Base de datos</h3>
        <?php endif; ?>
	<h3><a href="<?=site_url('observacion/insertar')?>">Insertar</a></h3>
	<h3><a href="<?=site_url('test')?>">Index</a></h3>
    </body>
</html>
