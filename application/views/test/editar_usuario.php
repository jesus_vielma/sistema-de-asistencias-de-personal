

<html>
    <head>
        <title>editar usuario</title>
        <style>
            .error{
                color: red;
            }
        </style>
    </head>
    <body>
        <h1>Editar usuario</h1>
        <span class="error"><?=validation_errors()?></span>
        <?=form_open('usuario/editar/'.$fila[0]->usuario_id.'/2')?>
            <table>
                <tr>
                    <td><?=form_label('Usuario:','usuario')?></td>
                    <td><?=form_input(array('id'=>'usuario','name'=>'usuario','value'=>(set_value('usuario')!='' ? set_value('usuario') : $fila[0]->usuario)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Contraseña:','contrasena')?></td>
                    <td><?=form_password(array('id'=>'contrasena','name'=>'contrasena'))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Repita contraseña:','confcontrasena')?></td>
                    <td><?=form_password(array('id'=>'confcontrasena','name'=>'confcontrasena'))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Email:','email')?></td>
                    <td><?=form_input(array('id'=>'email','name'=>'email','value'=>(set_value('email')!='' ? set_value('email') : $fila[0]->email)))?></td>
                </tr>
                 </tr>
                <?=form_hidden('empleado_id',$fila[0]->empleado_id)?>
                <tr>
                <?=form_hidden('usuario_id',$fila[0]->usuario_id)?>
                
                    <td><?=form_submit('Insertar','Modificar')?></td>
                </tr>
            </table>
        <?=form_close()?>
    </body>
</html>