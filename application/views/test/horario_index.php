<html>
    <head>
        <title>Horario Index</title>
    </head>
    <body>
        <?php if($filas != 0):?>
            <h1>Ultimos agregados</h1>
            <table border=1>
                <tr>
                    <th>Id</th>
                    <th>Hora de entrada</th>
                    <th>Hora de Salida</th>
		    <th>Descripción</th>
                    <th>Tipo</th>
		    <th colspan=3>Acciones</th>
                </tr>
                <?php foreach($filas as $fila):?>
                    <tr>
                       <td><?=$fila->horario_id?></td>
                       <td><?=$fila->hora_entrada?></td>
		       <td><?=$fila->hora_salida?></td>
		       <td><?=$fila->descripcion?></td>
                       <td><?=$fila->tipo?></td>
		       <td><a href="<?=site_url('horario/editar/'.$fila->horario_id)?>">Editar</a></td>
		       <td><a href="<?=site_url('horario/borrar/'.$fila->horario_id)?>">Borrar</a></td>
                    </tr>
                <?php endforeach;?>
            </table>
        <?php else: ?>
            <h3>No hay empleados en la Base de datos</h3>
        <?php endif; ?>
	<h3><a href="<?=site_url('horario/insertar')?>">Insertar</a></h3>
	<h3><a href="<?=site_url('test')?>">Index</a></h3>
    </body>
</html>

