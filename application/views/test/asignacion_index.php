<html>
    <head>
        <title>Asignacion Index</title>
    </head>
    <body>
        <?php if($filas != 0):?>
            <h1>Ultimos agregados</h1>
            <table border=1>
                <tr>
                    <th>Id</th>
                    <th>Fecha</th>
                    <th>Día</th>
		    <th>Turno</th>
		    <th>Empleado_Id</th>
		    <th colspan=3>Acciones</th>
                </tr>
                <?php foreach($filas as $fila):?>
                    <tr>
                       <td><?=$fila->asignacion_id?></td>
                       <td><?=$fila->fecha?></td>
		       <td><?=$fila->dia?></td>
		       <td><?=$fila->turno?></td>
                       <td><?=$fila->empleado_id?></td>
		       <td><a href="<?=site_url('asignacion/editar/'.$fila->asignacion_id)?>">Editar</a></td>
		       <td><a href="<?=site_url('asignacion/borrar/'.$fila->asignacion_id)?>">Borrar</a></td>
                    </tr>
                <?php endforeach;?>
            </table>
        <?php else: ?>
            <h3>No hay empleados en la Base de datos</h3>
        <?php endif; ?>
	<h3><a href="<?=site_url('asignacion/insertar')?>">Insertar</a></h3>
	<h3><a href="<?=site_url('test')?>">Index</a></h3>
    </body>
</html>

