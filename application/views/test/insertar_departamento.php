<html>
    <head>
        <title>Insertar Empleado</title>
    </head>
    <body>
        <h1>Insertar un Departamento</h1>
        <?=validation_errors()?>
        <?=form_open('departamento/insertar/2')?>
            <table>
                <tr>
                    <td><?=form_label('Nombre:','nombre')?></td>
                    <td><?=form_input(array('id'=>'nombre','name'=>'nombre','value'=>set_value('nombre')))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Supervisor:','supervisor')?></td>
                    <td><?=form_input(array('id'=>'supervisor','name'=>'supervisor','value'=>set_value('supervisor')))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Coordinador:','coordinador')?></td>
                    <td><?=form_input(array('id'=>'coordinador','name'=>'coordinador','value'=>set_value('coordinador')))?></td>
                </tr>
                 <tr>
                    <td><?=form_label('Descripcion:','descripcion')?></td>
                    <td><?=form_input(array('id'=>'descripcion','name'=>'descripcion','value'=>set_value('descripcion')))?></td>
                </tr>
                
                <tr>
                    <td><?=form_submit('Insertar','Insertar')?></td>
                </tr>
            </table>
        <?=form_close()?>
    </body>
</html>