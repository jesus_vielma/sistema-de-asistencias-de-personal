<html>
    <head>
        <title>Detalle departamento</title>
    </head>
    <body>
        <?php if ($fila!=0):?>
        <h3>Información del departamento</h3>
        <table>
            <tr>
                <th>Nombre de departamento</th>
                <td><?=$fila[0]->nombre?></td>
            </tr>
            <tr>
                <th>Nombre del supervisor</th>
                <td><?=$fila[0]->supervisor?></td>
            </tr>
            <tr>
                 <th>Nombre del Coordinador</th>
                 <td><?=$fila[0]->coordinador?></td>
            </tr>
            <tr>
                <th>Descripcion del departamento</th>
                <td><?=$fila[0]->descripcion?></td>
            </tr>
           
            </table>
            <h3><a href="<?=site_url('departamento/editar/'.$fila[0]->departamento_id).'/1'?>">Editar informacion</a></h3>
            <?php else :?>
                <h3>No se pudo encontrar la informacion del departamento</h3>
            <?php endif;?>
            <h3><a href="<?=site_url('departamento')?>">Lista de departamentos</a></h3>
    </body>
</html>