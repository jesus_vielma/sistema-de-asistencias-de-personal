<html>
    <head>
        <title>Usuario Index</title>
    </head>
    <body>
        <?php if($filas != 0):?>
            <h1>Ultimos agregados</h1>
            <table border=1>
                <tr>
                    <th>Id</th>
                    <th>Usuario</th>
                    <th>Contraseña</th>
		       <th>Email</th>
		    <th colspan=3>Acciones</th>
                </tr>
                <?php foreach($filas as $fila):?>
                    <tr>
                       <td><?=$fila->usuario_id?></td>
                       <td><?=$fila->usuario?></td>
		       <td><?=$fila->contrasena?></td>
		       <td><?=$fila->email?></td>
		       <!--<td><a href="<?=site_url('empleado/detalle/'.$fila->id)?>">Detalle</a></td>-->
		       <td><a href="<?=site_url('usuario/editar/'.$fila->usuario_id)?>">Editar</a></td>
		       <td><a href="<?=site_url('usuario/borrar/'.$fila->usuario_id)?>">Borrar</a></td>
                    </tr>
                <?php endforeach;?>
            </table>
        <?php else: ?>
            <h3>No hay usuarios en la Base de datos</h3>
        <?php endif; ?>
	<a href="<?=site_url('usuario/insertar')?>">Insertar</a>
    </body>
</html>
