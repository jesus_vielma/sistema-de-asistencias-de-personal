<html>
    <head>
        <title>Insertar Estacion</title>
    </head>
    <body>
        <h1>Insertar una Estacion</h1>
        <?=validation_errors()?>
        <?=form_open('Estacion/insertar/2')?>
            <table>
                <tr>
                    <td><?=form_label('Nombre:','nombre')?></td>
                    <td><?=form_input(array('id'=>'nombre','name'=>'nombre','value'=>set_value('nombre')))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Altura:','altura')?></td>
                    <td><?=form_input(array('id'=>'altura','name'=>'altura','value'=>set_value('altura')))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Descripcion:','descripcion')?></td>
                    <td><?=form_input(array('id'=>'descripcion','name'=>'descripcion','value'=>set_value('descripcion')))?></td>
                </tr> 
                <tr>
                    <td><?=form_submit('Insertar','Insertar')?></td>
                </tr>
            </table>
        <?=form_close()?>
    </body>
</html>
