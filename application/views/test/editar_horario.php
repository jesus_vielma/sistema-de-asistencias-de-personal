<?php
<?php
$tipo = array(                       
              ''         => '- Seleccione -',
              'Normal'   => 'Normal',
              '12x12'    => '12x12',
              'Pernocta' => 'Pernocta');
?>
<html>
    <head>
        <title>Editar Horario</title>
    </head>
    <body>
        <h1>Editar horario</h1>
        <?=validation_errors()?>
        <?=form_open('horario/editar/'.$fila[0]->horario_id.'/2')?>
            <table>
                <tr>
                    <td><?=form_label('Hora de Entrada:','hora_entrada')?></td>
                    <td><?=form_input(array('id'=>'hora_entrada','name'=>'hora_entrada','value'=>(set_value('hora_entrada')!='' ? set_value('hora_entrada') : $fila[0]->hora_entrada),'type'=>'time'))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Hora de Salida:','hora_salida')?></td>
                    <td><?=form_input(array('id'=>'hora_salida','name'=>'hora_salida','value'=>(set_value('hora_salida')!='' ? set_value('hora_salida') : $fila[0]->hora_salida),'type'=>'time'))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Descripción:','descripcion')?></td>
                    <td><?=form_textarea(array('id'=>'descripcion','name'=>'descripcion','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Tipo:','tipo')?></td>
                    <td><?=form_dropdown('tipo',$tipo,(set_value('tipo')!='' ? set_value('tipo') : $fila[0]->tipo))?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Actualizar','Actualizar')?></td>
                </tr>
                <?=form_hidden('horario_id',$fila[0]->horario_id)?>
            </table>
        <?=form_close()?>
    </body>
</html>