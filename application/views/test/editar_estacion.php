<html>
    <head>
        <title>Editar editar Estacion</title>
    </head>
    <body>
        <h1>Editar Estacion</h1>
        <?=validation_errors()?>
        <?=form_open('estacion/editar/'.$fila[0]->estacion_id.'/2')?>
            <table>
                <tr>
                    <td><?=form_label('Nombre:','nombre')?></td>
                    <td><?=form_input(array('estacion_id'=>'nombre','name'=>'nombre','value'=>(set_value('nombre')!='' ? set_value('nombre') : $fila[0]->nombre)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Altura','altura')?></td>
                    <td><?=form_input(array('estacion_id'=>'altura','name'=>'altura','value'=>(set_value('altura')!='' ? set_value('altura') : $fila[0]->altura)))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Descripción:','descripcion')?></td>
                    <td><?=form_textarea(array('estacion_id'=>'descripcion','name'=>'descripcion','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Actualizar','Actualizar')?></td>
                </tr>
                <?=form_hidden('estacion_id',$fila[0]->estacion_id)?>
            </table>
        <?=form_close()?>
    </body>
</html>
