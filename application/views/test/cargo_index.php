<html>
    <head>
        <title>Cargo Index</title>
    </head>
    <body>
        <?php if($filas != 0):?>
            <h1>Ultimos agregados</h1>
            <table border=1>
                <tr>
                    <th>Id</th>
                    <th>Cargo</th>
                    <th>Descripcion</th>
		    <th colspan=3>Acciones</th>
                </tr>
                <?php foreach($filas as $fila):?>
                    <tr>
                       <td><?=$fila->cargo_id?></td>
                       <td><?=$fila->cargo?></td>
		       <td><?=$fila->descripcion?></td>
		       <!--<td><a href="<?=site_url('empleado/detalle/'.$fila->id)?>">Detalle</a></td>-->
		       <td><a href="<?=site_url('cargo/editar/'.$fila->cargo_id)?>">Editar</a></td>
		       <td><a href="<?=site_url('cargo/borrar/'.$fila->cargo_id)?>">Borrar</a></td>
                    </tr>
                <?php endforeach;?>
            </table>
        <?php else: ?>
            <h3>No hay cargos en la Base de datos</h3>
        <?php endif; ?>
	<a href="<?=site_url('cargo/insertar')?>">Insertar</a>
    </body>
</html>
