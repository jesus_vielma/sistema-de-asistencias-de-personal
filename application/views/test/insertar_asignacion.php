<?php
$decicion = array(                       
              ''         => '- Seleccione -',
              'Si'       => 'Si',
              'No'       => 'No');
/*$dias = array(
    ''         => '- Seleccione -',
    'Lunes'    => 'Lunes',
    'Martes'   => 'Martes',
    'Miercoles'=> 'Miercoles',
    'Jueves'   => 'Jueves',
    'Viernes'  => 'Viernes',
    'Sabado'   => 'Sabado',
    'Domingo'  => 'Domingo');
*/
$turno = array(
    ''       => '- Seleccione -',
    'Día'    => 'Día',
    'Noche'  => 'Noche',
    'Libre'  => 'Libre'
);

$S_empleados = array(''=>'- Seleccione -');
foreach($empleados as $empleado)
{
    $S_empleados[$empleado->empleado_id] = $empleado->nombre;
}

$S_horarios = array(''=>'- Seleccione -');
foreach($horarios as $horario)
{
    $S_horarios[$horario->horario_id] = $horario->tipo.", de ".$horario->hora_entrada." a ".$horario->hora_salida;
}
?>
<html>
    <head>
        <title>Insertar Asignacion</title>
    </head>
    <body>
        <h1>Inserta Asignacion</h1>
        <?=validation_errors()?>
        <?=form_open('asignacion/insertar/2')?>
            <table>
                <tr>
                    <td><?=form_label('Fecha:','fecha')?></td>
                    <td><?=form_input(array('id'=>'fecha','name'=>'fecha','value'=>set_value('fecha'),'type'=>'date'))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Pernocta:','pernocta')?></td>
                    <td><?=form_dropdown('pernocta',$decicion,'')?></td>
                </tr>
                <tr>
                    <td><?=form_label('¿Trabaja?:','trabaja')?></td>
                    <td><?=form_dropdown('trabajo',$decicion,'')?></td>
                </tr>
                <tr>
                    <td><?=form_label('Dia:','dia')?></td>
                    <td><?=form_input(array('id'=>'dia','name'=>'dia','value'=>set_value('dia')))?></td>
                </tr>
                <tr>
                    <td><?=form_label('Turno:','turno')?></td>
                    <td><?=form_dropdown('turno',$turno,'')?></td>
                </tr>
                <tr>
                    <td><?=form_label('Empleado:','empleado_id')?></td>
                    <td><?=form_dropdown('empleado_id',$S_empleados,'')?></td>
                </tr>
                <tr>
                    <td><?=form_label('Horario:','horario_id')?></td>
                    <td><?=form_dropdown('horario_id',$S_horarios,'')?></td>
                </tr>
                <tr>
                    <td><?=form_submit('Insertar','Insertar')?></td>
                </tr>
            </table>
        <?=form_close()?>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                //$("#nombre").focus();
                $("#fecha").change(function(){
                    var date = $("#fecha").val();
                    $.post("<?=site_url('asignacion/get_date')?>", {date: date}, function(data){
                        if (data != "") {
                            $("#dia").val(data);
                        }
                        else
                            {
                                alert("No se puede obtener el dia para la fecha indicada");
                                //$("#nombre").focus();
                            }
                    });
            });
        });
        </script>
    </body>
</html>