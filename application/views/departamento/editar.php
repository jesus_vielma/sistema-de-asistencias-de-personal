<?php
$S_empleados = array(''=>'- Seleccione -');
foreach($empleados as $empleado)
{
    $S_empleados[$empleado->nombre] = $empleado->nombre;
}
?>
         <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Departamentos</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('departamento')?>">Departamentos</a>
                        </li>
                        <li class="active">
                            <strong>Insertar departamento</strong>
                        </li>
                    </ol>
                </div>
          </div>
           <div class="wrapper wrapper-content">        
                <div class="row">
                   <div class="col-lg-8 col-lg-offset-2">
                        <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3>Editar Departamento</h3>
                               </div>
                               <div class="panel-body">
                                        <?=validation_errors()?>
                                        <?=form_open('departamento/editar/'.$fila[0]->departamento_id.'/2')?>
                                            <div class="form-group">
                                               <?=form_label('Nombre:','nombre')?>
                                               <?=form_input(array('departamento_id'=>'nombre','name'=>'nombre','class'=>'form-control','value'=>(set_value('nombre')!='' ? set_value('nombre') : $fila[0]->nombre)))?>
                                            </div>
                                            <div class="form-group">
                                               <?=form_label('Responsable','responsable')?>
                                               <?=form_dropdown('responsable',$S_empleados,(set_value('departamento_id')!='' ? set_value('responsable') : $fila[0]->responsable),'class="chosen-select form-control" id="responsable" required')?>
                                            </div>
                                            <div class="form-group">
                                               <?=form_label('Descripción:','descripcion')?>
                                               <?=form_textarea(array('departamento_id'=>'descripcion','class'=>'form-control','name'=>'descripcion','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?>
                                            </div>

                                                
                                        <?=form_hidden('departamento_id',$fila[0]->departamento_id)?>
                                     <div class="row">
                                          <div class="col-lg-6 col-lg-offset-3">
                                              <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                              <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                                          </div>
                                      </div>
                                                
                                        <?=form_close()?>
                                </div>
                                
                               </div>   
                        </div>
                     </div>
                </div>
       