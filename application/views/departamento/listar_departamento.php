  <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Marcaje</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('departamento')?>">Departamento</a>
                        </li>
                        <li class="active">
                            <strong>Listar Empleados por departamento</strong>
                        </li>
                    </ol>
                </div>

          </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3>Empleados</h3>
                    </div>
                      <div class="panel-body ">
                         <?php if($filas != 0):?>
                              <div class="panel-body ">
                               <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Empleado</th>
                                        <th>Cédula:</th>
                                        <th>Fecha de ingreso</th>
                                    </tr>
                              </thead>
                                 <tbody>
                                    <?php foreach($filas as $fila):?>
                                    <tr class="odd gradeX">
                                    
                                          <td><?=$fila->nombre?></td>
                                          <td><?=$fila->cedula?></td>
                                          <td><?=$fila->fecha_ingreso?></td>
                                        <td>
                                            <div class="btn-group tooltip-demo">
                                                <a href="<?=site_url('empleado/detalle/'.$fila->empleado_id)?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Detalles"><i class="fa fa-search"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existen marcajes registrados</span>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
  </div>
 