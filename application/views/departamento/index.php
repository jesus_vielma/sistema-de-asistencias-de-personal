
      <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Departamentos</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('departamento')?>">Departamentos</a>
                        </li>
                        <li class="active">
                            <strong>Listar departamentos</strong>
                        </li>
                    </ol>
                </div>

          </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3>Departamentos</h3>
                    </div>
                      <div class="panel-body ">
                        <?php if($filas != 0 ):?>
                               <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th  style="width: 35%">Nombre del departamento</th>
                                        <th>Descripción</th>
                                        <th style="width: 10%">Acciones</th>
                                    </tr>
                              </thead>
                                <tbody>
                                    <?php foreach($filas as $fila):?>
                                    <tr class="odd gradeX">
                                           <td><?=$fila->nombre?></td>
                                          <td><?=$fila->descripcion?></td>
                                        <td>
                                            <div class="btn-group tooltip-demo">
                                                <a href="<?=site_url('departamento/detalle/'.$fila->departamento_id)?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Detalles"><i class="fa fa-search"></i></a>
                                                <a href="<?=site_url('departamento/editar/'.$fila->departamento_id.'/1')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Editar información"><i class="fa fa-pencil"></i></a>
                                                <!--<a href="<?=site_url('departamento/borrar/'.$fila->empleado_id)?>">Borrar</a>-->
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existen departamentos registrados</span>
                            </div>
                        <?php endif;?>
                    </div>
                    <div class="panel-footer">
                        <a href="<?=site_url('departamento/insertar')?>" class="btn btn-success"><i class="fa fa-plus"></i> Agregar departamentos</a>
                    </div>
                </div>
            </div>
        </div>
  </div>
