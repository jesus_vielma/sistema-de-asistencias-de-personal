      <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Feriados</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li class="active">
                            <strong>Calendario</strong>
                        </li>
                    </ol>
                </div>

          </div>

          <div class="wrapper wrapper-content">
            <div class="ibox-content">
                <div id="calendar"></div>
            </div>
          </div>