   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Feriados</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('feriados')?>">Feriados</a>
                        </li>
                        <li class="active">
                            <strong>Insertar un Feriado</strong>
                        </li>
                    </ol>
                </div>
          </div>
           <div class="wrapper wrapper-content">        
                <div class="row">
                   <div class="col-lg-8 col-lg-offset-2">
                        <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3>Editar Un Feriados</h3>
                               </div>
                               <div class="panel-body">
                                        <?=validation_errors()?>
                                        <?=form_open('feriados/editar/'.$fila[0]->feriado_id.'/2')?>
                                            <div class="form-group">
                                               <?=form_label('Dia:','dia')?>
                                               <?=form_input(array('feriado_id'=>'dia','name'=>'dia','class'=>'form-control','value'=>(set_value('dia')!='' ? set_value('dia') : $fila[0]->dia)))?>
                                            </div>
                                            <div class="form-group">
                                               <?=form_label('Mes','mes')?>
                                               <?=form_input (array('feriado_id'=>'mes','class'=>'form-control','name'=>'mes','value'=>(set_value('mes')!='' ? set_value('mes') : $fila[0]->mes)))?>
                                            </div>
                                            <div class="form-group">
                                               <?=form_label('Ano','ano')?>
                                               <?=form_input (array('feriado_id'=>'ano','class'=>'form-control','name'=>'ano','value'=>(set_value('ano')!='' ? set_value('ano') : $fila[0]->ano)))?>
                                            </div>
                                            <div class="form-group">
                                               <?=form_label('Descripción:','descripcion')?>
                                               <?=form_textarea(array('feriado_id'=>'descripcion','class'=>'form-control','name'=>'descripcion','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?>
                                            </div>

                                                
                                        <?=form_hidden('feriado_id',$fila[0]->feriado_id)?>
                                        <div class="row">
                                            <div class="col-lg-6 col-lg-offset-3">
                                                <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                                <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                                            </div>
                                        </div>
                                                
                                        <?=form_close()?>
                                </div>
                                </div>   
                      <?=form_close()?>
                        </div>
                     </div>
                </div>
       
