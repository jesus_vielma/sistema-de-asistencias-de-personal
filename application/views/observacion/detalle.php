<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-8">

                    <h2>Justificación</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('observacion')?>">Justificación</a>
                        </li>
                        <li class="active">
                            <strong>Detalle de Justificación</strong>
                        </li>
                    </ol>
          </div>
<?php if($fila!=0):?>  
          <div class="col-lg-4">
                    <div class="title-action">
                       <a href="<?=site_url('observacion/editar/'.$fila[0]->codigo_justi.'/1')?>" class="btn btn-white"><i class="fa fa-pencil"></i>Editar</a>      
                        <a href="<?=site_url('observacion/detalle_print/'.$fila[0]->codigo_justi)?>" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i> Imprimir </a>
                    </div>
                </div>
    
         
         <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="ibox-content p-xl">
                             <div class="row">
                                    <div class="col-lg-4">
                                            <span>
                                     <img  src="<?=base_url('assets/img/Mlogo.png')?> " class="img-reponsive col-lg-8" />
                                      </span>
                                   </div>
                                    <div class="col-lg-4 col-lg-offset-1">
                                        <br>
                                        <h2><strong>Justificación</strong></h2>
                                    </div>
                             </div>
                             <br>
                            <div class="row">
                                     <div class="col-lg-6">
                                            <address>
                                                <!--<strong><dt>Detalles del Justificativo</dt></strong><br>-->
                                                <dt>Empleado</dt>
                                                <?=$fila[0]->nombre?><br><br>
                                                <dt>Cedula:</dt> <?=$fila[0]->cedula?><br><br>
                                                <dt>Hecho por:</dt> <?=$fila[0]->nombre_supervisor?>
                                            </address>
                                    </div>
                                    <div class="col-lg-6 text-right">
                                         <address>
                                            <h4><dt>N° de Justificación.</dt></h4>
                                            <h4 class="text-navy"><dd><?=$fila[0]->codigo_justi?><dd></h4>
                                            <br>
                                            Edo. Mérida<br>
                                         </address> 
                                       <strong>Fecha de justificativo: </strong><?=$fila[0]->fecha?></span><br/>
                                    </div>
                            </div>
                    <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    <strong>Descripción:</strong><br>
                                    <?=$fila[0]->descripcion?>
                                </div>
                                
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-12">
                                    <strong>Tipo:</strong><br>
                                    <?=$fila[0]->tipo?>
                                </div>
                                
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-12">
                                    <strong>Asistencia</strong><br>
                                    <?=$fila[0]->asistencia?>
                                </div>
                            </div>
                            
                            <!--<div class="well m-t"><strong>Sistema Teleferico de Mérida</strong>-->
                            <!--Justificacion hecha al ciudadano  <?=$fila[0]->nombre?> por la administracion del Mukumbarí. Sistema Teleferico de Mérida.-->
                            <!--</div>-->
                            <div class="well m-t">
                                <strong>Nota:</strong>
                                Esta justificación ha sido realizada con el pleno consentiemiento del trabajor y <?=$fila[0]->nombre_supervisor?> quien fue su redactor(a).<br>
                                Esta justificación no tendra validez si no esta aprobada por el jefe inmendiato del trabajor y el Coordinador de Talento Humano.
                            </div>
                             <?php else :?>
                                    <div class="alert alert-info">
                                        <h3>Información</h3>
                                        <span>No existen Justificaciones registrados</span>
                                    </div>
                             <?php endif;?>
                     </div>
               </div>
            </div>
         </div>
        </div>