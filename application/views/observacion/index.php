
      <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Justificacion</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('observacion')?>">Justificacion</a>
                        </li>
                        <li class="active">
                            <strong>Listar de Justificaciones</strong>
                        </li>
                    </ol>
                </div>

          </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3>Justificación</h3>
                    </div>
                        <div class="panel-body">
                        <?php if($filas != 0 ):?>
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Descripcion</th>
                                        <th>Tipo</th>
                                        <th>Asistencia</th>
                                        <th>Empleado</th>
                                        <th>Cedúla</th>
                                        <th style="width: 10%">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($filas as $fila):?>
                                    <tr class="odd gradeX">
                                        <td><?=$fila->fecha?></td>
                                        <td><?=$fila->descripcion?></td>
                                        <td><?=$fila->tipo?></td>
                                        <td><?=$fila->asistencia?></td>
                                        <td><?=$fila->nombre?></td>
                                        <td><?=$fila->cedula?></td>
                                        <td>
                                            <div class="btn-group tooltip-demo">
                                                <a href="<?=site_url('observacion/detalle/'.$fila->codigo_justi)?>"class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Detalle"><i class="fa fa-search"></i></a>
                                                <a href="<?=site_url('observacion/editar/'.$fila->codigo_justi.'/1')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Editar información"><i class="fa fa-pencil"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                                <!--<tfoot>
                                    <tr>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existen justificaciones</span>
                            </div>
                        <?php endif;?>
                    </div>
                    <div class="panel-footer">
                        <a href="<?=site_url('observacion/insertar')?>" class="btn btn-success"><i class="fa fa-plus"></i> Agregar justificación</a>
                    </div>
                </div>
            </div>
        </div>
            </div>