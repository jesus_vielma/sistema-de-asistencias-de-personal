
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Login</title>

    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet">

    <link href="<?=base_url('assets/css/animate.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">

</head>

<body class="gray-bg">

      <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

            </div>
            <h3>Recuperación de Contraseña</h3>
            <?php if($error=='Si'):?>
            <div class="alert alert-warning">
               No existe esta dirección de correo electrónico. 
            </div>
            <?php endif;?>
            
            <form class="m-t" role="form" action="<?=site_url('login/recuperar')?>" autocomplete="off" method="POST">
                <div class="form-group">
                 
                    <input type="text" name="email" class="form-control" placeholder="Introduzca su correo electronico " required="">
                </div>
               
                <button type="submit" class="btn btn-primary block full-width m-b">Entrar</button>

            </form>
            <p class="m-t"> <small>SICAP Version 1 &copy; 2015</small> </p>
        </div>
    </div>
      
    <script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>

</body>

    
        
