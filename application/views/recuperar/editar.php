    
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Editar usuario</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('usuario')?>">Usuario</a>
                        </li>
                        <li class="active">
                            <strong>Editar Usuario</strong>
                        </li>
                    </ol>
                </div>
               
            </div>

            <div class="wrapper wrapper-content">
                
                <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3>Editar usuario</h3>
                    </div>
                    
                    <div class="panel-body">
                    
                    <?=form_open('usuario/editar/'.$fila[0]->usuario_id.'/2',array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>
                    
                    <?=validation_errors();?>
                        
                        <div class="form-group">
                            
                    <?=form_label('Usuario:','usuario')?>
                    <?=form_input(array('id'=>'usuario','class'=>'form-control','name'=>'usuario','value'=>(set_value('usuario')!='' ? set_value('usuario') : $fila[0]->usuario),'required'=>'required'))?>
 
                        </div>
                        
                        <div class="form-group">
                  
                    <?=form_label('Contraseña:','contrasena')?>
                    <?=form_password(array('id'=>'contrasena','name'=>'contrasena','class'=>'form-control','value'=>set_value('contrasena'),'required'=>'required'))?>
                       </div>
                        
                        
                        <div class="form-group">
                            
                <?=form_label('Repita contraseña:','confcontrasena')?>
                <?=form_password(array('id'=>'confcontrasena','name'=>'confcontrasena','class'=>'form-control','value'=>set_value('confcontrasena'),'required'=>'required'))?>
                 
                        </div>
                        
                        <div class="form-group">
                        
                <?=form_label('Email:','email')?>
                <?=form_input(array('id'=>'email','name'=>'email','class'=>'form-control','value'=>(set_value('email') !='' ? set_value('email') : $fila[0]->email)))?>
                
                        </div>
                        
                        
                
                        
                <?=form_hidden('empleado_id',$fila[0]->empleado_id)?>
                
                <?=form_hidden('usuario_id',$fila[0]->usuario_id)?>
                        
                        
                    
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div
                    <?=form_close()?>
                </div>
            </div>
        </div>
            </div>
