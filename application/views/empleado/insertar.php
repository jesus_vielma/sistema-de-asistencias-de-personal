<?php
$select_cargos = array(''=>'- Seleccione -');
foreach($cargos as $cargo)
{
    $select_cargos[$cargo->cargo_id] = $cargo->cargo;
}

$S_departamentos = array(''=>'- Seleccione -');
foreach($departamentos as $departamento)
{
    $S_departamentos[$departamento->departamento_id] = $departamento->nombre;
}

$S_estaciones = array(
  ''  => '- Seleccione -',
  '1' => 'Barinitas',
  '2' => 'La Montaña',
  '3' => 'La Aguada',
  '4' => 'Loma Redona',
  '5' => 'Pico Espejo'
);
?>

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Empleados</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('empleado')?>">Empleados</a>
                        </li>
                        <li class="active">
                            <strong>Insertar empleado</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                
                <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3>Insertar empleado</h3>
                    </div>
                    
                    <div class="panel-body">
                    
                    <?=form_open_multipart('empleado/insertar/2',array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>
                    <?=validation_errors();?>
                    <?php if($error_subida):?>
                    <div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?=$error_subida?>
                    </div>
                    <?php endif;?>
                        <div class="form-group">
                            <?=form_label('Nombre:','nombre')?>
                            <?=form_input(array('id'=>'nombre','name'=>'nombre','class'=>'form-control','value'=>set_value('nombre'),'required'=>'required','onKeyUp'=>'this.value=this.value.toUpperCase();'))?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Cédula:','cedula')?></td>
                            <?=form_input(array('id'=>'cedula','name'=>'cedula','class'=>'form-control','value'=>set_value('cedula'),'required'=>'required'))?>
                            <!--<span class="help-block">La cédula debe tener este formato xx.xxx.xxx</span>-->
                        </div>
                        <div class="form-group" id="data_1">
                                <?=form_label('Fecha de Nacimiento:','fecha_nac')?>
                                <div class="input-group date " >
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="fecha_nac" type="text" required class="form-control" value="<?=set_value('fecha_nac')?>">
                                </div>
                        </div>
                        <div class="form-group" id="data_1">
                                <?=form_label('Fecha de Ingreso:','fecha_ingreso')?>
                                <div class="input-group date " >
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="fecha_ingreso" type="text" required class="form-control" value="<?=set_value('fecha_ingreso')?>">
                                </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Codigo:','codigo_empl1')?>
                            <span id="codigo_empl"></span>
                        </div>
                        <div class="form-group">
                            <?=form_label('Tipo de contrato','contrato')?>
                            <br>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <label class="btn btn-primary btn-outline">
                                  <input type="radio" name="contrato" id="contrato" autocomplete="off" value="Fijo"><i class="glyphicon glyphicon-ok"></i> Fijo
                                </label>
                                <label class="btn btn-default btn-outline">
                                  <input type="radio" name="contrato" id="contrato" autocomplete="off" value="Contratado"><i class="glyphicon glyphicon-repeat"></i> Contratado
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Cargo:','cargo')?>
                            <?=form_dropdown('cargo_id',$select_cargos,set_value('cargo_id'),'class="chosen-select form-control" id="cargo" required="required"  tabindex="2"')?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Departamento:','departamento')?>
                            <?=form_dropdown('departamento_id',$S_departamentos,set_value('departamento_id'),'class="chosen-select form-control" id="departamento" required="required" tabindex="2"')?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Estacion:','estacion')?>
                            <?=form_dropdown('estacion_id',$S_estaciones,set_value('estacion_id'),'class="form-control" id="estacion" required="required"')?>
                        </div>
                        <!--<div class="form-group">
                            <?=form_label('Fotografia:','imagen')?>
                            <?=form_upload(array('id'=>'imagen','name'=>'userfile','class'=>'form-control','value'=>set_value('imagen'),'required'=>'required'))?>
                        </div>-->
                        <?=form_hidden('estatus','Activo')?>
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3    ">
                                <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                            </div>
                        
                        </div>
                    </div
                    <?=form_close()?>
                </div>
            </div>
        </div>
            </div>
