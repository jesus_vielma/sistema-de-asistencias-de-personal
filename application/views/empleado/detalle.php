<?php
if($fila[0]->fecha_nac)
{
  $da= explode('-', $fila[0]->fecha_nac);   

   $dia = $da[2];  
    $mes = $da[1]; 
     $anio = $da[0];  

      $diac =date("d"); 
       $mesc =date("m"); 
       $anioc =date("Y"); 

      $edadac=  $anioc-$anio; 

   if($mesc < $mes && $diac < $dia || $mesc < $mes || $diac < $dia){ 

    $edad_aux = $edadac - 1; 

     $edad = $edad_aux; 
     }
     else
        $edad = $edadac; 
}
else
{
    $edad = "No se puede calcular la edad";
}

if($fila[0]->fecha_ingreso)
{
  $da1= explode('-', $fila[0]->fecha_ingreso);   

   $dia1 = $da1[2];  
    $mes1 = $da1[1]; 
     $anio1 = $da1[0];  

      $diac1 =date("d"); 
       $mesc1 =date("m"); 
       $anioc1 =date("Y"); 

      $servicio=  $anioc-$anio1; 

   if($mesc1 < $mes1 && $diac1 < $dia1 || $mesc1 < $mes1 || $diac1 < $dia1){ 

    $servicio_aux = $servicio - 1; 

     $aservicio = $servicio_aux; 
     }
     else
        $aservicio = $servicio; 
}
else
{
    $aservicio = "No se pueden calcular los años der servicio";
}

$nivel = $this->session->userdata('usuario')['rol'];
?>
          
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-12">
                    <h2>Empleados</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('empleado')?>">Empleados</a>
                        </li>
                        <li class="active">
                            <strong>Información detalla del empleado</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <?php if($fila!=0):?>
                    <div class="ibox-title">
                        <div class="m-b-md">
                            <div class="pull-right">
                                <a href="<?=site_url('reporte/detalle_empleado/'.$fila[0]->empleado_id)?>" class="btn btn-outline btn-success btn-sm" target="_blank"><i class="fa fa-print"></i> Imprimir </a>
                                <?php if($nivel==1):?>
                                <a href="<?=site_url('empleado/editar/'.$fila[0]->empleado_id.'/1')?>" class="btn btn-outline btn-default btn-sm"><i class="fa fa-pencil"></i> Editar información Personal</a>
                                <?php endif;?>
                            </div>
                            <h3><strong>Empleado</strong>: <?=$fila[0]->nombre?></h3>
                        </div>
                    </div>
                    <?php if($error_subida):?>
                    <div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?=$error_subida?>
                    </div>
                    <?php endif;?>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-2">
                                <?php if($fila[0]->imagen==NULL || $fila[0]->imagen==''):?>
                                <p class="text-info">No se ha agregado una foto del trabajador</p>
                                <?=form_open_multipart('empleado/imagen/'.$fila[0]->empleado_id.'/'.$fila[0]->codigo_empl)?>
                                    
                                    <div class="fileUpload btn btn-white btn-sm">
                                        <span>Seleccione</span>
                                        <input type="file" name="userfile" class="upload"> 
                                    </div>
                                    <button type="submit" class="btn btn-primary" title="Subir"><i class="fa fa-upload"></i></button>
                                <?=form_close()?>
                                <?php else:?>
                                <img src="<?=base_url('assets/img/empleados/'.$fila[0]->imagen)?>" class="img-rounded img-responsive " >
                                <?php endif;?>
                                <div class="clearfix"></div>
                            </div>
                                <div class="col-lg-5">
                                    <dl class="dl-horizontal">
                                        <dt>Estado:</dt> <dd><span <?=($fila[0]->estatus)=='Activo' ? 'class="label label-primary " >Activo' : 'class="label label-danger">Inactivo'?></span></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Código:</dt> <dd><?=$fila[0]->codigo_empl?></dd>
                                        <dt>Cédula:</dt> <dd><?=$fila[0]->cedula?></dd>
                                        <dt>Fecha de Nacimiento:</dt> <dd><?=($fila[0]->fecha_nac)!='' ? $fila[0]->fecha_nac : 'Debe agregar una fecha de nacimiento'?></dd>
                                        <dt>Edad:</dt> <dd><?=$edad?></dd>
                                        <!--<dt>Client:</dt> <dd><a href="#" class="text-navy"> Zender Company</a> </dd>-->
                                    </dl>
                                </div>
                                <div class="col-lg-5">
                                    <dl class="dl-horizontal">
                                        <dt>Tipo de contrato:</dt> <dd><span <?=($fila[0]->contrato)=='Fijo' ? 'class="label label-primary " > Fijo' : 'class="label"><i class="fa fa-repeat"></i> Contratado'?></span></dd>
                                    </dl>
                                    <dl class="dl-horizontal" >
                                        <dt>Departamento Adjunto:</dt> <dd><?=$fila[0]->dep_nombre?></dd>
                                        <dt>Cargo:</dt> <dd><?=$fila[0]->cargo?></dd>
                                        <dt>Estación de Trabajo</dt><dd><?=$fila[0]->estacion?> </dd>
                                        <dt>Fecha de ingreso</dt><dd><?=$fila[0]->fecha_ingreso?> </dd>
                                        <!--<dt>Años de servicio</dt><dd><?=$aservicio?> </dd>-->
                                    </dl>
                                </div>
                            </div>
                            <!--<div class="row">
                                <div class="col-lg-6">
                                    <dl class="dl-horizontal">
                                        <dt>Código:</dt> <dd><?=$fila[0]->codigo_empl?></dd>
                                        <dt>Cédula:</dt> <dd><?=$fila[0]->cedula?></dd>
                                        <dt>Fecha de Nacimiento:</dt> <dd><?=($fila[0]->fecha_nac)!='' ? $fila[0]->fecha_nac : 'Debe agregar una fecha de nacimiento'?></dd>
                                        <dt>Edad:</dt> <dd><?=$edad?></dd>
                                        <dt>Client:</dt> <dd><a href="#" class="text-navy"> Zender Company</a> </dd>
                                    </dl>
                                </div>
                                <div class="col-lg-6">
                                    <dl class="dl-horizontal" >
                                        <dt>Departamento Adjunto:</dt> <dd><?=$fila[0]->dep_nombre?></dd>
                                        <dt>Cargo:</dt> <dd><?=$fila[0]->cargo?></dd>
                                        <dt>Estación de Trabajo</dt><dd><?=$fila[0]->estacion?> </dd>
                                        <dt>Fecha de ingreso</dt><dd><?=$fila[0]->fecha_ingreso?> </dd>
                                    </dl>
                                </div>
                            </div>-->
                            <!--<div class="row">
                                <div class="col-lg-12">
                                    <dl class="dl-horizontal">
                                        <dt>Completed:</dt>
                                        <dd>
                                            <div class="progress progress-striped active m-b-sm">
                                                <div style="width: 60%;" class="progress-bar"></div>
                                            </div>
                                            <small>Project completed in <strong>60%</strong>. Remaining close the project, sign a contract and invoice.</small>
                                        </dd>
                                    </dl>
                                </div>
                            </div>-->
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Ultimas asignaciones</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a href="<?=site_url('asignacion/insertar/1/'.$fila[0]->empleado_id)?>">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <?php if($asignaciones!=0):?>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>Día</th>
                                                    <th>Turno</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($asignaciones as $asignacion):?>
                                                    <tr>
                                                        <td><?=$asignacion->fecha?></td>
                                                        <td><?=$asignacion->dia?></td>
                                                        <td><?=$asignacion->turno?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    <?php else:?>
                                        <div class="alert alert-info">
                                            <h3>Información</h3>
                                            <span>No existen asignaciones para este empleado</span>
                                        </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-6">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Marcajes últimos quince días</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <?php if($marcajes!=0):?>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>Hora</th>
                                                    <th>Día</th>
                                                    <th>Tiempo Extra</th>
                                                    <th>Tipo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($marcajes as $marcaje):?>
                                                    <tr>
                                                        <td><?=$marcaje->fecha?></td>
                                                        <td><?=$marcaje->hora?></td>
                                                        <td><?=$marcaje->dia?></td>
                                                        <td><?=$marcaje->tiempo_extra?></td>
                                                        <td><?=$marcaje->tipo?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    <?php else:?>
                                        <div class="alert alert-info">
                                            <h3>Información</h3>
                                            <span>No existen marcajes para este empleado</span>
                                        </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existe el empleado</span>
                            </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
            </div>