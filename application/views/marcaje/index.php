  <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Marcaje</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('marcaje/listar')?>">Marcaje</a>
                        </li>
                        <li class="active">
                            <strong>Listar Marcajes</strong>
                        </li>
                    </ol>
                </div>

          </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3>Marcajes</h3>
                    </div>
                      <div class="panel-body ">
                         <?php if($filas != 0):?>
                              <div class="panel-body ">
                               <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Nombre del empleado</th>
                                        <th>Fecha de marcaje</th>
                                        <th>Dia de marcaje</th>
                                        <th style="width: 5px">Acciones</th>
                                    </tr>
                              </thead>
                                 <tbody>
                                    <?php foreach($filas as $fila):?>
                                    <tr class="odd gradeX">
                                    
                                          <td><?=$fila->nombre?></td>
                                          <td><?=$fila->fecha?></td>
                                          <td><?=$fila->dia?></td>
                                        <td>
                                            <div class="btn-group tooltip-demo">
                                                <a href="<?=site_url('marcaje/detalle/'.$fila->empleado_id.'/'.$fila->fecha)?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Detalles"><i class="fa fa-search"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existen marcajes registrados del departamento</span>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
  </div>
 