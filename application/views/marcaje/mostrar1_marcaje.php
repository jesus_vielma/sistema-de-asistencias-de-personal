<!DOCTYPE html>
<html>
<?php error_reporting(E_PARSE); ?>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CHAP - Marcaje</title>

    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet">
    
    <link href="<?=base_url('assets/css/plugins/toastr/toastr.min.css')?>" rel="stylesheet">

    <link href="<?=base_url('assets/css/animate.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">

</head>
<script type="text/javascript">
     var phpidioma   ='<?=setlocale(LC_TIME, 'es_VE.UTF-8') ?>';
    var phpDate      ='<?= date('Y-m-d') ?>';
    //var jsDate       = new Date(<?= $timestamp ?>);
    var phpDatei      ='<?= date('H:i:s',$timestamp) ?>';
    var phpDated      ='<?= strftime("%A") ?>';
   
 var phpDatei= $phpDatei;

</script>
<?php

    $timestamp = strtotime('now');
    
$fecha = array(
              'fecha'  => date('Y-m-d'),
        
            );
//$hora = array(
//              'hora' => date('H:i:s'),
//            );
 
$dia = array(
              'dia' => ucfirst(strftime("%A")),
            );
//var_dump($dia);
?>

<body class="gray-bg" ">

<noscript>
    <style type="text/css">
        #wrapper, #contenedor{display: none;}
        body {background-color: #f3f3f4;}
    </style>
<div class="middle-box text-center animated fadeInDown">
        <h1 style="margin-bottom: 20px; font-size: 160px;">Opps!</h1>
        <h3 class="font-bold">Hemos detectado un error</h3>

        <div class="error-desc">
            Hemos detectado que su navegador tiene deshabilitado Javascript.
            Por favor <strong>habilite javascript</strong> en sunavegador para poder hacer uso de CHAP.
            <div class="row">
                Para ver las instrucciones para habilitar javascript en su navegador, haga click en el boton <strong>Instrucciones</strong>,
                luego regresa a esta pagina y presiona el boton <strong>Inicio</strong>.<br>
                <a href="<?=site_url('home')?>" class="btn btn-success btn-lg"><i class="fa fa-home"></i> Inicio</a>
                <a href="http://www.enable-javascript.com/es/" class="btn btn-success btn-lg" target="_blank"><i class="fa fa-file"></i> Instrucciones</a>
            </div>
        </div>
    </div>
</noscript>
<!--<br><br><br>-->
<div id="contenedor">
    <div class="row animated fadeInDown">
    <div class="col-lg-8 col-lg-offset-2">
        <br>
        <div class="alert alert-info">
            <div class="row">
                <div class="col-lg-1">
                    <i class="fa fa-info-circle" style="font-size: 3em"></i>
                </div>
                <div class="col-lg-11">
                <p><strong>¿Cómo realizar el marcaje?<br></strong>
                Para realizar el marcaje deberá colocar su código de marcaje en la caja de texto, presione el botón la tecla ENTER. <br>
                Si el marcaje es exitoso se mostrara de nuevo esta pantalla, de lo contrario el sistema emitira una notificación la cual deberá leer y según lo indique realizar dicha acción.</p>
                </div>
            </div>
        </div>
    </div>
</div>
      <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h6 class="logo-name" style="color: #989898; " id="reloj"></h6>
            </div>
            <h3>Marcaje</h3>
            <p>Ingrese su codigo unico.</p>
        
           <?=validation_errors()?>
         <?=form_open('operacion_es/definir_horario',array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>
                <div class="form-group">
                 <?php  if($error!=NULL):?>
                    <div class="alert alert-danger">
                          <?php echo $error; ?>
                    </div>

                  <?php   endif;?> 
                   <?=form_label('Codigo unico:','codigo_empl')?> 
                  <input type="text" name="codigo_empl" class="form-control" required="required" onKeyUp="this.value=this.value.toUpperCase();" autofocus="autofocus">
                </div>
          
            
        <?=form_hidden($fecha)?>
      <input type="hidden" id="hora" name="hora">
      <?=form_hidden($dia)?>
          <!--<button type="submit" class="btn btn-primary block full-width m-b">Guardar</button>-->
        <?=form_close()?>
     

  
       
            <p class="m-t"> <small>CHAP Version 1 &copy; 2015</small> </p>
        
       
      
    <script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
    <!-- Jquery Validate -->
    <script src="<?=base_url('assets/js/plugins/validate/jquery.validate.min.js')?>"></script>
    
    <script>
        function show()
        {
            var Digital=new Date();
            var hours=Digital.getHours();
            var minutes=Digital.getMinutes();
            var seconds=Digital.getSeconds();
            var dn="am";
            var hoursN = hours;
            if (hours<10)
            hoursN = '0'+hours;
            if (hours>11 && hours!=12){
            dn="pm";
            hoursN='0'+(hours-12);
            }
            else
            {
               dn="pm";
            }
            if (hours==0)
            hoursN=12;
            if (minutes<=9)
            minutes="0"+minutes;
            else
            minutes = minutes;
            if (seconds<=9)
            seconds="0"+seconds;
            else
            seconds = seconds;
            var hora = hoursN+':'+ minutes+'<span style="font-size: 70px">'+dn+'</span>';
            var horas = hours+":"+minutes+':'+seconds;
            $('#reloj').html(hora);
            $('#hora').val(horas);
            setTimeout("show()",1)
        }
        show();
        
        function noCerrar() {
          return "Esta a punto de cerrar esta pagina. \nEsto puedo crear conflictos en el sistema si esta seguro confirme la salida, de lo contrarioa presione la tacla ESC.";
        }
    </script>
    </div>
</div>
</div> 
</html>