
        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Marcaje</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('marcaje/listar')?>">Listar Marcaje</a>
                        </li>
                        <li class="active">
                            <strong>Detalle del Marcaje</strong>
                        </li>
                    </ol>
                </div>
          </div>
          
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <div class="m-b-md">
                                    <h3><strong>Nombre del empleado: </strong> <?=$fila[0]->nombre?></h3>
                                    <strong>Fecha de marcaje:</strong>  <?=$fila[0]->fecha?> | <strong>Día de marcaje:</strong> <?=$fila[0]->dia?>
                                    <a href="<?=site_url('empleado/detalle/'.$fila[0]->empleado_id)?>" class="btn btn-white btn-sm pull-right">Información del empleado</a>
                                </div>       
                            </div>  
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">
                                            <dt>Tipo de marcaje:</dt> <dd><span class="label label-primary"><?=($fila[0]->tipo)?></span></dd>
                                            <dt>Hora de marcaje:</dt> <dd><?=$fila[0]->hora?></dd>
                                            <dt>Horas Extra:</dt> <dd><?=$fila[0]->tiempo_extra?></dd>
                                            <!--<dt>Tiempo extra de noche:</dt><dd><?=$fila[0]->tiempo_extra_noche?></dd> 
                                            <dt>Bono nocturno:</dt><dd><?=$fila[0]->bono_noc?></dd>-->
                                            <br><br><br><br>
                                            <dt>Fotografía al momento<br>de marcar</dt>
                                            <?php if($fila[0]->foto):?>
                                            <dd width="240" heigth="120"  >
                                            <img src="<?=base_url('assets/img/marcaje/'.$fila[0]->foto.'.jpg')?>" class="img-rounded img-responsive " >
                                            </dd>
                                            <?php else:?>
                                            <dd>Imagen no disponible</dd>
                                            <?php endif;?>
                                        </dl>
                                    </div>
                                    <?php if(isset($fila[1])):?>
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">
                                            <dt>Tipo de marcaje:</dt> <dd><span class="label label-primary "><?=$fila[1]->tipo?></span></dd>
                                            <dt>Hora de marcaje:</dt> <dd><?=$fila[1]->hora?></dd>
                                            <dt>Horas Extra:</dt> <dd><?=$fila[1]->tiempo_extra?></dd>
                                            <dt>Horas Extra Noche:</dt><dd><?=$fila[1]->tiempo_extra_noche?></dd> 
                                            <dt>Bono nocturno:</dt><dd><?=$fila[1]->bono_noc?></dd>
                                            <br><br>
                                            <dt>Fotografía al momento<br>de marcar</dt>
                                            <?php if($fila[0]->foto):?>
                                            <dd width="240" heigth="120"  >
                                            <img src="<?=base_url('assets/img/marcaje/'.$fila[1]->foto.'.jpg')?>" class="img-rounded img-responsive " >
                                            </dd>
                                            <?php else:?>
                                            <dd>Imagen no disponible</dd>
                                            <?php endif;?>
                                        </dl>  
                                    </div>
                                    <?php else:?>
                                    <div class="col-lg-5">
                                        <div class="alert alert-info">
                                            <p>El empleado aún no ha marcado su salida</p>
                                        </div>
                                    </div>
                                    <?php endif;?>
                                </div>
                              </div>
                           </div>
                    </div>
                    </div>
           

  