<?php
if($fila[0]->fecha_nac)
{
  $da= explode('-', $fila[0]->fecha_nac);   

   $dia = $da[2];  
    $mes = $da[1]; 
     $anio = $da[0];  

      $diac =date("d"); 
       $mesc =date("m"); 
       $anioc =date("Y"); 

      $edadac =  $anioc-$anio; 

   if($mesc < $mes && $diac < $dia || $mesc < $mes || $diac < $dia){ 

    $edad_aux = $edadac - 1; 

     $edad = $edad_aux; 
     }  
}
else
{
    $edad = "No se puede calcular la edad";
}
?>
          
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Empleados</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('empleado')?>">Empleados</a>
                        </li>
                        <li class="active">
                            <strong>Detalle</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="ibox ">
                    <?php if($fila!=0):?>
                    <div class="ibox-title">
                        <div class="m-b-md">
                            <a href="<?=site_url('empleado/editar/'.$fila[0]->empleado_id.'/1')?>" class="btn btn-white btn-sm pull-right">Editar información</a>
                            <h3><strong>Empleado</strong>: <?=$fila[0]->nombre?></h3>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                                <div class="col-lg-6">
                                    <dl class="dl-horizontal">
                                        <dt>Estado:</dt> <dd><span <?=($fila[0]->estatus)=='Activo' ? 'class="label label-primary " >Activo' : 'class="label label-danger">Inactivo'?></span></dd>
                                    </dl>
                                </div>
                                <div class="col-lg-6">
                                    <dl class="dl-horizontal">
                                        <dt>Tipo de contrato:</dt> <dd><span <?=($fila[0]->contrato)=='Fijo' ? 'class="badge badge-primary " > Fijo' : 'class="badge"><i class="fa fa-repeat fa-spin"></i> Contratado'?></span></dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <dl class="dl-horizontal">
                                        <dt>Código:</dt> <dd><?=$fila[0]->codigo_empl?></dd>
                                        <dt>Cédula:</dt> <dd><?=$fila[0]->cedula?></dd>
                                        <dt>Fecha de Nacimiento:</dt> <dd><?=($fila[0]->fecha_nac)!='' ? $fila[0]->fecha_nac : 'Debe agregar una fecha de nacimiento'?></dd>
                                        <dt>Edad:</dt> <dd><?=$edad?></dd>
                                        <!--<dt>Client:</dt> <dd><a href="#" class="text-navy"> Zender Company</a> </dd>-->
                                    </dl>
                                </div>
                                <div class="col-lg-7" id="cluster_info">
                                    <dl class="dl-horizontal" >
                                        <dt>Departamento Adjunto:</dt> <dd><?=$fila[0]->dep_nombre?></dd>
                                        <dt>Cargo:</dt> <dd><?=$fila[0]->cargo?></dd>
                                        <dt>Estación de Trabajo</dt><dd><?=$fila[0]->estacion?> </dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <dl class="dl-horizontal">
                                        <dt>Completed:</dt>
                                        <dd>
                                            <div class="progress progress-striped active m-b-sm">
                                                <div style="width: 60%;" class="progress-bar"></div>
                                            </div>
                                            <small>Project completed in <strong>60%</strong>. Remaining close the project, sign a contract and invoice.</small>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                    </div>
                    <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existen empleados registrados</span>
                            </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
            </div>