            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Horarios</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('empleado')?>">Horarios</a>
                        </li>
                        <li class="active">
                            <strong>Editar horario</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                
                <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3>Editar Horario</h3>
                    </div>
                    
                    <div class="panel-body">
                    
                    <?=form_open('horario/editar/'.$fila[0]->horario_id.'/2',array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>
                    <?=validation_errors();?>
                        <div class="form-group col-lg-6">
                            <?=form_label('Hora de Entrada:','hora_entrada')?>
                            <div class="input-append bootstrap-timepicker input-group">
                                 <input id="hora_entrada" class="form-control" type="text" name="hora_entrada" value="<?=(set_value('hora_entrada')!='' ? set_value('hora_entrada') : $fila[0]->hora_entrada)?>"/>
                                 <span class="input-group-btn">
                                     <button class="btn btn-info add-on" type="button"><i class="fa fa-clock-o"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-lg-6">
                            <?=form_label('Hora de Salida:','hora_salida')?>
                            <div class="input-append bootstrap-timepicker input-group">
                                 <input id="hora_salida" class="form-control" type="text" name="hora_salida" value="<?=(set_value('hora_salida')!='' ? set_value('hora_salida') : $fila[0]->hora_salida)?>"/>
                                 <span class="input-group-btn">
                                     <button class="btn btn-info add-on" type="button"><i class="fa fa-clock-o"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Nombre del Horario','nombre')?>
                            <?=form_input(array('id'=>'nombre','name'=>'nombre','value'=>(set_value('nombre')!='' ? set_value('nombre') : $fila[0]->nombre),'required'=>'required','class'=>'form-control'))?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Tipo de horario','tipo')?>
                            <br>
                            <?php if($fila[0]->tipo=='Normal'):?>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <label class="btn btn-primary btn-outline active">
                                  <input type="radio" name="tipo" id="contrato" checked autocomplete="off" value="Normal"> Normal
                                </label>
                                <label class="btn btn-default btn-outline">
                                  <input type="radio" name="tipo" id="tipo" autocomplete="off" value="12x12"> 12x12
                                </label>
                                <label class="btn btn-info btn-outline">
                                  <input type="radio" name="tipo" id="tipo" autocomplete="off" value="Pernocta"> Pernocta
                                </label>
                            </div>
                            <?php elseif($fila[0]->tipo=='12x12'):?>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <label class="btn btn-primary btn-outline ">
                                  <input type="radio" name="tipo" id="contrato" autocomplete="off" value="Normal"> Normal
                                </label>
                                <label class="btn btn-default btn-outline active">
                                  <input type="radio" name="tipo" id="tipo" checked autocomplete="off" value="12x12"> 12x12
                                </label>
                                <label class="btn btn-info btn-outline">
                                  <input type="radio" name="tipo" id="tipo" autocomplete="off" value="Pernocta"> Pernocta
                                </label>
                            </div>
                            <?php else:?>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <label class="btn btn-primary btn-outline ">
                                  <input type="radio" name="tipo" id="contrato" checked autocomplete="off" value="Normal"> Normal
                                </label>
                                <label class="btn btn-default btn-outline">
                                  <input type="radio" name="tipo" id="tipo" autocomplete="off" value="12x12"> 12x12
                                </label>
                                <label class="btn btn-info btn-outline active">
                                  <input type="radio" name="tipo" id="tipo" checked autocomplete="off" value="Pernocta"> Pernocta
                                </label>
                            </div>
                            <?php endif;?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Días','dias')?>
                            <br>
                            <?php if($fila[0]->dias=='L a V'):?>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <label class="btn btn-primary btn-outline active">
                                  <input type="radio" name="dias" id="contrato" checked autocomplete="off" value="L a V"> Lunes a Viernes
                                </label>
                                <label class="btn btn-default btn-outline">
                                  <input type="radio" name="dias" id="dias" autocomplete="off" value="X a D"> Miercoles a Domingo
                                </label>
                                <label class="btn btn-info btn-outline">
                                  <input type="radio" name="dias" id="dias" autocomplete="off" value="1D"> 1 día
                                </label>
                            </div>
                            <?php elseif($fila[0]->dias=='X a D'):?>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <label class="btn btn-primary btn-outline ">
                                  <input type="radio" name="dias" id="contrato" autocomplete="off" value="L a V"> Lunes a Vienes
                                </label>
                                <label class="btn btn-default btn-outline active">
                                  <input type="radio" name="dias" id="dias" checked autocomplete="off" value="X a D"> Miercoles a Domingo
                                </label>
                                <label class="btn btn-info btn-outline">
                                  <input type="radio" name="dias" id="dias" autocomplete="off" value="1D"> 1 día
                                </label>
                            </div>
                            <?php else:?>
                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                <label class="btn btn-primary btn-outline ">
                                  <input type="radio" name="dias" id="contrato" checked autocomplete="off" value="L a V"> Lunes a Vienes
                                </label>
                                <label class="btn btn-default btn-outline">
                                  <input type="radio" name="dias" id="dias" autocomplete="off" value="X a D"> Miercoles a Domingo
                                </label>
                                <label class="btn btn-info btn-outline active">
                                  <input type="radio" name="dias" id="dias" checked autocomplete="off" value="1D"> 1 día
                                </label>
                            </div>
                            <?php endif;?>
                        </div>
                        <div class="form-group">
                            <?=form_label('Descripción:','descripcion')?>
                            <?=form_textarea(array('id'=>'descripcion','name'=>'descripcion','class'=>'form-control','value'=>(set_value('descripcion')!='' ? set_value('descripcion') : $fila[0]->descripcion)))?>
                        </div>
                        <div class="row">
                            <?=form_hidden('horario_id',$fila[0]->horario_id)?>
                            <div class="col-lg-6 col-lg-offset-3    ">
                                <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                            </div>
                        
                        </div>
                    </div
                    <?=form_close()?>
                </div>
            </div>
        </div>
            </div>