            <div class="row wrapper border-bottom white-bg page-heading">   <!--migas de pan -->
                <div class="col-sm-4">
                    <h2>Horario</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li class="active">
                            <strong>Horario</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3>Horarios</h3>
                    </div>
                    <div class="panel-body">
                        <?php if($filas != 0 ):?>
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Hora de Entreda</th>
                                        <th>Hora de Salida</th>
                                        <th>Descripción</th>
                                        <th>Nombre</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($filas as $fila):?>
                                    <tr class="odd gradeX">
                                        <td><?=$fila->hora_entrada?></td>
                                        <td><?=$fila->hora_salida?></td>
                                        <td><?=$fila->descripcion?></td>
                                        <td><?=$fila->nombre?></td>
                                        <td>
                                            <div class="btn-group tooltip-demo">
                                                <!--<a href="<?=site_url('empleado/detalle/'.$fila->empleado_id)?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Detalles"><i class="fa fa-search"></i></a>-->
                                                <a href="<?=site_url('horario/editar/'.$fila->horario_id.'/1')?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="" data-original-title="Editar información"><i class="fa fa-pencil"></i></a>
                                                <!--<a href="<?=site_url('empleado/borrar/'.$fila->empleado_id)?>">Borrar</a>-->
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                                <!--<tfoot>
                                    <tr>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        <?php else :?>
                            <div class="alert alert-info">
                                <h3>Información</h3>
                                <span>No existen horarios registrados</span>
                            </div>
                        <?php endif;?>
                    </div>
                    <div class="panel-footer">
                        <a href="<?=site_url('horario/insertar')?>" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo horario</a>
                    </div>
                </div>
            </div>
        </div>
            </div>