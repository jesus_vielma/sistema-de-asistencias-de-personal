

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Empleados</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?=site_url('home')?>">Inicio</a>
                        </li>
                        <li>
                            <a href="<?=site_url('cargo')?>">Cargo</a>
                        </li>
                        <li class="active">
                            <strong>Crear cargo</strong>
                        </li>
                    </ol>
                </div>
                <!--
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
                -->
            </div>

            <div class="wrapper wrapper-content">
                
                <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3>Insertar cargo</h3>
                    </div>
                    
                    <div class="panel-body">
                    
                    <?=form_open('cargo/insertar/2',array('role'=>'form','id'=>'form','autocomplete'=>'off'))?>
                    <?=validation_errors();?>
                        <div class="form-group">
                                <?=form_label('Cargo:','cargo')?>
                                <?=form_input(array('id'=>'cargo','name'=>'cargo','class'=>'form-control','value'=>set_value('cargo'),'required'=>'required'))?>
                        </div>
                        <div class="form-group">
                             <?=form_label('Descripcion:','descripcion')?>
                            <?=form_textarea(array('id'=>'descripcion','name'=>'descripcion','class'=>'form-control','value'=>set_value('descripcion'),'required'=>'required'))?>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3">
                                <button type="submit" class="col-lg-6 btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                <button type="reset" class="col-lg-6 btn btn-danger"><i class="fa fa-times"></i> Borrar</button>
                            </div>
                        </div>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>
            </div>