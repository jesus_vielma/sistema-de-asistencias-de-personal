<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Fotos_Model extends CI_Model {

 
    public function grabarFoto($src,$id){
    
    
       return $this->db->insert('fotos',array('foto'=> $src,
       	                                       'es_id'=>$id,));
    }
   
   public function getLastFoto(){
     return $this->db->order_by('id_fotos','desc')->get('fotos')->row()->foto;
    }
    public function obtener_maxid()
    {
        $query = $this->db->select ('MAX(es.es_id) as id')
                                 ->get('es');
                         
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
}