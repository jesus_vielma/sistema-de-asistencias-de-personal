<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notificacion_Model extends CI_Model
{

  function __construct()
    {
        parent::__construct();
    }

function get(){

     $query = $this->db->select('fallas_empl.status as status, fallas_empl.falla_id as falla_id, fallas_empl.hora as hora, fallas_empl.fecha as fecha, fallas_empl.descripcion, fallas_empl.empleado_id, empleado.nombre')
                          ->join('empleado','empleado.empleado_id=fallas_empl.empleado_id')
                          ->where('fallas_empl.status = ', 'No leido')
                          ->order_by('fecha desc')
                          ->get('fallas_empl');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
        
      
    }


  
    function obtener_por_id_noti($_id){
        $query = $this->db->where('falla_id',$_id)->get('fallas_empl');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

     function obtener_por_departamento_id_noti($dep){

        $query = $this->db->select('fallas_empl.status_supervisor as status_supervisor, fallas_empl.falla_id, fallas_empl.empleado_id AS empleado_id, fallas_empl.hora as hora, fallas_empl.fecha as fecha,   fallas_empl.dia as dia,
                                    fallas_empl.tipo as tipo,  fallas_empl.descripcion, empleado.departamento_id as departamento_id,
                                    departamento.nombre AS dep_nombre, empleado.nombre as nombre')
                          ->join('empleado','fallas_empl.empleado_id = empleado.empleado_id','INNER')
                          ->join('departamento','empleado.departamento_id = departamento.departamento_id','INNER')
                          ->where('fallas_empl.status_supervisor  = ', 'No leido')
                         ->where('departamento.departamento_id = ', $dep)
                          ->get('fallas_empl');
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    function actualizar_status($_data){
        
        $data = array(
            'falla_id'            => $_data->post('falla_id'),
            'fecha'           => $_data->post('fecha'),
            'hora'        => $_data->post('hora'),
            'dia'       => $_data->post('dia'),
            'tipo'      => $_data->post('tipo'),
            'descripcion'       => $_data->post('descripcion'),
            'status'       => $_data->post('status'),
            'status_supervisor'       => $_data->post('status_supervisor'),
            'empleado_id'       => $_data->post('empleado_id'),
        );
        
        if($this->db->where('falla_id',$_data->post('falla_id'))->update('fallas_empl',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}
