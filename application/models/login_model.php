<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_Model extends CI_Model {
 
  function __construct() {
    parent::__construct();
  }
 
  function login($usr, $pass) {
    $this->db->where('usuario', $usr);
    $this->db->where('contrasena', $pass);
    $query = $this->db->get('usuario');
 
    if ($query->num_rows() == 0) :
    
      //usuario no existe
      return 0;
    else :
      //Usuario y contraseña correcta
      return 1;
    endif;
  }
  
  function obtener_rol($usr){
        $query = $this->db->select('usuario.usuario, usuario.niveles, empleado.departamento_id, cargo_empl.cargo, empleado.nombre,
                                   usuario.empleado_id')
                          ->join('empleado','usuario.empleado_id = empleado.empleado_id','INNER')
                          ->join('cargo_empl','empleado.cargo_id = cargo_empl.cargo_id','INNER')
                          ->where('usuario.usuario = ',$usr)
                          ->get('usuario');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
  

               

  function recuperar($email) {
    $this->db->where('email', $email);
    
    $query = $this->db->get('usuario');
 
    if ($query->num_rows()== 0) :
    
      //email no existe
      return 0;
    else :
      //Usuario y contraseña correcta
      return 1;
    endif;
  }
  
   function obtener_por_id($_id){
        $query = $this->db->where('email',$_id)->get('usuario');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
     function actualizar_contrasena($_data){
        
        $data = array(
            //'usuario'      => $_data->post('usuario'), //aqui va igual a los nombre de bd
            'contrasena'   => $_data->post('contrasena'),
            //'email'        => $_data->post('email'),
            'empleado_id'  => $_data->post('empleado_id')
        );
        
        if($this->db->where('usuario_id',$_data->post('usuario_id'))->update('usuario',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
  
    
}                 

