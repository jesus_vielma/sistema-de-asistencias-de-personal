<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Departamento_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function insertar_departamento($_data){
        $data = array(
            
            'nombre'            => $_data->post('nombre'),
            'responsable'       => $_data->post('responsable'),
            'descripcion'       => $_data->post('descripcion')
    
        );
        if($this->db->insert('departamento',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function obtener_todos(){
        $query = $this->db->order_by('nombre asc')->get('departamento');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_id($_id){
        $query = $this->db->where('departamento_id',$_id)->get('departamento');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function lastid(){
        $query = $this->db->select('MAX(departamento_id) AS departamento_id')
                          ->get('departamento');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    
    function obtener_departamento($_id){
        $query = $this->db->select  ('departamento.departamento_id AS departamento_id, departamento.nombre as nombre, departamento.supervisor as supervisor,
                                   departamento.coordinador as coordinador, departamento.descripcion as descripcion')
                          ->where('departamento_id = ',$_id)
                          ->get('departamento');


    
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
   

    function actualizar_departamento($_data){
        
        $data = array(
            'nombre'            => $_data->post('nombre'),
            'responsable'       => $_data->post('responsable'),
            'descripcion'       => $_data->post('descripcion'),
        );
        
        if($this->db->where('departamento_id',$_data->post('departamento_id'))->update('departamento',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function borrar_departamento($id){
        
        if($this->db->where('departamento_id',$id)->delete('departamento')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
}   