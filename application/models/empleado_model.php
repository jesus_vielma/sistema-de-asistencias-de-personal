<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Empleado_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function insertar_empleado($_data){
        $data = array(
            'codigo_empl'       => $_data->post('codigo_empl'),
            'nombre'            => ($_data->post('nombre')),
            'cedula'            => $_data->post('cedula'),
            'fecha_nac'         => $_data->post('fecha_nac'),
            'estatus'           => $_data->post('estatus'),
            'tipo_contrato'     => $_data->post('contrato'),
            'fecha_ingreso'     => $_data->post('fecha_ingreso'),
            'departamento_id'   => $_data->post('departamento_id'),
            'cargo_id'          => $_data->post('cargo_id'),
            'estacion_id'       => $_data->post('estacion_id')
        );
        if($this->db->insert('empleado',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function obtener_todos(){
        $query = $this->db->order_by('nombre ASC')
                          ->where('empleado_id <> 0')
                          ->where('estatus =', 'Activo')
                          ->get('empleado');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_id($_id){
        $query = $this->db->where('empleado_id',$_id)->get('empleado');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_departamento_id($_id){
        $query = $this->db->select('*')
                          ->where('departamento_id',$_id)
                          ->where('empleado_id <> 0')
                          ->where('estatus =', 'Activo')
                          ->order_by('nombre','ASC')
                          ->get('empleado');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_estatus($estatus,$empl){
        $where ="estatus = '$estatus' AND empleado_id <> 0 AND empleado_id <> '$empl'";
        $query = $this->db->where($where)->order_by('nombre','ASC')->get('empleado');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }    
    function obtener_empleado_con_estac_depto_cargo($_id){
        $query = $this->db->select('empleado.empleado_id AS empleado_id, empleado.nombre as nombre, empleado.cedula as cedula, empleado.codigo_empl as codigo_empl,
                                   empleado.fecha_nac as fecha_nac, empleado.fecha_ingreso as fecha_ingreso, empleado.estatus as estatus, empleado.tipo_contrato as contrato, cargo_empl.cargo as cargo, departamento.nombre AS dep_nombre,
                                   estacion.nombre AS estacion, empleado.imagen as imagen')
                          ->join('cargo_empl','empleado.cargo_id = cargo_empl.cargo_id','INNER')
                          ->join('departamento','empleado.departamento_id = departamento.departamento_id','INNER')
                          ->join('estacion','empleado.estacion_id = estacion.estacion_id','INNER')
                          ->where('empleado_id = ',$_id)
                          ->get('empleado');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_todos_con_depto_cargo(){
        $query = $this->db->select('empleado.empleado_id AS empleado_id, empleado.nombre as nombre, empleado.cedula as cedula, empleado.codigo_empl as codigo_empl,
                                   empleado.fecha_nac as fecha_nac, empleado.fecha_ingreso as fecha_ingreso, empleado.estatus as estatus, empleado.tipo_contrato as contrato,
                                   cargo_empl.cargo as cargo, departamento.nombre AS dep_nombre')
                          ->join('cargo_empl','empleado.cargo_id = cargo_empl.cargo_id','INNER')
                          ->join('departamento','empleado.departamento_id = departamento.departamento_id','INNER')
                          ->where('empleado_id <> 0 ')
                          ->order_by('empleado_id ASC')
                          ->get('empleado');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_todos_con_depto_cargo_por_dep($dep){
        $query = $this->db->select('empleado.empleado_id AS empleado_id, empleado.nombre as nombre, empleado.cedula as cedula, empleado.codigo_empl as codigo_empl,
                                   empleado.fecha_nac as fecha_nac, empleado.fecha_ingreso as fecha_ingreso, empleado.estatus as estatus, empleado.tipo_contrato as contrato,
                                   cargo_empl.cargo as cargo, departamento.nombre AS dep_nombre')
                          ->join('cargo_empl','empleado.cargo_id = cargo_empl.cargo_id','INNER')
                          ->join('departamento','empleado.departamento_id = departamento.departamento_id','INNER')
                          ->where('empleado.departamento_id = ',$dep)
                          ->where('empleado.empleado_id <> 0 ')
                          ->order_by('empleado_id ASC')
                          ->get('empleado');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function lastid(){
        $query = $this->db->select('MAX(empleado_id) AS id')
                          ->get('empleado');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function actualizar_empleado($_data){
        
        $data = array(
            'codigo_empl'       => $_data->post('codigo_empl'),
            'nombre'            => $_data->post('nombre'),
            'cedula'            => $_data->post('cedula'),
            'fecha_nac'         => $_data->post('fecha_nac'),
            'estatus'           => $_data->post('estatus'),
            'tipo_contrato'     => $_data->post('contrato'),
            'departamento_id'   => $_data->post('departamento_id'),
            'cargo_id'          => $_data->post('cargo_id'),
            'estacion_id'       => $_data->post('estacion_id')
            
        );
        
        if($this->db->where('empleado_id',$_data->post('empleado_id'))->update('empleado',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function direccion_imagen($imagen,$empl){
        $data = array(
          'imagen'   => $imagen
        );
        
        if($this->db->where('empleado_id',$empl)->update('empleado',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function borrar_empleado($id){
        
        if($this->db->where('empleado_id',$id)->delete('empleado')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function obtenerReporte($dep,$ini,$fin)
    {
        $where = "es.fecha between '$ini' AND '$fin' AND empleado.departamento_id='$dep'";
        $query = $this->db->select('es.es_id, empleado.nombre as empleado, es.fecha, es.tipo ,es.tiempo_extra, es.tiempo_extra_noche,
                                   departamento.nombre as dep')
                          ->join('es','es.empleado_id = empleado.empleado_id','INNER')
                          ->join('departamento','empleado.departamento_id = departamento.departamento_id','INNER')
                          ->where($where)
                          ->order_by('empleado.nombre, es.tipo, es.fecha','ASC')
                          ->get('empleado'); 
      if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    function obtener_supervisor($dep){
        $where = "empleado.departamento_id = '$dep' AND cargo_empl.cargo like 'SUPERVISOR' OR  empleado.departamento_id = '$dep' and cargo_empl.cargo like 'COORDINADOR'";
        $query = $this->db->select('empleado.nombre, cargo_empl.cargo')
                          ->join('cargo_empl','empleado.cargo_id = cargo_empl.cargo_id','INNER')
                          ->where($where)
                          ->get('empleado');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
}	
