<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class   Usuario_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function insertar_usuario($_data){
        $data = array(
            'usuario'      => $_data->post('usuario'), //aqui va igual a los nombre de bd
            'contrasena'   => $_data->post('contrasena'),
            'email'        => $_data->post('email'),
            'niveles'        => $_data->post('niveles'),
            'empleado_id'  => $_data->post('empleado_id')
            
        );
        if($this->db->insert('usuario',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function obtener_todos(){
        $query = $this->db->get('usuario');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    function obtener_por_id($_id){
        $query = $this->db->where('usuario_id',$_id)->get('usuario');
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    
    
    function actualizar_usuario($_data){
        
        $data = array(
            'usuario'      => $_data->post('usuario'), //aqui va igual a los nombre de bd
            'contrasena'   => $_data->post('contrasena'),
            'email'        => $_data->post('email'),
            'empleado_id'  => $_data->post('empleado_id')
        );
        
        if($this->db->where('usuario_id',$_data->post('usuario_id'))->update('usuario',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function borrar_usuario($id){
        
        if($this->db->where('usuario_id',$id)->delete('usuario')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}	
