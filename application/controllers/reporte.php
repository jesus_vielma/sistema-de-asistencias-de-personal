<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 * Controlador Reporte.
 *
 *
 * El controlador Reporte es el encargado de generar los archivos o reportes en PDF que emite el sistema.
 * 
 * 
 * @author Jesús Vielma
 * @package Controllers
 */
class Reporte extends Login{
    
    public function __construct()
    {
        parent::__construct();
        // Se carga el modelo alumno
        $this->load->model('Empleado_Model');
		$this->load->model('Marcaje_Model');
		$this->load->model('Departamento_Model');
		$this->load->model('Feriados_Model');
		$this->load->model('Asignacion_Model');
        // Se carga la libreria fpdf
        $this->load->library('pdf');
        date_default_timezone_set('America/Caracas');
	    setlocale(LC_ALL,'es_VE.UTF-8','es_VE','Windows-1252','esp','es_ES.UTF-8','es_ES');
		$this->pdf = new Pdf();
    }
        // El pie del pdf
		
    public function prenomina()
    {
		$this->check_session();
        $dep = $this->input->post('departamento_id');
		$fecha_ini = $this->input->post('fecha_ini');
		$fecha_fin = $this->input->post('fecha_fin');
		
		$arrayFechas = $this->periodo($fecha_ini,$fecha_fin);
		//echo "Fecha<br>";
		//echo "<pre>";
		//print_r($arrayFechas);
		//echo "</pre>";
		// Se obtienen los datos
		
		$empleados = $this->Empleado_Model->obtener_por_departamento_id($dep);
		//echo "Empleados";
		//echo "<pre>";
		//print_r($empleados);
		//echo "</pre>";
		
		foreach($empleados as $empleado)
		{
			for($fe=0;$fe<(count($arrayFechas));$fe++)
			{
				
				$marcajes = $this->Marcaje_Model->obtener_por_empleado_por_fecha($empleado->empleado_id,$arrayFechas[$fe]);
				//echo "Marcajes<br>";
				//echo "<pre>";
				//print_r($marcajes);
				//echo "</pre>";
			}
		}
        $alumnos = $this->Empleado_Model->obtenerReporte($dep,$fecha_ini,$fecha_fin);
		if($empleados == 0 )
		{
			$data['departamentos'] = $this->Departamento_Model->obtener_todos();
			$data['titulo'] = 'CHAP - Reportes';
			$data['controlador'] = 'Prenomina';
			$data['error'] = 'Se ha producido un error obteniendo los datos para la pre-nomina solicitada,
			por favor verifique que los datos son correctos e intentenlo de nuevo. <br><br>
			
			Consejos: Verifique que los empleados para el depatamento solicitado realizaron marcajes en la fecha solictada
			';
			
			$this->load->view('base/cabecera',$data);
			$this->load->view('base/pre_nomina');
			$this->load->view('base/pie');
		}
		else{
		$this->pdf->FPDF('L','mm','Letter');

        // Creacion del PDF
        /*
         * Se crea un objeto de la clase Pdf, recuerda que la clase Pdf
         * heredó todos las variables y métodos de fpdf
         */
        
        // Agregamos una página
        $this->pdf->AddPage();
        // Define el alias para el número de página que se imprimirá en el pie
        $this->pdf->AliasNbPages();
 
        /* Se define el titulo, márgenes izquierdo, derecho y
         * el color de relleno predeterminado
         */
        $this->pdf->SetTitle("Prenomina");
        $this->pdf->SetLeftMargin(10);
        $this->pdf->SetRightMargin(9);
        $this->pdf->SetFillColor(200,200,200);
 
        // Se define el formato de fuente: Arial, negritas, tamaño 9
        $this->pdf->SetFont('Times', 'B', 10);
        /*
         * TITULOS DE COLUMNAS
         *
         * $this->pdf->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
         */
        $this->pdf->Ln();
        $this->pdf->Cell(0,7,'PRE-NOMINA DE PERSONAL: '.utf8_decode($alumnos[0]->dep),'LTRB',0,'C');
        $this->pdf->Ln(7);
		foreach($alumnos as $fecha)
		{
			$fechai[] = $fecha->fecha;
		}
		//print_r($fechai);
		//echo count($fechai);
		$numFecha = explode('-',$fechai[0]);
		$feriados = $this->Feriados_Model->obtener_mes_ano($numFecha[1],$numFecha[0]);
		

		if($numFecha[2]=='01')
		$this->pdf->Cell(206,7,'1RA. QUINCENA DEL MES DE '.strtoupper(strftime('%B',strtotime($alumnos[0]->fecha))).' DEL '.$numFecha[0],'LTRB',0,'C');
		elseif($numFecha[2]=='16')
		$this->pdf->Cell(206,7,'2DA. QUINCENA DEL MES DE '.strtoupper(strftime('%B',strtotime($alumnos[0]->fecha))).' DEL '.$numFecha[0],'LTRB',0,'C');
		//$this->pdf->Cell(0,7,'QUINCENA DESDE '.$fechai[0].' HASTA '.$fechai[count($fechai)-1].' DEL MES DE '.strtoupper(strftime('%B',strtotime($alumnos[0]->fecha))).' DEL 2015','LTRB',0,'C');
        //$this->pdf->Cell(0,7,'2DA. QUINCENA DEL MES DE '.strtoupper(strftime('%B',strtotime(date('F')))).' DEL 2015','LTRB',0,'C');
        //$this->pdf->Ln(7);
        //$this->pdf->Cell(50,7,'Departamento Mecanico (Fijos)','LTRB',0);
		
		//if($fechai[0] == date('Y').'-'.date('m').'-01')
		//$this->pdf->Cell(206,7,'DIAS CORRESPONDIENTE A LA 1RA. QUINCENA DEL MES DE '.strtoupper(strftime('%B',strtotime($alumnos[0]->fecha))).' DEL 2015','LTRB',0,'C');
		//elseif($fechai[0] == date('Y').'-'.date('m').'-16')
		//$this->pdf->Cell(206,7,'DIAS CORRESPONDIENTE A LA 2DA. QUINCENA DEL MES DE '.strtoupper(strftime('%B',strtotime($alumnos[0]->fecha))).' DEL 2015','LTRB',0,'C');
        //$this->pdf->Cell(206,7,'DIAS CORRESPONDIENTE A LA 1ERA. QUINCENA DEL MES DE FEBRERO DEL 2015','LTRB',0,'C');
        $this->pdf->SetFont('Times', '', 10);
        //Se obtienen las coordenadas X & Y 
        $x = $this->pdf->GetX();
        $y = $this->pdf->GetY();
        
        $this->pdf->MultiCell(18.8,8.5,'Total Horas Extras','LTRB','C');
        
        //Se configura el eje
        $this->pdf->SetXY($x +18.8, $y);
        //$this->pdf->Ln();
        
        $this->pdf->MultiCell(18,8.5,'Total Dias Trabajados','LTRB','C');
    
        $this->pdf->Ln(-17);
        $this->pdf->SetXY($x +36.8, $y);
        
        $this->pdf->SetFont('Times', 'B', 7.5);
        $this->pdf->Cell(4.5,37,$this->RotatedText(255,79,'TOTAL LIBRE TRABAJADO',90),'LTRB','C');
        
        $this->pdf->SetXY($x +41.4, $y);
        
        $this->pdf->SetFont('Times', 'B', 7.5);
        $this->pdf->Cell(4.5,37,$this->RotatedText(260,73,'BONO DOMINICAL',90),'LTRB','C');
        
        $this->pdf->SetXY($x +46.2, $y);
        
        $this->pdf->SetFont('Times', 'B', 7.5);
        $this->pdf->Cell(4.5,37,$this->RotatedText(264.5,75,'BONO ALOJAMIENTO',90),'LTRB','C');
        
        $this->pdf->SetXY($x +51, $y);
        
        $this->pdf->SetFont('Times', 'B', 7.5);
        $this->pdf->Cell(3.5,37,$this->RotatedText(269.5,73,'BONO NOCTURNO',90),'LTRB','C');
        
        $this->pdf->SetFont('Times', 'B', 10);
        $this->pdf->Ln(7);
        $this->pdf->Cell(50,30,'Apellidos y Nombres','LTRB',0,'C');
    
        $b = count($fechai)-1;
		foreach($fechai as $fechaI)
		{
			$ex=explode('-',$fechaI);
			$dia[]= $ex[2];
		}
		$this->pdf->SetFont('Times', '', 7.5);
        for ($i=1;$i<=15;$i++)
        {
			if($dia[$i-1]==$feriados[0]->dia)
			{
				$this->pdf->SetFillColor(255,255,102);
				$this->pdf->Cell(156/15,5,utf8_decode(strftime('%A',strtotime($fechai[$i-1]))),'LTRB',0,'C',true);
				$this->pdf->SetFillColor(200,200,200);
			}
			else
			
            $this->pdf->Cell(156/15,5,utf8_decode(strftime('%A',strtotime($fechai[$i-1]))),'LTRB',0,'C');
			
        }
		//print_r($fechai);
        $this->pdf->Ln();
        $this->pdf->Cell(50);
        $this->pdf->SetFont('Times', '', 10);
		
		for ($i=0;$i<=14;$i++)
		{	
		
			if($dia[$i]==$feriados[0]->dia)
			{
				$this->pdf->SetFillColor(255,255,102);
				$this->pdf->Cell(156/15,5,$dia[$i],'LTRB',0,'C',true);
				$this->pdf->SetFillColor(200,200,200);
			}
			else
			$this->pdf->Cell(156/15,5,$dia[$i],'LTRB',0,'C');
			
		}
		
        $this->pdf->Ln();
        $this->pdf->Cell(50);
        //$this->pdf->Rotate(0);
        // Hora Extras
        $v = 0;
        $this->pdf->SetFont('Times', '', 8);
        for($j=1;$j<=15;$j++)
        { 
            for ($i=1;$i<=1;$i++)
            {
                if($dia[$j-1]==$feriados[0]->dia)
				{
					$this->pdf->SetFillColor(255,255,102);
					
					$this->pdf->Cell(3.466,20,$this->RotatedText(62.5+$v,73,'H.E.E',90),'LTRB',0,'C',true);
					$this->pdf->Cell(3.466,20,$this->RotatedText(66+$v,73,'H.E.S',90),'LTRB',0,'C',true);
					$this->pdf->Cell(3.466,20,$this->RotatedText(69.5+$v,73,'H.E.N',90),'LTRB',0,'C',true);
					$this->pdf->SetFillColor(200,200,200);
				}
				else
					{$this->pdf->Cell(3.466,20,$this->RotatedText(62.5+$v,73,'H.E.E',90),'LTRB',0,'C');
					$this->pdf->Cell(3.466,20,$this->RotatedText(66+$v,73,'H.E.S',90),'LTRB',0,'C');
					$this->pdf->Cell(3.466,20,$this->RotatedText(69.5+$v,73,'H.E.N',90),'LTRB',0,'C');}
            }
            $v = $v+10.4;
        }
        //Total Horas Extras    
        $this->pdf->SetFont('Times', '', 10);
        $this->pdf->Cell(18.8/3,20,$this->RotatedText(220,73,'H.E.E',90),'LTRB',0,'C');
        $this->pdf->Cell(18.8/3,20,$this->RotatedText(226,73,'H.E.S',90),'LTRB',0,'C');
        $this->pdf->Cell(18.8/3,20,$this->RotatedText(233,73,'H.E.N',90),'LTRB',0,'C');
        
        //Total dias Trabajados
        $this->pdf->SetFont('Times', '', 9);
        $this->pdf->Cell(18/5,20,$this->RotatedText(237.5,85.4-7,'REGULARES',90),'LTRB',0,'C');
        $this->pdf->Cell(18/5,20,$this->RotatedText(241,84-7,'FERIADOS',90),'LTRB',0,'C');
        $this->pdf->Cell(18/5,20,$this->RotatedText(244,82-7,'SABADOS',90),'LTRB',0,'C');
        $this->pdf->Cell(18/5,20,$this->RotatedText(248.3,83-7,'DOMINGO',90),'LTRB',0,'C');
        $this->pdf->Cell(18/5,20,$this->RotatedText(251,83-7,'',90),'LTRB',0,'C');
        
        $this->pdf->SetFont('Times', 'B', 10);
        $this->pdf->Ln(13);
        //$this->pdf->Cell(0,7,'PERSONAL OPERATIVO  - MANTENIMIENTO MECANICO. MIERCOLES A DOMINGOS.','LTRB',0,'C');
        
        $this->pdf->SetFont('Times', '', 8);
        
        $this->pdf->Ln(7);
        //Se imprimen los Empleados
        $num=1;
		for($q=0;$q<(count($alumnos));)
		{
			$this->pdf->Cell(4,5,$num,'LTRB');
			$this->pdf->SetFont('Times', '', 7);
			$this->pdf->Cell(46,5,utf8_decode($alumnos[$q]->empleado),'LTRB');
			$this->pdf->SetFont('Times', 'B', 6);
			$hee =0;
			$hes =0;
			$hen =0;
			$regulares=0;
			$sabados=0;
			$domingo=0;
			$feriados1 = 0;
			$libres = 0;
			$alojamiento= 0;
			$nocturno= 0;
			for($o=0;$o<=14;$o++)
			{
				if($dia[$o]==$feriados[0]->dia)
				{
					$this->pdf->SetFillColor(255,255,102);
					
					$this->pdf->Cell(3.466,5,$alumnos[$q+$o]->tiempo_extra,'LTRB',0,'l',true);
					$this->pdf->Cell(3.466,5,$alumnos[$o+15]->tiempo_extra,'LTRB',0,'l',true);
					$this->pdf->Cell(3.466,5,$alumnos[$o+15]->tiempo_extra_noche,'LTRB',0,'l',true);
					$this->pdf->SetFillColor(200,200,200);
				}
				else
					{$this->pdf->Cell(3.466,5,$alumnos[$q+$o]->tiempo_extra,'LTRB');
					$this->pdf->Cell(3.466,5,$alumnos[$o+15]->tiempo_extra,'LTRB');
					$this->pdf->Cell(3.466,5,$alumnos[$o+15]->tiempo_extra_noche,'LTRB');}
				/*$this->pdf->Cell(3.466,5,$alumnos[$q+$o]->tiempo_extra,'LTRB');
				$this->pdf->Cell(3.466,5,$alumnos[$o+15]->tiempo_extra,'LTRB');
				$this->pdf->Cell(3.466,5,$alumnos[$o+15]->tiempo_extra_noche,'LTRB');*/
				$hee = $hee + $alumnos[$q+$o]->tiempo_extra;
				$hes = $hes + $alumnos[$o+15]->tiempo_extra;
				$hen = $hen + $alumnos[$o+15]->tiempo_extra_noche;
				$fechaass = strftime('%u',strtotime($alumnos[$q+$o]->fecha));
				if($fechaass==1 || $fechaass==2 || $fechaass==3 || $fechaass==4 || $fechaass==5)
				{
					$regulares++;
				}
				elseif($fechaass==6)
				{
					$sabados++;
				}
				else
				{
					$domingo++;
				}
				$fechaass = strftime('%d',strtotime($alumnos[$q+$o]->fecha));
				foreach($feriados as $feriado)
				{
					if($fechaass==$feriado->dia)
					$feriados1++;
				}
			}
			$num=$num+1;
			$q+=30;
			$this->pdf->SetFont('Times', '', 9);
			$this->pdf->Cell(18.8/3,5,$hee,'LTRB',0,'C');
			$this->pdf->Cell(18.8/3,5,$hes,'LTRB',0,'C');
			$this->pdf->Cell(18.8/3,5,$hen,'LTRB',0,'C');
			$this->pdf->Cell(18/5,5,$regulares,'LTRB',0,'C');
			$this->pdf->Cell(18/5,5,$feriados1,'LTRB',0,'C');
			$this->pdf->Cell(18/5,5,$sabados,'LTRB',0,'C');
			$this->pdf->Cell(18/5,5,$domingo,'LTRB',0,'C');
			$this->pdf->Cell(18/5,5,'','LTRB',0,'C');
			$this->pdf->Cell(4.7,5,'','LTRB',0,'C');
			$this->pdf->Cell(4.5,5,$domingo,'LTRB',0,'C');
			$this->pdf->Cell(4.5,5,$alojamiento,'LTRB',0,'C');
			$this->pdf->Cell(4.3,5,$nocturno,'LTRB',0,'C');
			$this->pdf->SetFont('Times', '', 8);
			$this->pdf->Ln(5);
			
		}
		/*
        $this->pdf->SetFont('Times', 'B', 6);
        for($i=1;$i<=3*15;$i++)
        {
            $this->pdf->Cell(3.466,5,'0','LTRB','L');
        }
        $this->pdf->SetFont('Times', '', 9);
        for($i=1;$i<=3;$i++)
        {
            $this->pdf->Cell(6.67,5,'0','LTRB',0,'C');
        }
        
		$this->pdf->Cell(5.6,5,'15','LTRB',0,'C');
		$this->pdf->Cell(5.6,5,'0','LTRB',0,'C');
		for($i=1;$i<=3;$i++)
        {
            $this->pdf->Cell(5.6,5,'2','LTRB',0,'C');
        }
		
		$this->pdf->Cell(5.8,5,'0','LTRB',0,'C');
		$this->pdf->Cell(5.8,5,'2','LTRB',0,'C');
		$this->pdf->Cell(5.8,5,'2','LTRB',0,'C');
		$this->pdf->Cell(5.8,5,'0','LTRB',0,'C');
		$this->pdf->Cell(5.8,5,'0','LTRB',0,'C');
		
		*/
		
		//print_r($alumnos);
		$empl = array();
		foreach($alumnos as $emp)
		{
			$empl[] = $emp->empleado;
		}
		
		//echo "<pre>";
		//print_r($alumnos);
		//echo "</pre>";
		
		
        /*
 
        $this->pdf->Cell(15,7,'NUM','TBL',0,'C','1');
        $this->pdf->Cell(25,7,'PATERNO','TB',0,'L','1');
        $this->pdf->Cell(25,7,'MATERNO','TB',0,'L','1');
        $this->pdf->Cell(25,7,'NOMBRE','TB',0,'L','1');
        $this->pdf->Cell(40,7,'FECHA DE NACIMIENTO','TB',0,'C','1');
        $this->pdf->Cell(25,7,'GRADO','TB',0,'L','1');
        $this->pdf->Cell(25,7,'GRUPO','TBR',0,'C','1');
        $this->pdf->Ln(7);
        // La variable $x se utiliza para mostrar un número consecutivo
        $x = 1;
        foreach ($alumnos as $alumno) {
            // se imprime el numero actual y despues se incrementa el valor de $x en uno
            $this->pdf->Cell(15,5,$x++,'BL',0,'C',0);
            // Se imprimen los datos de cada alumno
            $this->pdf->Cell(25,5,utf8_decode($alumno->paterno),'B',0,'L',0);
            $this->pdf->Cell(25,5,$alumno->materno,'B',0,'L',0);
            $this->pdf->Cell(25,5,utf8_decode($alumno->nombre),'B',0,'L',0);
            $this->pdf->Cell(40,5,$alumno->fec_nac,'B',0,'C',0);
            $this->pdf->Cell(25,5,utf8_decode($alumno->grado),'B',0,'L',0);
            $this->pdf->Cell(25,5,$alumno->grupo,'BR',0,'C',0);
            //Se agrega un salto de linea
            $this->pdf->Ln(5);
        }*/
        /*
         * Se manda el pdf al navegador
         *
         * $this->pdf->Output(nombredelarchivo, destino);
         *
         * I = Muestra el pdf en el navegador
         * D = Envia el pdf para descarga
         *
         */
		
		$this->Footer($dep);
        $this->pdf->Output("PRENOMINA-".utf8_decode($alumnos[0]->dep)."-MES-".strtoupper(strftime('%B',strtotime($alumnos[0]->fecha))).".pdf", 'I');
		}
    }
	
	/**
	 * Función firma().
	 *
	 * La función firma recibe un paratamentro en el cual esta incluido el nombre de la persona que esta firmando el
	 * PDF
	 *
	 * @param string $dep Variable encargada de de recibir y guarda información obre quien firma el documento que
	 * se esta generando.
	 */
	public function firma($dep)
	{
		$this->pdf->SetY(-35);
		$this->pdf->SetFont('Times','I',8);
		
		//$apro = $this->Empleado_Model->obtener_supervisor($dep);
		$this->pdf->Cell(150);
		$this->pdf->Cell(60,10,'AUTORIZADO POR: '.$dep,'T',0);
		$this->pdf->Cell(80);
		
    }
	
	/**
	 * Función Leyenda().
	 *
	 * Esta función se encarga de imprimir una leyenda en el archivo de prenomina
	 */
	public function Leyenda()
	{
		$this->pdf->SetY(-50);
		$this->pdf->SetFont('Times','',9);
		$this->pdf->Cell(60,5,'Leyenda',1,1,'C');
		//$apro = $this->Empleado_Model->obtener_supervisor($dep);
		//$this->pdf->Cell(5);
		$this->pdf->Cell(10,5,'DNT',1,0,'C');
		$this->pdf->Cell(50,5,utf8_decode('Día No Trabajado'),1,1,'C');
		
		//$this->pdf->Cell(5);
		$this->pdf->Cell(10,5,'NM',1,0,'C');
		$this->pdf->Cell(50,5,utf8_decode('No Marco Salida'),1,1,'C');
		
		$this->pdf->SetFillColor(255,255,102);
		$this->pdf->Cell(10,5,'DLF',1,0,'C',true);
		$this->pdf->SetFillColor(0);
		$this->pdf->Cell(50,5,utf8_decode('Día Libre por Feriado'),1,1,'C');
		
		//$this->pdf->Cell(80);
    }
	
    function RotatedText($x,$y,$txt,$angle)
    {
        //Text rotated around its origin
        $this->pdf->Rotate($angle,$x,$y);
        $this->pdf->Text($x,$y,$txt);
        $this->pdf->Rotate(0);
    }
	
	/**
	 * Función relacion_nomina().
	 *
	 * Esta función es la encarga de realizar creación de un archivo PDF que contiene el resultado de la
	 * consulta a base de datos para extraer todos los empelados con parte de su información personal. 
	 */
	public function relacion_nomina()
	{
		$deps = $this->Departamento_Model->obtener_todos();
		
		//Se Cambian las preferecias del archivos en cuanto a tamañana orientacion 
		$this->pdf->FPDF('L','mm','Letter');
		
		$this->pdf->AddPage();
        // Define el alias para el número de página que se imprimirá en el pie
        $this->pdf->AliasNbPages();
		
		$this->pdf->SetTitle("Relación Nominal");
        //$this->pdf->SetLeftMargin(15);
        //$this->pdf->SetRightMargin(15);
		
		$this->pdf->SetFont('Arial', 'B', 10);
		$this->pdf->Ln(-2);
		$this->pdf->Cell(0,5,utf8_decode('MUKUMBARÍ SISTEMA TELEFÉRICO DE MÉRIDA'),0,1);
		$this->pdf->Cell(0,5,utf8_decode('RELACION NOMINA AL '.date('d-m-Y')),0,1);
		$this->pdf->Cell(0,5,utf8_decode('PERSONAL MUKUMBARI STM'),0,1);
		
		$this->pdf->Ln(5);
		//Encabezado de la tabla
		$this->pdf->SetFont('Times', 'B', 8);
		$this->pdf->SetFillColor(255,0,0);
		$this->pdf->SetTextColor(255,255,255);
		$this->pdf->Cell(20,10,'CODIGO',1,0,'C',true);
		$this->pdf->Cell(80,10,'NOMBRES Y APELLIDOS',1,0,'C',true);
		$this->pdf->Cell(20,10,'CEDULA',1,0,'C',true);
		$this->pdf->Cell(55,10,'CARGO',1,0,'C',true);
		$this->pdf->Cell(70,10,'COORDINACION DE ADSCRIPCION',1,0,'C',true);
		$this->pdf->Cell(15,10,'INGRESO',1,1,'C',true);
		
		foreach($deps as $dep){
			$empleados = $this->Empleado_Model->obtener_todos_con_depto_cargo_por_dep($dep->departamento_id);
			
			$this->pdf->SetFillColor(255,255,102);
			$this->pdf->SetTextColor(0,0,0);
			$this->pdf->SetFont('Times', 'B', 10);
			
			$this->pdf->Cell(260,7,utf8_decode($dep->nombre),1,1,'C',true);
			
		$this->pdf->SetFont('Times', '', 10);
		$this->pdf->SetTextColor(0,0,0);
		$this->pdf->SetFillColor(255,255,255);
		foreach($empleados as $empleado)
		{
			$this->pdf->SetFont('Times', '', 9);
			$this->pdf->Cell(20,7,$empleado->codigo_empl,1,0,'C');
			$this->pdf->SetFont('Times', '', 10);
			$this->pdf->Cell(80,7,utf8_decode($empleado->nombre),1,0,'C');
			$this->pdf->Cell(20,7,$empleado->cedula,1,0,'C');
			$this->pdf->SetFont('Times', '', 6);
			//$x = $this->pdf->GetX();
			//$y = $this->pdf->GetY();
			$this->pdf->Cell(55,7,utf8_decode($empleado->cargo),1,0,'C');
			//$this->pdf->SetXY($x +55, $y);
			$this->pdf->Cell(70,7,utf8_decode($empleado->dep_nombre),1,0,'C');
			//$this->pdf->SetXY($x +125, $y);
			$this->pdf->SetFont('Times', '', 8);
			$this->pdf->Cell(15,7,$empleado->fecha_ingreso,1,1,'C');			
		}
		}
		$this->pdf->Output('Relacion-Nomina-'.date('Y-m-d'),'I');
	}
	
	/**
	 * Función relacion_nomina_dep().
	 *
	 * Esta función es muy parecida a la función relacion nomina, pero con una diferencia, esta creara
	 * un PDF con todos los empleados en un departamento especificio.
	 *
	 * @param int $dep Recibe el codigo del departamento para poder realizar la consulta con la cual
	 * se extraen los emplados de un departamento.
	 */
	public function relacion_nomina_dep($dep)
	{
		$empleados = $this->Empleado_Model->obtener_todos_con_depto_cargo_por_dep($dep);
		
		//Se Cambian las preferecias del archivos en cuanto a tamañana orientacion 
		$this->pdf->FPDF('P','mm','Letter');
		
		$this->pdf->AddPage();
        // Define el alias para el número de página que se imprimirá en el pie
        $this->pdf->AliasNbPages();
		
		$this->pdf->SetTitle("Relación Nominal");
        //$this->pdf->SetLeftMargin(15);
        //$this->pdf->SetRightMargin(15);
		
		$this->pdf->SetFont('Arial', 'B', 10);
		$this->pdf->Ln(-2);
		$this->pdf->Cell(0,5,utf8_decode('MUKUMBARÍ SISTEMA TELEFÉRICO DE MÉRIDA'),0,1);
		$this->pdf->Cell(0,5,utf8_decode('RELACION NOMINA AL '.date('d-m-Y')),0,1);
		$this->pdf->Cell(0,5,utf8_decode($empleados[0]->dep_nombre),0,1);
		
		$this->pdf->Ln(5);
		//Encabezado de la tabla
		$this->pdf->SetFont('Times', 'B', 8);
		$this->pdf->SetFillColor(255,0,0);
		$this->pdf->SetTextColor(255,255,255);
		$this->pdf->Cell(5,10,utf8_decode('Nº'),1,0,'C',true);
		$this->pdf->Cell(60,10,'NOMBRES Y APELLIDOS',1,0,'C',true);
		$this->pdf->Cell(15,10,'CEDULA',1,0,'C',true);
		$this->pdf->Cell(46,10,'CARGO',1,0,'C',true);
		$this->pdf->Cell(56,10,'COORDINACION DE ADSCRIPCION',1,0,'C',true);
		$this->pdf->Cell(15,10,'INGRESO',1,1,'C',true);
		
		
		$this->pdf->SetFont('Times', '', 8);
		$this->pdf->SetTextColor(0,0,0);
		$this->pdf->SetFillColor(255,255,255);
		$a = 1;
		foreach($empleados as $empleado)
		{
			$this->pdf->SetFont('Times', '', 6);
			//$this->pdf->Cell(5,7,$empleado->empleado_id,1,0,'C');
			$this->pdf->Cell(5,7,$a++,1,0,'C');
			$this->pdf->SetFont('Times', '', 7);
			$this->pdf->Cell(60,7,utf8_decode($empleado->nombre),1,0,'C');
			$this->pdf->Cell(15,7,$empleado->cedula,1,0,'C');
			$this->pdf->SetFont('Times', '', 5);
			$x = $this->pdf->GetX();
			$y = $this->pdf->GetY();
			$this->pdf->MultiCell(46,7,utf8_decode($empleado->cargo),1,'C');
			$this->pdf->SetXY($x +46, $y);
			$this->pdf->MultiCell(56,7,utf8_decode($empleado->dep_nombre),1,'C');
			$this->pdf->SetXY($x +102, $y);
			$this->pdf->SetFont('Times', '', 8);
			$this->pdf->Cell(15,7,$empleado->fecha_ingreso,1,1,'C');			
		}
		
		$this->pdf->Output('Relacion-Nomina-'.$empleados[0]->dep_nombre.'-'.date('Y-m-d'),'I');
	}
	
	/**
	 * Funcion relacionN().
	 *
	 * Esta función es la encarga de reconocer cual de las funciones de relación nomina es necesario usar.
	 * Esto es realizado cuando re recibe de un formulario el parametro del departamento.
	 */
	public function relacionN()
	{
		$this->check_session();
		$dep = $this->input->post('departamento_id');
		if($dep==0)
			$this->relacion_nomina();
		else
			$this->relacion_nomina_dep($dep);
	}
	
	/**
	 * Función listado().
	 *
	 * La funcion listado se encarga de imprimir un listado de todos los empleados con sus respectivos codigos
	 * de marcaje.
	 */
	public function listado()
	{
		
		$deps = $this->Departamento_Model->obtener_todos();
		$this->pdf->FPDF('P','mm','Letter');
		$this->pdf->AddPage();
		// Define el alias para el número de página que se imprimirá en el pie
		$this->pdf->AliasNbPages();
		
		$this->pdf->SetTitle("Relación Nominal");
		//$this->pdf->SetLeftMargin(15);
		//$this->pdf->SetRightMargin(15);
		
		$this->pdf->SetFont('Times', 'B', 10);
		$this->pdf->Ln(-2);
		$this->pdf->Cell(0,5,utf8_decode('CODIGOS PARA MARCAJE INGRESO/EGRESO DE LA EMPRESA'),0,1,'C');
		$this->pdf->Ln(5);
		//Encabezado de la tabla
		$this->pdf->SetFont('Times', 'B', 10);
		$this->pdf->SetFillColor(255,0,0);
		$this->pdf->SetTextColor(255,255,255);
		$this->pdf->Cell(17);
		$this->pdf->Cell(25,10,'CODIGO',1,0,'C',true);
		$this->pdf->Cell(120,10,'NOMBRES Y APELLIDOS',1,0,'C',true);
		$this->pdf->Cell(20,10,'CEDULA',1,1,'C',true);
			
		foreach($deps as $dep)
		{
			$empleados = $this->Empleado_Model->obtener_por_departamento_id($dep->departamento_id);

			$this->pdf->SetFillColor(255,255,102);
			$this->pdf->SetTextColor(0,0,0);
			$this->pdf->SetFont('Times', 'B', 10);
			$this->pdf->Cell(17);
			$this->pdf->Cell(165,7,utf8_decode($dep->nombre),1,1,'C',true);
			
			
			$this->pdf->SetFont('Times', '', 10);
			$this->pdf->SetTextColor(0,0,0);
			$this->pdf->SetFillColor(255,255,255);
			$a = 1;
			
			foreach($empleados as $empleado)
			{
				$this->pdf->SetFont('Times', '', 10);
				$this->pdf->Cell(17);
				//$this->pdf->Cell(5,7,$empleado->empleado_id,1,0,'C');
				$this->pdf->Cell(25,7,$empleado->codigo_empl,1,0,'C');
				$this->pdf->Cell(120,7,utf8_decode($empleado->nombre),1,0,'C');
				$this->pdf->Cell(20,7,$empleado->cedula,1,1,'C');
				
			}
		}
		$this->pdf->Output('Instructivo','I');
	}
	
	/**
	 * Función pre_nomina().
	 *
	 * Esta funcion se encarga de hacer el llamado a la vista que sera la encarga de enviar los datos
	 * para generar el archivo PDF de la Prenomina
	 */
	public function pre_nomina()
	{
		$data['departamentos'] = $this->Departamento_Model->obtener_todos();
		$data['titulo'] = 'CHAP - Reportes';
		$data['controlador'] = 'Prenomina';
		$data['error'] = '';
		
		$this->load->view('base/cabecera',$data);
		$this->load->view('base/pre_nomina');
		$this->load->view('base/pie');
		
	}
	
	/**
	 * Función periodo
	 *
	 * Esta funcion se encarga de realizar un arreglo de fechas con el cual se determina el periodo de tiempo
	 * que debera ser consultado en la prenomina o en otros archivos.
	 *
	 * @param $fechaInicio Fecha de inicio de la consulta
	 * @param $fechaFin Fecha de fin de la consulta
	 *
	 * @return array $arrayFechas
	 */
	function periodo($fechaInicio, $fechaFin)
	{
		$arrayFechas=array();
		$fechaMostrar = $fechaInicio;
		
		while(strtotime($fechaMostrar) <= strtotime($fechaFin)) {
		$arrayFechas[]=$fechaMostrar;
		$fechaMostrar = date("Y-m-d", strtotime($fechaMostrar . " + 1 day"));
		}
		/*
		echo "<pre>";
		print_r($arrayFechas);
		echo "</pre>";*/
		
		return $arrayFechas;
	}
	
	/**
	 * Función prenomina2()
	 *
	 * Esta funcion es la encargada de generar el archivo PDF de prenomina
	 */
	public function prenomina2()
	{
		$this->check_session();
        $dep = $this->input->post('departamento_id');
		$fecha_ini = $this->input->post('fecha_ini');
		$fecha_fin = $this->input->post('fecha_fin');
		
		$arrayFechas = $this->periodo($fecha_ini,$fecha_fin);
		//echo "Fecha<br>";
		//echo "<pre>";
		//print_r($arrayFechas);
		//echo "</pre>";
		// Se obtienen los datos
		
		$empleados = $this->Empleado_Model->obtener_por_departamento_id($dep);
		$depinfo = $this->Departamento_Model->obtener_por_id($dep);
		//echo "Empleados";
		//echo "<pre>";
		//print_r($empleados);
		//echo "</pre>";
		$this->pdf->FPDF('L','mm','Letter');

        // Creacion del PDF
        /*
         * Se crea un objeto de la clase Pdf, recuerda que la clase Pdf
         * heredó todos las variables y métodos de fpdf
         */
        
        // Agregamos una página
        $this->pdf->AddPage();
        // Define el alias para el número de página que se imprimirá en el pie
        $this->pdf->AliasNbPages();
 
        /* Se define el titulo, márgenes izquierdo, derecho y
         * el color de relleno predeterminado
         */
        $this->pdf->SetTitle("Prenomina");
        $this->pdf->SetLeftMargin(10);
        $this->pdf->SetRightMargin(9);
        $this->pdf->SetFillColor(200,200,200);
 
        // Se define el formato de fuente: Arial, negritas, tamaño 9
        $this->pdf->SetFont('Times', 'B', 10);
        // Titutlo del documento
        $this->pdf->Ln();
        $this->pdf->Cell(0,7,'PRE-NOMINA DE PERSONAL: '.utf8_decode($depinfo[0]->nombre),'LTRB',0,'C');
        $this->pdf->Ln(7);
		
		$this->pdf->Cell(206,7,'PRE-NOMINA PARA LA FECHA '.$fecha_ini.' HASTA '.$fecha_fin.'.','LTRB',0,'C');
		
		$this->pdf->SetFont('Times', '', 10);
        //Se obtienen las coordenadas X & Y 
        $x = $this->pdf->GetX();
        $y = $this->pdf->GetY();
        
        $this->pdf->MultiCell(18.8,8.5,'Total Horas Extras','LTRB','C');
        
        //Se configura el eje
        $this->pdf->SetXY($x +18.8, $y);
        //$this->pdf->Ln();
        
        $this->pdf->MultiCell(18,8.5,'Total Dias Trabajados','LTRB','C');
    
        $this->pdf->Ln(-17);
        $this->pdf->SetXY($x +36.8, $y);
        
        $this->pdf->SetFont('Times', 'B', 7.5);
        $this->pdf->Cell(4.5,37,$this->RotatedText(255,79,'TOTAL LIBRE TRABAJADO',90),'LTRB','C');
        
        $this->pdf->SetXY($x +41.4, $y);
        
        $this->pdf->SetFont('Times', 'B', 7.5);
        $this->pdf->Cell(4.5,37,$this->RotatedText(260,73,'BONO DOMINICAL',90),'LTRB','C');
        
        $this->pdf->SetXY($x +46.2, $y);
        
        $this->pdf->SetFont('Times', 'B', 7.5);
        $this->pdf->Cell(4.5,37,$this->RotatedText(264.5,75,'BONO ALOJAMIENTO',90),'LTRB','C');
        
        $this->pdf->SetXY($x +51, $y);
        
        $this->pdf->SetFont('Times', 'B', 7.5);
        $this->pdf->Cell(3.5,37,$this->RotatedText(269.5,73,'BONO NOCTURNO',90),'LTRB','C');
		
		$this->pdf->SetFont('Times', 'B', 10);
        $this->pdf->Ln(7);
        $this->pdf->Cell(50,30,'Apellidos y Nombres','LTRB',0,'C');
		
		foreach($arrayFechas as $arrayFecha)
		{
			$ex = explode('-',$arrayFecha);
			$dia[] = $ex[2];
		}
		
		$feriados = $this->Feriados_Model->obtener_mes_ano($ex[1],$ex[0]);
		//print_r($feriados);
		
		$this->pdf->SetFont('Times', '', 7.5);
		
		// Se imprime el día
        for ($i=0;$i<count($arrayFechas);$i++)
        {
			if(isset($feriados[1]->dia) && $dia[$i]==$feriados[0]->dia || isset($feriados[1]->dia) && $dia[$i]==$feriados[1]->dia || isset($feriados[2]->dia) && $dia[$i]==$feriados[2]->dia || isset($feriados[3]->dia) && $dia[$i]==$feriados[3]->dia)
			{
				$this->pdf->SetFillColor(255,255,102);
				$this->pdf->Cell(156/count($arrayFechas),5,utf8_decode(strftime('%A',strtotime($arrayFechas[$i]))),'LTRB',0,'C');
				$this->pdf->SetFillColor(200,200,200);
			}
			else
			
            $this->pdf->Cell(156/count($arrayFechas),5,utf8_decode(strftime('%A',strtotime($arrayFechas[$i]))),'LTRB',0,'C');
			
        }
		
		$this->pdf->Ln();
        $this->pdf->Cell(50);
        $this->pdf->SetFont('Times', '', 10);
		
		//Se imprime el numero del día
		for ($i=0;$i<count($arrayFechas);$i++)
		{	
		
			if(isset($feriados[1]->dia) && $dia[$i]==$feriados[0]->dia || isset($feriados[1]->dia) && $dia[$i]==$feriados[1]->dia || isset($feriados[2]->dia) && $dia[$i]==$feriados[2]->dia || isset($feriados[3]->dia) && $dia[$i]==$feriados[3]->dia)
			{
				$this->pdf->SetFillColor(255,255,102);
				$this->pdf->Cell(156/count($arrayFechas),5,$dia[$i],'LTRB',0,'C');
				$this->pdf->SetFillColor(200,200,200);
			}
			else
			$this->pdf->Cell(156/count($arrayFechas),5,$dia[$i],'LTRB',0,'C');
			
		}
		
		$this->pdf->Ln();
        $this->pdf->Cell(50);
        //$this->pdf->Rotate(0);
        // Hora Extras
        $v = 0;
        $this->pdf->SetFont('Times', '', 8);
        for($j=0;$j<count($arrayFechas);$j++)
        { 
            for ($i=1;$i<=1;$i++)
            {
                if(isset($feriados[1]->dia) && $dia[$j]==$feriados[0]->dia || isset($feriados[1]->dia) && $dia[$j]==$feriados[1]->dia || isset($feriados[2]->dia) && $dia[$j]==$feriados[2]->dia || isset($feriados[3]->dia) && $dia[$j]==$feriados[3]->dia)
				{
					$this->pdf->SetFillColor(255,255,102);
					
					$this->pdf->Cell((156/count($arrayFechas))/3,20,$this->RotatedText(62.5+$v,73,'H.E.E',90),'LTRB',0,'C');
					$this->pdf->Cell((156/count($arrayFechas))/3,20,$this->RotatedText(66+$v,73,'H.E.S',90),'LTRB',0,'C');
					$this->pdf->Cell((156/count($arrayFechas))/3,20,$this->RotatedText(69.5+$v,73,'H.E.N',90),'LTRB',0,'C');
					$this->pdf->SetFillColor(200,200,200);
				}
				else
					{$this->pdf->Cell((156/count($arrayFechas))/3,20,$this->RotatedText(62.5+$v,73,'H.E.E',90),'LTRB',0,'C');
					$this->pdf->Cell((156/count($arrayFechas))/3,20,$this->RotatedText(66+$v,73,'H.E.S',90),'LTRB',0,'C');
					$this->pdf->Cell((156/count($arrayFechas))/3,20,$this->RotatedText(69.5+$v,73,'H.E.N',90),'LTRB',0,'C');}
            }
            $v = $v+(156/count($arrayFechas));
        }
		//Total Horas Extras    
        $this->pdf->SetFont('Times', '', 10);
        $this->pdf->Cell(18.8/3,20,$this->RotatedText(220,73,'H.E.E',90),'LTRB',0,'C');
        $this->pdf->Cell(18.8/3,20,$this->RotatedText(226,73,'H.E.S',90),'LTRB',0,'C');
        $this->pdf->Cell(18.8/3,20,$this->RotatedText(233,73,'H.E.N',90),'LTRB',0,'C');
        
        //Total dias Trabajados
        $this->pdf->SetFont('Times', '', 9);
        $this->pdf->Cell(18/5,20,$this->RotatedText(237.5,85.4-7,'REGULARES',90),'LTRB',0,'C');
        $this->pdf->Cell(18/5,20,$this->RotatedText(241,84-7,'FERIADOS',90),'LTRB',0,'C');
        $this->pdf->Cell(18/5,20,$this->RotatedText(244,82-7,'SABADOS',90),'LTRB',0,'C');
        $this->pdf->Cell(18/5,20,$this->RotatedText(248.3,83-7,'DOMINGO',90),'LTRB',0,'C');
        $this->pdf->Cell(18/5,20,$this->RotatedText(251,83-7,'',90),'LTRB',0,'C');
		
		$this->pdf->SetFont('Times', '', 8);
        
        $this->pdf->Ln(20);
		
		$num = 1;
		foreach($empleados as $empleado)
		{
			$this->pdf->Cell(4,5,$num,'LTRB');
			$this->pdf->SetFont('Times', '', 6);
			$this->pdf->Cell(46,5,utf8_decode($empleado->nombre),'LTRB');
			$this->pdf->SetFont('Times', 'B', 6);
			$hee =0;
			$hes =0;
			$hen =0;
			$regulares=0;
			$sabados=0;
			$domingo=0;
			$feriados1 = 0;
			$libres = 0;
			$alojamiento= 0;
			$nocturno= 0;
			for($fe=0;$fe<(count($arrayFechas));$fe++)
			{
				$marcajes = $this->Marcaje_Model->obtener_por_empleado_por_fecha($empleado->empleado_id,$arrayFechas[$fe]);
				$cm = count($marcajes);
				//if($cm==1)
				//{
				//	echo $empleado->nombre." ".count($marcajes)." <hr><br><pre>";
				//	print_r($marcajes);
				//	echo "</pre><br>";
				//	if(isset($marcajes[0]->tipo))
				//	{
				//		if($marcajes[0]->tipo=="Entrada")
				//		{
				//			echo "Entrada <br>";
				//		}
				//		else
				//		{
				//			echo "Salida <br>";
				//		}
				//	}
				//	
				//}
				//elseif($cm==2)
				//{
				//	echo $empleado->nombre." ".count($marcajes)."<br><pre>";
				//	print_r($marcajes);
				//	echo "</pre>";
				//}
				if(is_array($marcajes) || is_object($marcajes))
				{
					foreach($marcajes as $marcaje)
					{
						$fechaass = strftime('%u',strtotime($marcaje->fecha));
						if(isset($feriados[1]->dia) && $dia[$fe]==$feriados[0]->dia || isset($feriados[1]->dia) && $dia[$fe]==$feriados[1]->dia || isset($feriados[2]->dia) && $dia[$fe]==$feriados[2]->dia || isset($feriados[3]->dia) && $dia[$fe]==$feriados[3]->dia)
						{
							$this->pdf->SetFillColor(255,255,102);
							$cm = count($marcajes);
							if($cm == 1)
							{ //echo $empleado->nombre." Con feriados".$cm."<br>" ;
								//if(isset($marcaje->tipo))
								//{
									if($marcaje->tipo=='Entrada')
									{
										$this->pdf->Cell((156/count($arrayFechas))/3,5,$marcaje->tiempo_extra,'LTRB',0,'l',true);
										$hee = $hee + $marcaje->tiempo_extra;
										$feriados1++;
										
										
										if($fechaass==1 || $fechaass==2 || $fechaass==3 || $fechaass==4 || $fechaass==5)
										{
											$regulares++;
										}
										elseif($fechaass==6)
										{
											$sabados++;
										}
										else
										{
											$domingo++;
										}
									}
								//}
								//else
									//{
										$this->pdf->Cell((156/count($arrayFechas))/3,5,'N','LTRB',0,'l',true);
										//$hes = $hes + $marcaje->tiempo_extra;
										$this->pdf->Cell((156/count($arrayFechas))/3,5,'M','LTRB',0,'l',true);
										//$hen = $hen + $marcaje->tiempo_extra_noche;
									//}
									$this->pdf->SetFillColor(200,200,200);
							}
							elseif($cm == 2)
							{//echo $empleado->nombre." Con feriados ambos marcaje".$cm."<br>" ;
								if($marcaje->tipo=='Entrada')
								{
									$this->pdf->Cell((156/count($arrayFechas))/3,5,$marcaje->tiempo_extra,'LTRB',0,'l',true);
									$hee = $hee + $marcaje->tiempo_extra;
									$feriados1++;
									
									
									if($fechaass==1 || $fechaass==2 || $fechaass==3 || $fechaass==4 || $fechaass==5)
									{
										$regulares++;
									}
									elseif($fechaass==6)
									{
										$sabados++;
									}
									else
									{
										$domingo++;
									}
								}
								else
									{
										$this->pdf->Cell((156/count($arrayFechas))/3,5,$marcaje->tiempo_extra,'LTRB',0,'l',true);
										$hes = $hes + $marcaje->tiempo_extra;
										$this->pdf->Cell((156/count($arrayFechas))/3,5,$marcaje->tiempo_extra_noche,'LTRB',0,'l',true);
										$hen = $hen + $marcaje->tiempo_extra_noche;
									}
								$this->pdf->SetFillColor(200,200,200);
							}
						}
						else
							{
								if($cm == 1)
								{//echo $empleado->nombre." Sin feriados 1 marcaje".$cm."<br>" ;
									if($marcaje->tipo=='Entrada')
									{
										$this->pdf->Cell((156/count($arrayFechas))/3,5,$marcaje->tiempo_extra,'LTRB');
										$hee = $hee + $marcaje->tiempo_extra;
										
										if($fechaass==1 || $fechaass==2 || $fechaass==3 || $fechaass==4 || $fechaass==5)
										{
											$regulares++;
										}
										elseif($fechaass==6)
										{
											$sabados++;
										}
										else
										{
											$domingo++;
										}
									}
									
									// echo "Hola";
									//$this->pdf->SetFillColor(255,102,102);
										$this->pdf->Cell((156/count($arrayFechas))/3,5,'N','LTRB',0,'L');
										//$hes = $hes + $marcaje->tiempo_extra;
										$this->pdf->Cell((156/count($arrayFechas))/3,5,'M','LTRB',0,'L');
										//$hen = $hen + $marcaje->tiempo_extra_noche;
									
								}
								elseif($cm == 2)
								{//echo $empleado->nombre." Sin feriados 2 marcajes".$cm."<br>" ;
									if($marcaje->tipo=='Entrada')
									{
										$this->pdf->Cell((156/count($arrayFechas))/3,5,$marcaje->tiempo_extra,'LTRB');
										$hee = $hee + $marcaje->tiempo_extra;
										
										if($fechaass==1 || $fechaass==2 || $fechaass==3 || $fechaass==4 || $fechaass==5)
										{
											$regulares++;
										}
										elseif($fechaass==6)
										{
											$sabados++;
										}
										else
										{
											$domingo++;
										}
									}
									else
									{
										$this->pdf->Cell((156/count($arrayFechas))/3,5,$marcaje->tiempo_extra,'LTRB');
										$hes = $hes + $marcaje->tiempo_extra;
										$this->pdf->Cell((156/count($arrayFechas))/3,5,$marcaje->tiempo_extra_noche,'LTRB');
										$hen = $hen + $marcaje->tiempo_extra_noche;
									}
								}
								
							}
						
						//$fechaass = strftime('%d',strtotime($marcaje->fecha));
						////foreach($feriados as $feriado)
						////{
						////	if($fechaass==$feriado->dia)
						////	$feriados1++;
						////}
					}
				}
				else
					if(isset($feriados[1]->dia) && $dia[$fe]==$feriados[0]->dia || isset($feriados[1]->dia) && $dia[$fe]==$feriados[1]->dia || isset($feriados[2]->dia) && $dia[$fe]==$feriados[2]->dia || isset($feriados[3]->dia) && $dia[$fe]==$feriados[3]->dia)
					{
						$this->pdf->SetFillColor(255,255,102);
						$this->pdf->Cell((156/count($arrayFechas)),5,'DLF','LTRB',0,'C',true);
						//$this->pdf->Cell((156/count($arrayFechas))/3,5,'N','LTRB',0,'l',true);
						//$this->pdf->Cell((156/count($arrayFechas))/3,5,'T','LTRB',0,'l',true);
					}
					else
					{
						//$this->pdf->SetFillColor(153,153,255);
						$this->pdf->Cell((156/count($arrayFechas)),5,'DNT','LTRB',0,'C');
						//$this->pdf->Cell((156/count($arrayFechas))/3,5,'N','LTRB');
						//$this->pdf->Cell((156/count($arrayFechas))/3,5,'T','LTRB');
					}
			}
			$num++;
			$this->pdf->SetFont('Times', '', 9);
			$this->pdf->Cell(18.8/3,5,$hee,'LTRB',0,'C');
			$this->pdf->Cell(18.8/3,5,$hes,'LTRB',0,'C');
			$this->pdf->Cell(18.8/3,5,$hen,'LTRB',0,'C');
			$this->pdf->Cell(18/5,5,$regulares,'LTRB',0,'C');
			$this->pdf->Cell(18/5,5,$feriados1,'LTRB',0,'C');
			$this->pdf->Cell(18/5,5,$sabados,'LTRB',0,'C');
			$this->pdf->Cell(18/5,5,$domingo,'LTRB',0,'C');
			$this->pdf->Cell(18/5,5,'','LTRB',0,'C');
			$this->pdf->Cell(4.7,5,'','LTRB',0,'C');
			$this->pdf->Cell(4.5,5,$domingo,'LTRB',0,'C');
			$this->pdf->Cell(4.5,5,$alojamiento,'LTRB',0,'C');
			$this->pdf->Cell(4.3,5,$nocturno,'LTRB',0,'C');
			$this->pdf->SetFont('Times', '', 8);
			$this->pdf->Ln(5);
		}
		$this->firma($depinfo[0]->responsable);
		$this->Leyenda();
	$this->pdf->Output("PRENOMINA-".utf8_decode($depinfo[0]->nombre)."-MES-".strtoupper(strftime('%B',strtotime($arrayFechas[0])))."-".strtoupper(strftime('%G',strtotime($arrayFechas[0]))).".pdf", 'I');
	}
	
	/**
	 * Función detalle_empleado().
	 *
	 * Esta funcion se encarga de imprimir un archivo que contiene toda la información detallada de un empleado
	 *
	 * @param int $id identificador del empelado
	 */
	public function detalle_empleado($id)
	{
		$this->load->model('Observacion_Model');

		$info_empl =  $this->Empleado_Model->obtener_empleado_con_estac_depto_cargo($id);
		$asignaciones = $this->Asignacion_Model->obtener_por_empleado_id($id);
		$marcajes = $this->Marcaje_Model->obtener_por_empleado($id);
		$obsrvaciones = $this->Observacion_Model->obtener_por_empleado_id($id);
		if($info_empl[0]->fecha_nac)
		{
		  $da= explode('-', $info_empl[0]->fecha_nac);   
		
			$dia = $da[2];  
			$mes = $da[1]; 
			$anio = $da[0];  
		
			$diac =date("d"); 
			$mesc =date("m"); 
			$anioc =date("Y"); 
		
			$edadac=  $anioc-$anio; 
		
			if($mesc < $mes && $diac < $dia || $mesc < $mes || $diac < $dia){ 
		
			$edad_aux = $edadac - 1; 
		
			$edad = $edad_aux; 
			 }
			 else
				$edad = $edadac; 
		}
		else
		{
			$edad = "No se puede calcular la edad";
		}
		
		$this->pdf->FPDF('L','mm','Letter');
		$this->pdf->AddPage();
		// Define el alias para el número de página que se imprimirá en el pie
		$this->pdf->AliasNbPages();
		
		$this->pdf->SetTitle("Información detallada del empleado");
		$this->pdf->SetFont('Arial', '', 11);
		$this->pdf->Cell(130,7,'INFORMACION DEL EMPLEADO: ',0,0,'R');
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(120,7,utf8_decode($info_empl[0]->nombre),0,1,'L');
		$this->pdf->Ln(5);
		if($info_empl[0]->imagen!=NULL)
		{
			$this->pdf->Image(base_url('assets/img/empleados').'/'.$info_empl[0]->imagen,8,45,45);
			$this->pdf->Cell(45);
		}
		else
		$this->pdf->Cell(45,7,'El empleado no tiene fotografia',0,0);
		$this->pdf->Cell(102.5,7,'Estado del trabajador: '.$info_empl[0]->estatus,0,0,'C');
		$this->pdf->Cell(102.5,7,'Tipo de contrato: '.$info_empl[0]->contrato,0,1,'C');
		
		$this->pdf->Cell(45);
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(102.5/2,7,'Codigo de marcaje:',0,0,'R');
		$this->pdf->SetFont('Arial', '', 11);
		$this->pdf->Cell(102.5/2,7,$info_empl[0]->codigo_empl,0,0,'L');
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(102.5/2,7,'Deptarmento Adjunto:',0,0,'R');
		$this->pdf->SetFont('Arial', '', 7);
		$this->pdf->Cell(102.5/2,7,$info_empl[0]->dep_nombre,0,1,'L');
		
		$this->pdf->Cell(45);
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(102.5/2,7,utf8_decode('Cédula:'),0,0,'R');
		$this->pdf->SetFont('Arial', '', 11);
		$this->pdf->Cell(102.5/2,7,$info_empl[0]->cedula,0,0,'L');
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(102.5/2,7,'Cargo:',0,0,'R');
		$this->pdf->SetFont('Arial', '', 7);
		$this->pdf->Cell(102.5/2,7,$info_empl[0]->cargo,0,1,'L');
		
		$this->pdf->Cell(45);
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(102.5/2,7,'Fecha de nacimiento:',0,0,'R');
		$this->pdf->SetFont('Arial', '', 11);
		$this->pdf->Cell(102.5/2,7,$info_empl[0]->fecha_nac,0,0,'L');
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(102.5/2,7,utf8_decode('Estación de trabajo:'),0,0,'R');
		$this->pdf->SetFont('Arial', '', 9);
		$this->pdf->Cell(102.5/2,7,$info_empl[0]->estacion,0,1,'L');
		
		$this->pdf->Cell(45);
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(102.5/2,7,'Edad:',0,0,'R');
		$this->pdf->SetFont('Arial', '', 11);
		$this->pdf->Cell(102.5/2,7,$edad,0,0,'L');
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(102.5/2,7,'Fecha de Ingreso:',0,0,'R');
		$this->pdf->SetFont('Arial', '', 11);
		$this->pdf->Cell(102.5/2,7,$info_empl[0]->fecha_ingreso,0,1,'L');
		
		$this->pdf->Ln(20);
		if(!$marcajes)
		{
			$this->pdf->SetFont('Arial', 'B', 12);
			$this->pdf->SetTextColor(255,0,0);
			$this->pdf->Cell(0,7,utf8_decode('¡El empleado no ha realizado marcajes!'),0,1,'C');
		}
		else
		{
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(15);
		$this->pdf->Cell(240,7,'MARACAJES',1,1,'C');
		$this->pdf->Cell(15);
		$this->pdf->SetFont('Arial', 'B', 9.5);
		$this->pdf->Cell(40,7,'FECHA',1,0);
		$this->pdf->Cell(40,7,'HORA',1,0);
		$this->pdf->Cell(40,7,'DIA',1,0);
		$this->pdf->Cell(40,7,'TIPO',1,0);
		$this->pdf->Cell(40,7,'HORAS EXTRAS',1,0);
		$this->pdf->Cell(40,7,'HORAS EXTRAS NOCHE',1,1);
		
		$this->pdf->SetFont('Arial', '', 11);
		foreach($marcajes as $marcaje)
		{
			$this->pdf->Cell(15);
			$this->pdf->Cell(40,7,$marcaje->fecha,1,0);
			$this->pdf->Cell(40,7,$marcaje->hora,1,0);
			$this->pdf->Cell(40,7,utf8_decode($marcaje->dia),1,0);
			$this->pdf->Cell(40,7,$marcaje->tipo,1,0);
			$this->pdf->Cell(40,7,$marcaje->tiempo_extra,1,0);
			$this->pdf->Cell(40,7,$marcaje->tiempo_extra_noche,1,1);
		}
		}

		$this->pdf->Ln(20);
		if(!$obsrvaciones)
		{
			$this->pdf->SetFont('Arial', 'B', 12);
			$this->pdf->SetTextColor(255,0,0);
			$this->pdf->Cell(0,7,utf8_decode('¡El empleado no tiene observaciones!'),0,1,'C');
			$this->pdf->SetTextColor(0,0,0);
		}
		else
		{
		$this->pdf->SetFont('Arial', 'B', 11);
		$this->pdf->Cell(15);
		$this->pdf->Cell(200,7,'Justificativos, observaciones, y faltas',1,1,'C');
		$this->pdf->Cell(15);
		$this->pdf->SetFont('Arial', 'B', 9.5);
		$this->pdf->Cell(40,7,'CODIGO',1,0);
		$this->pdf->Cell(40,7,'FECHA',1,0);
		$this->pdf->Cell(40,7,'TIPO',1,0);
		$this->pdf->Cell(80,7,'REALIZADA POR',1,1);
		//$this->pdf->Cell(40,7,'TIPO',1,0);
		//$this->pdf->Cell(40,7,'HORAS EXTRAS',1,0);
		//$this->pdf->Cell(40,7,'HORAS EXTRAS NOCHE',1,1);
		

		$this->pdf->SetFont('Arial', '', 11);
		foreach($obsrvaciones as $obsrvacion)
		{
			$this->pdf->Cell(15);
			$this->pdf->Cell(40,7,$obsrvacion->codigo_justi,1,0);
			$this->pdf->Cell(40,7,$obsrvacion->fecha,1,0);
			$this->pdf->Cell(40,7,utf8_decode($obsrvacion->tipo),1,0);
			$this->pdf->Cell(80,7,utf8_decode($obsrvacion->nombre_supervisor),1,1);
			//$this->pdf->Cell(40,7,$marcaje->tiempo_extra,1,0);
			//$this->pdf->Cell(40,7,$marcaje->tiempo_extra_noche,1,1);
		}
		}
		$this->pdf->Output('Información detallada del empleado'.$info_empl[0]->nombre,'I');
		
	}
}
