 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *Controlador Marcaje.
 *
 *El controlador Marcaje se encarga de realizar la funciones de la entrada y salida del personal 
 *Con el fin de obtener, los retardos, horas extras y fecha de ingreso y egreso
 *necesarios llenar el formulario con el codigo unico.
 *@author Ericka Simancas
 *@package Controllers*/

class operacion_es extends CI_Controller{


    public function __construct()
    { 
	    parent::__construct();
	    date_default_timezone_set('America/Caracas');
	   
	   // $this->load->controllers('observacion');
	    $this->load->model('Marcaje_Model');
	   $this->load->model('Empleado_Model');
	    $this->load->model('Departamento_Model');
    }


   public function definir_horario()
	 {

	 	$data['titulo'] = 'CHAP - Marcaje';    
	    $cod = $this->input->post('codigo_empl');
	    $data= $this->Marcaje_Model-> obtener_por_codigoemple($cod);

	     if($data==0)
	     {
	         
	     $data1['error'] ="Error, Codigo incorrecto";
		 $this->load->view('marcaje/mostrar1_marcaje',$data1,  'refresh');

	     }elseif($data>0)
	     {

	     	  $id= $data[0]->empleado_id;
	     	  $nombre= $data[0]->nombre;
	     	  $data_asig = $this->Marcaje_Model-> asignacion($id); 
      	
	     	 if($data_asig==0)
		     {
		     	 $data1['error']=" Error, No tiene asignacion en el dia actual (Supervisor Inmediato) ";
				 $this->load->view('marcaje/mostrar1_marcaje',$data1,  'refresh');
				

		     }elseif($data_asig>0)
		     {  
		     	$turno=$data_asig[0]->turno;
		     	$horario=$data_asig[0]->nombre;
		     	$pernocta=$data_asig[0]->pernocta;
		     	$fecha_asig=$data_asig[0]->fecha;
		     	$hora_entrada=$data_asig[0]->hora_entrada;
		     	$hora_salida=$data_asig[0]->hora_salida;
				$tipo = $data_asig[0]->tipo;

                if($turno=='Libre'){
                	$tipo="Libre";
                }
                if(($horario=='Seguridad P. Turno 1')||($horario =='Seguridad P. Turno 2'))
	            $horario=0;
	            elseif(($horario!='Seguridad P. Turno 1')||($horario !='Seguridad P. Turno 2'))
	            $horario=1;


			    $inf= array('id' =>  $id, 'nombre'=>$nombre,'turno'=>$turno, 'hora_entrada'=>$hora_entrada, 'hora_salida'=> $hora_salida,
			   'fecha_asig'=> $fecha_asig);
                  
				switch($tipo)
				{
				case "12x12":
			    $this->horario_seguridad($inf);
				break;
				case "Normal":
				 $this->horario_normal($inf);
				break;

				case "Pernocta":
				 $this->horario_pernocta($inf);
				break;
				case "Libre":
				 $this->turno_libre($inf);
				break;

				}	   
	         }
	     }      
	 }
	 public function turno_libre($inf)
	 {

			$id= $inf['id'];
		    $nombre= $inf['nombre'];
		    $fecha= $this->input->post("fecha");
		    $dia = $this->input->post("dia");
		
		    $data_tipo = $this->Marcaje_Model-> obtener_tipo_por_fecha($fecha, $id);     
		    $mensaje= "Es su dia libre y esta marcando ".$nombre;   
	      
	       if($data_tipo==0)
	       {        
                $info_empleado= array('id' => $id, 'tipo' => 'Entrada','horaex' => '0.0','horaex_n'=> '0.0','bono_noc'=> 'No');                 
                $infor_insert=array('descripcion' => $mensaje, 'dia'=> $dia);              
                $this->proceso_insertar($info_empleado,$infor_insert);    
	   		}elseif($data_tipo!=0)
	   		{
	   	         $info_empleado= array('id' => $id, 'tipo' => 'Salida','horaex' => '0.0','horaex_n'=> '0.0','bono_noc'=> 'No');                 
                 $infor_insert=array( 'status' => 'No leido','status_supervisor' => 'No leido',
                 	          'descripcion' => $mensaje,'fecha'=>$fecha, 'dia'=> $dia); 
                //var_dump($infor_insert);
                 $this->proceso_insertar($info_empleado,$infor_insert);
	        }
     }

	 public function horario_seguridad($inf)
	{
	   $id= $inf['id'];
	   $nombre= $inf['nombre'];
	   $hora_entrada= $inf['hora_entrada'];
	   $hora_salida=$inf['hora_salida'];
	   $nombre= $inf['nombre'];
	   $turno_dia_actual=$inf['turno'];
	   $fecha = $this->input->post("fecha");
	   $dia = $this->input->post("dia");
       $data_tipo_anterior = $this->Marcaje_Model->obtener_dosultimas_asig($id);
       $data_tipo_es = $this->Marcaje_Model->obtener_dosultimas_es($id);
       //var_dump($data_tipo_es);
       $turno_anterior=$data_tipo_anterior[1]->turno;
       $hora_salida_ant=$data_tipo_anterior[1]->hora_salida;
       $bono_noc_es=$data_tipo_es[0]->bono_noc;
       $tipo_es=$data_tipo_es[0]->tipo;
       //var_dump($tipo_es);
       
     // var_dump($data_tipo_es);
      //var_dump($inf);
//	    var_dump($data_tipo_anterior);
//var_dump($turno_anterior);

		if(($turno_anterior=='Noche')&&($tipo_es=='Entrada')) //en esta parte indicar q si en el dia anterior la ultima insercion en (es) es entrada  y tiene bono_noc 
		                             //entonces ingresar aca, de lo contrario sino es asi ir a la entrada por noche
		{

          $data_tipo = $this->Marcaje_Model-> obtener_tipo_por_fecha($fecha, $id);
	      //var_dump($data_tipo);
		  $tipo = end($data_tipo); // Para saber el ultimo elemento del arreglo
		  //var_dump($tipo);
		  $tipo1=$tipo->tipo;
		  $inf_s= array('id' =>  $id, 'nombre'=>$nombre, 'dia'=>$dia, 'hora_salida'=> $hora_salida_ant,'bono_noc'=> 'No',);
          if(($data_tipo!=0)&&($tipo1=='Salida'))
			{
	           $data1['error']=$nombre." .Ya marco su Entrada y Salida. Si tiene algun inconveniente, consulte con su supervisor inmediato)";
	      	   $this->load->view('marcaje/mostrar1_marcaje', $data1, 'refresh'); 
			}else
			{

              $this->salida($inf_s,$data_tipo_anterior); 
		    }
		}elseif(($turno_anterior=='Noche')&&($tipo_es=='Salida')) //en esta parte indicar q si en el dia anterior la ultima insercion en (es) es entrada  y tiene bono_noc 
		                             //entonces ingresar aca, de lo contrario sino es asi ir a la entrada por noche
		{

          $data_tipo = $this->Marcaje_Model-> obtener_tipo_por_fecha($fecha, $id);
	      //var_dump($data_tipo);
		  $tipo = end($data_tipo); // Para saber el ultimo elemento del arreglo
		  //var_dump($tipo);
		  $tipo1=$tipo->tipo;
		  $inf_s= array('id' =>  $id, 'nombre'=>$nombre, 'dia'=>$dia, 'hora_salida'=> $hora_salida_ant,'bono_noc'=> 'No',);
          if(($data_tipo!=0)&&($tipo1=='Salida'))
			{
	           $data1['error']=$nombre." .Ya marco su Entrada y Salida. Si tiene algun inconveniente, consulte con su supervisor inmediato)";
	      	   $this->load->view('marcaje/mostrar1_marcaje', $data1, 'refresh'); 
			}else
			{
			  $inf= array('id' =>  $id, 'nombre'=>$nombre, 'dia'=>$dia, 'hora_entrada'=> $hora_entrada,
		      'bono_noc'=> 'Si',);

              $this->entrada($inf); 
		    }
		}elseif(($turno_anterior=='Día')||($turno_anterior=='Libre'))
		  {

		     $data_tipo = $this->Marcaje_Model-> obtener_tipo_por_fecha($fecha, $id);
             $tipo = end($data_tipo); // Para saber el ultimo elemento del arreglo

			 $tipo1=$tipo->tipo;
	        if(($data_tipo!=0)&&($tipo1=='Salida'))
			{
	           $data1['error']=$nombre." .Ya marco su Entrada y Salida. Si tiene algun inconveniente, consulte con su supervisor inmediato)";
	      	   $this->load->view('marcaje/mostrar1_marcaje', $data1, 'refresh'); 
			}else
		    {
		    	//var_dump($turno_dia_actual);
		    	//la alteracion de la fecha de asignacion va a ser en la salida ya que es el momento q salen las personas al dia siguiebte
				if ($turno_dia_actual=='Noche')
				    $bono_noc='Si';
				 else
				 	$bono_noc='No';
         

				 $inf= array('id' =>  $id, 'nombre'=>$nombre, 'dia'=>$dia, 'hora_entrada'=> $hora_entrada,
		        'bono_noc'=> $bono_noc,);
				// var_dump($data_tipo);
		         if($data_tipo==0)
		        // echo "entrada";
		         $data_tipo=0;
		       
		         elseif($data_tipo!=0)
		         $data_tipo=1;

		         switch($data_tipo)
		        {
		         case 0: //Entrada
		         $this->entrada($inf);
			     break;

			     case 1:

			       $inf_s= array('id' =>  $id, 'nombre'=>$nombre, 'dia'=>$dia, 'hora_salida'=> $hora_salida,'bono_noc'=> 'No',);
			     $this->salida($inf_s,$data_tipo_anterior); 
			     break;
		        }
		    }		
        }
	}
	public function horario_normal($inf)
	{
	   $id= $inf['id'];
	   $nombre= $inf['nombre'];
	   $hora_entrada= $inf['hora_entrada'];
	   $hora_salida=$inf['hora_salida'];
	   $nombre= $inf['nombre'];
	   $turno= $inf['turno'];
	   $fecha = $this->input->post("fecha");
	   $dia = $this->input->post("dia");

	    if ($turno=='Noche')
		    $bono_noc='Si';
	    else
 	    $bono_noc='No';


	    $data_tipo = $this->Marcaje_Model-> obtener_tipo_por_fecha($fecha, $id);     
//	    $tipo = end($data_tipo); 
//        $tipo1=$tipo->tipo;
		if($data_tipo!=0)
		{
			$tipo = $data_tipo[0]->tipo;
		}
		else
		$tipo = 0;



		if(($data_tipo!=0)&&($tipo=='Salida'))
		{
           $data1['error']=$nombre." .Ya marco su Entrada y Salida. Si tiene algun inconveniente, consulte con su supervisor inmediato)";
      	   $this->load->view('marcaje/mostrar1_marcaje', $data1, 'refresh'); 
		}else
	    {
		 $inf= array('id' =>  $id, 'nombre'=>$nombre, 'dia'=>$dia, 'hora_entrada'=> $hora_entrada,'hora_salida'=> $hora_salida,
        'bono_noc'=> $bono_noc,);
		 //var_dump($data_tipo);
         if($data_tipo=0)
         
         $data_tipo=0;
       
         //elseif($tipo1=='Entrada')
		 elseif($tipo=='Entrada')
         $data_tipo=1;

         switch($data_tipo)
        {
         case 0: //Entrada

         $this->entrada($inf);
	     break;

	     case 1: //Salida
	     $data_tipo_anterior=0;
	       $this->salida($inf,$data_tipo_anterior); 

	     break;
        }

     }
	}

 public function entrada($inf)
 {
			$id= $inf['id'];
			$nombre= $inf['nombre']; 
            $hora= date('H:i:s'); 
            $dia= $inf['dia']; 
            $hora_entrada= $inf['hora_entrada']; 
            $bono_noc= $inf['bono_noc'];
      
            
           

	         $hora= explode(':',$hora,3);
	         $hora_new=$hora[0].":".$hora[1].":".$hora[2]; 
	         $hora_entrada =$hora_entrada;        
             $hora2=$hora_entrada;
             $hora2=array();
             $hora2=explode(':',$hora_entrada,3);
             $hora_new2=$hora2[0].":".$hora2[1].":".$hora2[2];           
             $hora_h =(($hora[0])-($hora2[0]));
             $hora_min =(($hora[1])-($hora2[1]));
             $hora_seg =(($hora[2])-($hora2[2]));
		     $restraso = $hora_h.":".$hora_min.":".$hora_seg;
              
             $info_empleado= array('id' => $id,'tipo' => 'Entrada','horaex' => '0.0','horaex_n'=>'0.0','bono_noc'=> $bono_noc,'status' => 'No leido',);

            if($hora_h > 0)  
            {   
             $mensaje=$nombre." .Llego con  ".$hora_h." horas aproximadamente de Retraso. (Supervisor inmediato)";
            
            }elseif($hora_min >= 15)  
            {
             $mensaje=$nombre." .Llego con ".$hora_min." minutos de Retraso. (Supervisor inmediato)";                     
            }

            if( $hora[0]  < $hora2[0] )
	        {
   	          $hora_h_res= (($hora2[0])-($hora[0]));
       	      $hora_h_fi= (($hora_h_res)-1);
              $hora_res=((60)-($hora_min));
	              
	        if (($hora_res<10)&&($hora_h_fi>0))
     	  	  $hora_res="0".$hora_res;
          	  $mensaje= $nombre."  .Llego  ".$hora_h_fi.":".$hora_res."  antes de la hora de entrada";
              
              $info_empleado= array('id' => $id,'tipo' => 'Entrada','horaex' => $hora_h_fi,'horaex_n'=> '0.0','bono_noc'=> $bono_noc,);
             
	        if ($hora_res>=45)
             {      
               $hora_ex=(('1.0')+($hora_h_fi.'.0')); // Sacar las hor	                   
               $info_empleado= array('id' => $id,'tipo' => 'Entrada','horaex' => $hora_ex,'horaex_n'=> '0.0','bono_noc'=> $bono_noc,);
             }else 

            if (($hora_res>=30)&&($hora_res<=45))
            { 	
               $hora_ex=(('0.5')+($hora_h_fi.'.0'));
               $info_empleado= array('id' => $id, 'tipo' => 'Entrada','horaex' => $hora_ex,'horaex_n'=> '0.0','bono_noc'=> $bono_noc);                    
            }
	        }elseif(($hora_h==0)&&($hora_min<15))
	        {
                $mensaje= "Marcaje exitoso de ".$nombre;
            	$info_empleado= array('id' => $id, 'tipo' => 'Entrada','horaex' => '0.0','horaex_n'=> '0.0','bono_noc'=> $bono_noc);
            }
      
            if ($mensaje!=NULL)
            {
                  $infor_insert=array('descripcion' => $mensaje, 'dia'=> $dia); 
                  //var_dump($infor_insert);
                  //var_dump($info_empleado);
                 $this->proceso_insertar($info_empleado,$infor_insert);

            }   
  }

   public function salida($inf,$data)
   {
   	if($data==0){
   		$hora_salida=$inf['hora_salida'];
   	}else{

      $hora_salida=$data[1]->hora_salida;

   	}
   	        $id= $inf['id'];
			$nombre= $inf['nombre']; 
            $hora= date('H:i:s'); 
            //$hora='20:45:00';
            $dia= $inf['dia']; 
            $hora_salida= $inf['hora_salida'];          
            $bono_noc= $inf['bono_noc']; 
	        $hora= explode(':',$hora,3);
	        $hora_new=$hora[0].":".$hora[1].":".$hora[2]; 
	     
            $hora_noc_finall='0.0';	     
			$info_empleado= array('id' => $id,'tipo' => 'Salida','horaex' => '0.0','horaex_n'=>'0.0','bono_noc'=> $bono_noc, //'status' => 'No leido'
			 );


	         $hora_s=$hora_salida;
	         $hora_s=array();
	         $hora_s=explode(':',$hora_salida,3);
	         $hora_new_s=$hora_s[0].":".$hora_s[1].":".$hora_s[2];     
	         $hora_h_s =(($hora[0])-($hora_s[0]));
	         $hora_h_a =(($hora_s[0])-($hora[0]));
	         $hora_min =(($hora[1])-($hora_s[1]));
	         $hora_seg =(($hora[2])-($hora_s[2]));
	       
		      $hora_noc_final1='0.0'; //se inicializa la variable de esta manera
		     
		     
		     if (($hora[0]>=19)|| ($hora[0]<=00))
	            {   
	             
                  $hora_noc_final=(($hora[0])-(19));
                  $hora_noc_final1=(($hora_noc_final).'.0');
                  $info_empleado= array('id' => $id,'tipo' => 'Salida','horaex' => $hora_h_s,'horaex_n'=> $hora_noc_final1,'bono_noc'=> $bono_noc );
  
	            }
	           if($hora_h_s > 0)  
	            {     
	              $info_empleado= array('id' => $id,'tipo' => 'Salida','horaex' => $hora_h_s, 'horaex_n'=>$hora_noc_final1, 'bono_noc'=> $bono_noc);
	              $mensaje=$nombre."  .Esta saliendo ".$hora_h_s. " despues de la hora de salida que le corresponde";
	            	             
	            }

	         if (($hora_h_s ==0)&&($hora_min>=45))//esta saliendo despues de 45 min.
	            {

              	  $hora_ex=(($hora_h_s.'.0')+('1.0'));
	              $hora_ex_f_f=(($hora_ex)-($hora_noc_final1)); //se resta las horas extras de noche con las horas extras normales          
	              $hora_ex_final=($hora_ex_f_f.'.0'); //  var_dump($hora_ex_final)                                       
	              $info_empleado= array('id' => $id,'tipo' => 'Salida','horaex' => $hora_ex_final, 'horaex_n'=>$hora_noc_final1, 'bono_noc'=> $bono_noc);
          
	               $mensaje=$nombre."  .Esta marcando ".$hora_ex_f_f. " horas despues de la hora de salida que le corresponde";
					                    	
	             }elseif (($hora_h_s >0)&&($hora_min<30)&&($hora[0]!=19)) //Todo numero mayor a 19 y min menos de 45 entra aqui 
	             {
                  
	              $hora_h_s=(($hora_h_s)-($hora_noc_final1));
	           
	              $info_empleado= array('id' => $id,'tipo' => 'Salida','horaex' => $hora_h_s, 'horaex_n'=>$hora_noc_final1, 'bono_noc'=> $bono_noc);	                	
	              $mensaje=$nombre." .Esta marcando ".$hora_h_s." horas despues de su hora de salida";             
	          
	             }elseif(($hora_h_s>0)&&($hora[0]>19)&&($hora_min>=30)&&($hora_min<=44))
                 {
                   
                  $hora_h_s=(($hora_h_s)-($hora_noc_final1));
	              $hora_noc_finall=(($hora_noc_final1)+('0.5'));
	              $info_empleado= array('id' => $id,'tipo' => 'Salida','horaex' => $hora_h_s, 'horaex_n'=>$hora_noc_finall, 'bono_noc'=> $bono_noc);	                	
	              $mensaje=$nombre." .Esta marcando ".$hora_h_s." horas despues de su hora de salida"; 

                 }

	             elseif(($hora_h_s>0)&&($hora_min>=45)&&($hora[0]!=19))
	             {

	              $hora_h_ss=(($hora_h_s)-($hora_noc_final1));
	                
	               if ($hora_noc_final1!=0)
                   $hora_noc_finall=(($hora_noc_final1)+('1.0')); //
                  
                   if(($hora_min>=45)&&($hora_noc_finall=='0.0')) //En caso de que la salida sea en la mañana teniendo horas extras pero sin las horas extras nocturnas
                   {
                     $hora_h_ss=(($hora_h_ss)+('1.0'));
                   }
                   
                  $info_empleado= array('id' => $id,'tipo' => 'Salida','horaex' => $hora_h_ss, 'horaex_n'=>$hora_noc_finall, 'bono_noc'=> $bono_noc);	                	
	              $mensaje=$nombre." .Esta marcando ".$hora_h_s." horas despues de su hora de salida";
	             
	             }elseif(($hora_min>=45)&&($hora[0]==19))
                  {
                  	$hora_noc_final1=(($hora_noc_final1)+('1.0'));
                  	$mensaje=$nombre." .Esta marcando ".$hora_h_s." horas despues de su hora de salida";
                  	$info_empleado= array('id' => $id,'tipo' => 'Salida','horaex' => $hora_h_s, 'horaex_n'=>$hora_noc_final1, 'bono_noc'=> $bono_noc);
                 
                  }elseif(($hora_min==30)&&($hora[0]==19))
                  {
                  	
                  	$hora_noc_final1=(($hora_noc_final1)+('0.5'));
                  	$mensaje=$nombre." .Esta marcando ".$hora_h_s." horas despues de su hora de salida";
                  	$info_empleado= array('id' => $id,'tipo' => 'Salida','horaex' => $hora_h_s, 'horaex_n'=>$hora_noc_final1, 'bono_noc'=> $bono_noc);
                 
                  }
                  elseif(($hora_min==30)&&($hora_h_s<0)){

                 $mensaje=$nombre." .Salida antes de la hora que le corresponde. (Supervisor inmediato)";	

	             }
	             else if(($hora_min==30)&&($hora_min<=45))
	             {	
	               $hora_ex=(('0.5')+($hora_h_s.'.0'));
	                var_dump($hora_ex);
	                $info_empleado= array('id' => $id,'tipo' => 'Salida','horaex' => $hora_h_s, 'horaex_n'=>$hora_noc_final1, 'bono_noc'=> $bono_noc);

	               $mensaje=$nombre." .Marcaje despues de la hora";
	             }elseif(($hora_min=30)&&($hora_h_s<0)){

                 $mensaje=$nombre." .Salida antes de la hora que le corresponde. (Supervisor inmediato)";	

	             }elseif ( $hora_h_a>0)//revisar cuando se ingresa 00:45 no hace bien la oprecion y tener en cuenta cuando son horas extras de noche
	             {   
	               $mensaje=$nombre." .Salida antes de la hora que le corresponde. (Supervisor inmediato)";	
	             }elseif($hora[0]==$hora_s[0])
                 {
                  $mensaje=$nombre."   .Marcaje exitoso"; 
                  $info_empleado= array('id' => $id, 'tipo' => 'Salida','horaex' => '0.0','horaex_n'=> '0.0','bono_noc'=> $bono_noc);              	  
                 }


		        if ($mensaje!=NULL)
            	{

                  $infor_insert=array('descripcion' => $mensaje,'hora'=>$hora, 'dia'=> $dia); 
                  $this->proceso_insertar($info_empleado,$infor_insert);
                  //var_dump($info_empleado);
                  //var_dump($infor_insert);
                
     
                }
                


  }
    public function proceso_insertar($info_empleado,$inf2)
    {

		 $dia=$inf2['dia'];
		 $descripcion=$inf2['descripcion'];
		 $fecha = array('fecha'  => date('Y-m-d'),);
		 $dia= array('dia' => $dia, );
		 $hora = array('hora' => date('H:i:s'),);
		 $status = array('status' => 'No leido', );
		 $status_supervisor = array('status_supervisor' => 'No leido', );
		 $descripcion = array('descripcion' => $descripcion);

			?>

		 <?= form_open(base_url() . 'index.php/marcaje/insertar_notificacion') ?>          
		 <?= form_hidden($fecha)?> 
		 <?= form_hidden($hora)?> 
		 <?= form_hidden($dia)?> 
		 <?= form_hidden($info_empleado)?> 
		 <?= form_hidden($descripcion) ?>   
		 <?=form_hidden($status)?>
		 <?=form_hidden($status_supervisor)?>  
		                 <?php 
		 ?>
		<script src="<?=base_url('assets/js/jquery-2.1.1.js')?>"></script>
		   <script>
		    $(document).ready(function(){
		        setTimeout(function(){
		            $('form').submit();
		            },001)
		        })
		  </script> 

		 <?php
    }   
 
}
