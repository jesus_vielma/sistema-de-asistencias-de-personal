<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 *Controlador Observacion.
 *
 *El controlador Observacion se encarga de realizar la funciones CRUD (Create, Read, Update, Delete)
 *para el modulo de observaciones, justificaciones, y faltas. Así mismo contiene los métodos necesarios para obtener datos 
 *necesarios en los formularios.
 *
 *@author Jesus Vielma
 *@author Ericka Simancas
 *@package Controllers
 */
class Observacion  extends Login {


    public function __construct()
    { 
	    parent::__construct();
	    date_default_timezone_set('America/Caracas');
	
	    $this->load->model('Observacion_Model');
	    $this->load->model('Empleado_Model');
    }
    
	/**
	 * Funcion index().
	 *
	 * Utlizada para cargar por defecto la pagina de inicio del controlador, es decir esta función
	 * es la encargada de mostrar el listado de las observaciones, justificaciones, y faltas creadas para los empleados.
	 */
    public function index()
   {
	    $data['titulo'] = 'CHAP - Justificación';
	    $data['controlador'] = 'Listar';
	    $data['filas'] = $this->Observacion_Model->obtener_todos();
    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('observacion/index');
	    $this->load->view('base/pie');
    }
	/**
	 * Funcion insertar().
	 * 
	 * Funcion para realizar: la carga de las vista de inserción, validación de campos e insercion
	 * en base de datos
	 * @param int $phase Parametro que define en cual de fase se encunetra la función.
	 *
	 * @internal Si la $phase==1 se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos, por el contrario se mostrara
	 * de nuevo el formulario de inserciín con los errores correspondientes.
	 */
     public function insertar($phase=1)
    {
	$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');
   $this->check_session();
   $usuario['usuario']=$this->session->userdata('usuario')['nombre'];

    $this->load->library('form_validation');
	$data['empleados'] = $this->Empleado_Model->obtener_todos();
	$data['titulo'] = 'CHAP - Justificación';
    $data['controlador'] = 'Insertar';
	if ($phase==1) {
	    
	    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('observacion/insertar',$usuario);
	    $this->load->view('base/pie');
	}
	else
    {
	    $this->form_validation->set_rules('descripcion', 'Descripcion', 'required');
	    $this->form_validation->set_rules('tipo', 'Tipo', 'required');
	    $this->form_validation->set_rules('asistencia','Asistencia','required');
	    $this->form_validation->set_rules('empleado_id','Empleado','required');
   	   
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->insertar(1);
	    }
	    else
	    {
	    
		
		if ($this->Observacion_Model->insertar_observacion($this->input))
		{
		    redirect('observacion', 'refresh');
		}
		else
		{
		    redirect('observacion/insertar', 'refresh');   
		}
	    }
    }       
	}
	
	/**
	 * Funcion insertar_usu().
	 * 
	 * Funcion para realizar: la carga de las vista de inserción, validación de campos e insercion
	 * en base de datos
	 * @param int $phase Parametro que define en cual de fase se encunetra la función.
	 *
	 * @internal Si la $phase==1 se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos, por el contrario se mostrara
	 * de nuevo el formulario de inserciín con los errores correspondientes.
	 */
    public function insertar_usu($id,$phase=1)
    {
	$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');
    
   $this->check_session();
   $usuario['usuario']=$this->session->userdata('usuario')['nombre'];
   $id_empleado=array('empleado_id' => $id);

    $this->load->library('form_validation');
	$data['empleados'] = $this->Empleado_Model->obtener_por_id($id);
	$data['titulo'] = 'CHAP - Justificación';
    $data['controlador'] = 'Insertar';
	$data['controlador1'] = 'SummerNote';
	if ($phase==1) {
	    
	    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('observacion/insertar',$usuario);
	    $this->load->view('base/pie');
	}
	else
    {
	    $this->form_validation->set_rules('descripcion', 'Descripcion', 'required');
	    $this->form_validation->set_rules('tipo', 'Tipo', 'required');
	    $this->form_validation->set_rules('asistencia','Asistencia','required');
	    $this->form_validation->set_rules('empleado_id','Empleado','required');
   	   
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->insertar_usu($id, 1);
	    }
	    else
	    {
		if ($this->Observacion_Model->insertar_observacion($this->input))
		{
		    redirect('observacion', 'refresh');
		}
		else
		{
		    redirect('observacion/insertar', 'refresh');   
		}
	    }
    }       
	}

    /**
	 *Funcion editar().
	 *
	 * Función dedicada a la carga de los datos para la vista de edicion, validación de campos
	 * y modificación del registro en la base de datos
	 * @param int $id Identificador del empleado en la base de datos 
	 * @param inf $phase Parametro que define en cual de fase se encuentra la función.
	 *
	 * @internal Si la $phase==1 Se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos por el contrario se mostrara 
	 * de nuevo el formulario de inserción con los errores correspondientes.
	 */
     public function editar($id, $phase=1)
      { 
      	  $data['empleados'] = $this->Empleado_Model->obtener_todos();

			if ($phase==1)
			{
			    $data['fila'] = $this->Observacion_Model->obtener_por_id($id);
			    $data['titulo'] = 'CHAP - Justificación';
			    $data['controlador'] = 'Editar';

			    if($data['fila']==0)
			    {
				redirect('observacion','refresh');
			    }
			    
			    $this->load->view('base/cabecera',$data);
			    $this->load->view('observacion/editar');
			    $this->load->view('base/pie');
			}
			else
			{
				$this->form_validation->set_rules('descripcion', 'Descripcion', 'required');
			    $this->form_validation->set_rules('tipo', 'Tipo', 'required');
			    $this->form_validation->set_rules('asistencia','Asistencia','required');
			    $this->form_validation->set_rules('empleado_id','Empleado','required');
		   	   
		       if($this->form_validation->run()== FALSE )
		       	 {
				$this->editar($id, 1);
			    }
			    else
			    {

				    if ($this->Observacion_Model->actualizar_observacion($this->input))
				    {
					 redirect('observacion/index', 'refresh');
				    }
				    else
					{
					    redirect('observacion/editar/'.$id.'/1', 'refresh');   
					}
			    }
		    }
    
      }
     

  
   /* 
    public function borrar($id)
    {
        $this -> Observacion_Model -> borrar_observacion($id);
        redirect('observacion', 'refresh');
    }

  */
public function detalle($id)
   {
   	$data['fila']    =  $this->Observacion_Model->obtener_justificacion($id);
	$data['titulo'] = 'CHAP - Justificación';
	$data['controlador'] = 'Detalle';
    
	$this->load->view('base/cabecera',$data);
	$this->load->view('observacion/detalle');
	$this->load->view('base/pie');
    }
   
   public function detalle_print($id)
   {
   	$data['fila']    =  $this->Observacion_Model->obtener_justificacion($id);
	$data['titulo'] = 'CHAP - Observación';
	$data['controlador'] = 'Detalle';
	$this->load->view('observacion/observa_print',$data);


    }
}
