<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 *Controlador Asignación.
 *
 *
 *El controlador Asignación se encarga de realizar la funciones CRUD (Create, Read, Update, Delete)
 *para el modulo de asignaciones. Así mismo contiene los métodos necesarios para obtener datos 
 *necesarios en los formularios.
 *
 *@author Jesús Vielma
 *@package Controllers
 */
class Asignacion extends Login {

    public function __construct()
    { 
	    parent::__construct();
	    date_default_timezone_set('America/Caracas');
	    setlocale(LC_ALL,'es_VE.UTF-8','es_VE','Windows-1252','esp','es_ES.UTF-8','es_ES');
	    $this->load->model('Asignacion_Model');
	    $this->load->model('Empleado_Model');
	    $this->load->model('Horario_Model');
    }
    
	/**
	 * Funcion index().
	 *
	 * Utlizada para cargar por defecto la pagina de inicio del controlador, es decir esta función
	 * es la encargada de mostrar el listado de las asignaciones creadas para los empleados.
	 */
    public function index()
    {
	    $this->check_session();
		
	    $data['filas'] = $this->Asignacion_Model->obtener_todos_con_empleado_horario();
	    $data['titulo'] = 'CHAP - Asignacion';
	    $data['controlador'] = 'Listar';
	    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('asignacion/index');
	    $this->load->view('base/pie');
    }
    
	/**
	 * Funcion insertar().
	 * 
	 * Funcion para realizar: la carga de las vista de inserción, validación de campos e insercion
	 * en base de datos
	 * @param int $phase Parametro que define en cual de fase se encunetra la función.
	 *
	 * @internal Si la $phase==1 se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos, por el contrario se mostrara
	 * de nuevo el formulario de inserciín con los errores correspondientes.
	 */
    public function insertar($phase=1,$emple_id=0)
    {
		$this->check_session();
		$empl_id = $this->session->userdata('usuario')['id'];
		//$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');
		$data['horarios']   = $this->Horario_Model->obtener_todos();
		$data['titulo'] = 'CHAP - Asignacion';
		$data['controlador'] = 'Insertar';
		
		if($emple_id!=0) 
		{
			$data['empleado'] = $this->Empleado_Model->obtener_por_id($emple_id);
		}
		else
		{
			$data['empleado'] = '0';
			$data['empleados'] = $this->Empleado_Model->obtener_por_estatus('Activo',$empl_id);
		}
		
		
		if ($phase==1) {
			
			$this->load->view('base/cabecera',$data);
			$this->load->view('asignacion/insertar');
			$this->load->view('base/pie');
		}
		else
		{
			$this->form_validation->set_rules('fecha', 'Fecha', 'required');
			$this->form_validation->set_rules('pernocta', 'Pernocta', 'required');
			$this->form_validation->set_rules('trabajo','Trabajo','required');
			$this->form_validation->set_rules('dia','Día','required');
			$this->form_validation->set_rules('turno','Turno','required');
			$this->form_validation->set_rules('empleado_id','Empleado','required');
			$this->form_validation->set_rules('horario_id','Horario','required');
			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			
			if($this->form_validation->run()== FALSE )
			{
			$this->insertar(1,$emple_id);
			}
			else
			{
			
			if ($this->Asignacion_Model->insertar_asignacion($this->input))
			{
				redirect('asignacion', 'refresh');
			}
			else
			{
				redirect('asginacion/insertar', 'refresh');   
			}
			}
				
		}
    }
    /**
	 *Funcion editar().
	 *
	 * Función dedicada a la carga de los datos para la vista de edicion, validación de campos
	 * y modificación del registro en la base de datos
	 * @param int $id Identificador del empleado en la base de datos 
	 * @param inf $phase Parametro que define en cual de fase se encuentra la función.
	 *
	 * @internal Si la $phase==1 Se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos por el contrario se mostrara 
	 * de nuevo el formulario de inserción con los errores correspondientes.
	 *
	 * @internal Esta función no esta en funcionamiento puesto que no es correcto realizar la edición de una
	 * asignación 
	 */
    public function editar($id, $phase=1)
    {
		$this->check_session();
		
		if ($phase==1)
		{
			$data['fila'] = $this->Asignacion_Model->obtener_por_id($id);
			
			if($data['fila']==0)
			{
			redirect('asignacion/index','refresh');
			}
			
			$this->load->view('test/editar_asignacion',$data);
		}
		else
		{
			if ($this->Asignacion_Model->actualizar_asignacion($this->input))
			{
			redirect('asignacion/index', 'refresh');
			}
			else
			{
			redirect('asignacion/editar/'.$id.'/1', 'refresh');   
			}
		}
    }
    
	/**
	 *Funcion detalle().
	 *
	 *Función usada para mostrar la vista detallada de la información del empleado
	 *@param int $id Identificador del empleado, usado para realizar la consulta de la información del empleado
	 *
	 *@internal Este función no esta en funcionamiento puesto que no hay datos relevantes que mostrar
	 *que no se muestren en el index del controlador.
	 */
    public function detalle($id)
    {
		$data['fila']    =  $this->Asignacion_Model->obtener_por_id($id);
		$this->load->view('test/detalle_asignacion',$data);
    }
	
    /**
	 *Funcion borrar().
	 *
	 *Borra el registro de la asignación de la base de datos.
	 *@param int $id Identificador del empleado en la base de datos
	 */
    public function borrar($id)
    {
        $this->check_session();
		
		$this -> Asignacion_Model -> borrar_asignacion($id);
        redirect('asignacion', 'refresh');
    }
    
	/**
	 * Funcion get_date().
	 *
	 * La función ha sido creada para enviarle al formulario de inserción que día es la fecha indicada,
	 * es decir, si la persona intruoduce la fecha 2015-09-11 este método se encarga de escribir en el
	 * formulario que el 11 de septiembre de 2015 es un día viernes.
	 */
    public function get_date()
    {
		if($this->input->post('date'))
		{
			$date = $this->input->post('date');
			
			$fecha = ucfirst(strftime('%A',strtotime($date)));
		}
		
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
		{
			print_r(utf8_encode($fecha));
		}
		else
			{
				print_r($fecha);
			}
		
    }
}