<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controlador Webcam
 *
 * Este controlador se encarga de hacer el proceso de capturar una fotografia del la persona
 * que esta realizando el marcaje. Por otro lado tamben se encarga de subirla al servidor.
 *
 * @author Ericka Simanas.
 * @package Controllers
 *
 */
class Webcam extends CI_Controller
{

    public function __construct()
    { 
      parent::__construct();
      date_default_timezone_set('America/Caracas');
     $this->load->model('Fotos_Model');
    }

    /**
     * Funcion index().
     *
     * Función encarga de mostrar la vista donde muestra la camara para la toma de la fotografia
     */
    public function index()
    {
      $data['titulo'] = 'CHAP - webcam';

      $this->load->view('webcam/index_webcam');
        
    
    }
    
    /**
     * Función ajax().
     *
     * Esta funcion se encarga de generar un nombre que contiene la fecha y hora en la que la foto se esta tomando
     * así mismo se encarga decirle a la función de JavaScript donde debera subir la imagen y con que nombre.
     */
    public function ajax()
    {
      $id_foto=date('YmdHis');
      $id= $this->Fotos_Model->obtener_maxid();
      $_id =$id[0]->id;
       var_dump($_POST);
       $filename = "./assets/img/marcaje/".$id_foto.'.jpg';//nombre del archivo
      
      if($this->Fotos_Model->grabarFoto($id_foto,$_id))
      {
         
         $result = file_put_contents( $filename, file_get_contents('php://input') );
        
       }
        $filename_nuevo = "assets/img/marcaje/".$id_foto.'.jpg';//nombre del archivo
        //$url = 'http://localhost/sicap/' . $filename_nuevo;
        $url = base_url().$filename_nuevo;
         print "$url\n";


    }
  
    
}  