<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 *Controlador Cargo.
 *
 *
 *El controlador Cargo se encarga de realizar la funciones CRUD (Create, Read, Update, Delete)
 *para el modulo de Cargos. Así mismo contiene los métodos necesarios para obtener y entregar los
 *datos necesarios en los formularios.
 *
 *@author Joeinny Osorio
 *@package Controllers
 */
class Cargo extends Login {


    public function __construct()
    { 
	    parent::__construct();
	    
	    $this->load->model('Cargo_Model'); //como va en la clase del modelo
    }
    
	/**
	 * Funcion index().
	 *
	 * Utlizada para cargar por defecto la pagina de inicio del controlador, es decir esta función
	 * es la encargada de mostrar el listado de los cargos podrá tener un empleado.
	 */
    public function index()
    {
		$this->check_session();	
	    $data['titulo'] = 'CHAP - Cargos';
	    $data['controlador'] = 'Listar';
	    $data['filas'] = $this->Cargo_Model->obtener_todos();
	    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('cargo/index');
	    $this->load->view('base/pie');
    }
    
	/**
	 * Funcion insertar().
	 * 
	 * Funcion para realizar: la carga de las vista de inserción, validación de campos e insercion
	 * en base de datos
	 * @param int $phase Parametro que define en cual de fase se encunetra la función.
	 *
	 * @internal Si la $phase==1 se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos, por el contrario se mostrara
	 * de nuevo el formulario de inserciín con los errores correspondientes.
	 */
    public function insertar($phase=1)
    {
	$this->check_session();	
	$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');

	$data['titulo'] = 'CHAP - Cargos';
	$data['controlador'] = 'Insertar';
	
	if ($phase==1) {
	    
	    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('cargo/insertar');
	    $this->load->view('base/pie');
	}
	else
	{
	    $this->form_validation->set_rules('cargo', 'Cargo', 'required');
	    $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
	    	    
	    $this->form_validation->set_message('required', 'El campo %s  es requerido');
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->insertar(1);
	    }
	    else
	    {
		
		if ($this->Cargo_Model->insertar_cargo($this->input))
		{
		    redirect('cargo/index', 'refresh');
		}
		else
		{
		    redirect('cargo/insertar', 'refresh');   
		}
	    }
            
	}
    }
    /**
	 *Funcion editar().
	 *
	 * Función dedicada a la carga de los datos para la vista de edicion, validación de campos
	 * y modificación del registro en la base de datos
	 * @param int $id Identificador del empleado en la base de datos 
	 * @param inf $phase Parametro que define en cual de fase se encuentra la función.
	 *
	 * @internal Si la $phase==1 Se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos por el contrario se mostrara 
	 * de nuevo el formulario de inserción con los errores correspondientes.
	 */
    public function editar($id, $phase=1)
    {
    $this->check_session();
	if ($phase==1)
	{
	    $data['fila'] = $this->Cargo_Model->obtener_por_id($id);
	    $data['titulo'] = 'CHAP - Cargos';
	    $data['controlador'] = 'Editar';
	    if($data['fila']==0)
	    {
		redirect('cargo/index','refresh');
	    }
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('cargo/editar');
	    $this->load->view('base/pie');
	}
	else
	{
	    if ($this->Cargo_Model->actualizar_cargo($this->input))
	    {
		redirect('cargo/index', 'refresh');
	    }
	    else
	    {
		redirect('cargo/editar/'.$id.'/1', 'refresh');   
	    }
	}
    }
/*    
    public function detalle($id)
    {
	$data['fila']    =  $this->Cargo_Model->obtener_por_id($id);
	$this->load->view('test/detalle_cargo',$data);
    }*/
    
	/**
	 *Funcion borrar().
	 *
	 *Borra el registro de la asignación de la base de datos.
	 *@param int $id Identificador del empleado en la base de datos
	 */
    public function borrar($id)
    {
		$this->check_session();
        $this -> Cargo_Model -> borrar_cargo($id);
        redirect('cargo/index', 'refresh');
    }
}
