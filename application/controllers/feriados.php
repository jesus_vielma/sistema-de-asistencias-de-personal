<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 *Controlador Feriados.
 *
 *
 *El controlador Feriados se encarga de realizar la funciones CRUD (Create, Read, Update, Delete)
 *para el modulo de estación. Así mismo contiene los métodos necesarios para obtener datos 
 *necesarios para la carga del calendario.
 *
 *@author Fernando Araque 
 *@package Controllers
 */
class Feriados extends Login {


    public function __construct()
    { 
	    parent::__construct();
	    date_default_timezone_set('America/Caracas');
	    $this->load->model('Feriados_Model');
    }

    /**
	 * Funcion index().
	 *
	 * Utlizada para cargar por defecto la pagina de inicio del controlador, es decir esta función
	 * es la encargada de mostrar el listado de las asignaciones creadas para los empleados.
	 */
    public function index()
    {
		$this->check_session();
	    $data['titulo'] = 'CHAP - Feriados';
	    $data['controlador'] = 'Listar';
	    $data['filas'] = $this->Feriados_Model->obtener_todos();
    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('feriados/index');
	    $this->load->view('base/pie');
    }

	/**
	 * Funcion insertar().
	 * 
	 * Funcion para realizar: la carga de las vista de inserción, validación de campos e insercion
	 * en base de datos
	 * @param int $phase Parametro que define en cual de fase se encunetra la función.
	 *
	 * @internal Si la $phase==1 se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos, por el contrario se mostrara
	 * de nuevo el formulario de inserciín con los errores correspondientes.
	 */
    public function insertar($phase=1)
	{
	$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');
    
	$this->check_session();
	$data['titulo'] = 'CHAP - Feriados';
    $data['controlador'] = 'Insertar';
	if ($phase==1) {
	    
	    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('feriados/insertar');
	    $this->load->view('base/pie');
	}
	else
    {
	    $this->form_validation->set_rules('dia', 'Dia', 'required');
	    $this->form_validation->set_rules('mes', 'Mes', 'required');
	    $this->form_validation->set_rules('ano', 'Ano', 'required');
	    $this->form_validation->set_rules('descripcion','Descripcion','required');
   	   
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->insertar(1);
	    }
	    else
	    {
		
		if ($this->Feriados_Model->insertar_feriados($this->input))
		{
		    redirect('feriados/calendario', 'refresh');
		}
		else
		{
		    redirect('feriados/insertar', 'refresh');   
		}
	    }
    }       
	}
    
    /**
	 *Funcion editar().
	 *
	 * Función dedicada a la carga de los datos para la vista de edicion, validación de campos
	 * y modificación del registro en la base de datos
	 * @param int $id Identificador del empleado en la base de datos 
	 * @param inf $phase Parametro que define en cual de fase se encuentra la función.
	 *
	 * @internal Si la $phase==1 Se cargara la vista con los datos necesarios, por el contrario, se haran las validaciones
	 * de campos, si estas son correctas se hara la inserción en la base de datos por el contrario se mostrara 
	 * de nuevo el formulario de inserción con los errores correspondientes.
	 */
    public function editar($id, $phase=1)
     {
			$this->check_session();
			if ($phase==1)
			{
			    $data['fila'] = $this->Feriados_Model->obtener_por_id($id);
			    $data['titulo'] = 'CHAP - Feriados';
			    $data['controlador'] = 'Editar';
			
			    
			    if($data['fila']==0)
			    {
				redirect('feriados','refresh');
			    }
			    
			    $this->load->view('base/cabecera',$data);
			    $this->load->view('feriados/editar');
			    $this->load->view('base/pie');
			}
			else
			{
				 $this->form_validation->set_rules('dia', 'Dia', 'required');
			    $this->form_validation->set_rules('mes', 'Mes', 'required');
			    $this->form_validation->set_rules('ano', 'Ano', 'required');
			    $this->form_validation->set_rules('descripcion','Descripcion','required');
		   	   
		       if($this->form_validation->run()== FALSE )
		       	 {
				$this->editar(1,$id);
			    }
			    else
			    {

				    if ($this->Feriados_Model->actualizar_feriados($this->input))
				    {
					 redirect('feriados/index', 'refresh');
				    }
				    else
					{
					    redirect('feriados/editar/'.$id.'/1', 'refresh');   
					}
			    }
		    }
    
    }
  
    /**
	 *Funcion borrar().
	 *
	 *Borra el registro de la asignación de la base de datos.
	 *@param int $id Identificador del empleado en la base de datos
	 */
    public function borrar($id)
    {
        $this -> Feriados_Model -> borrar_feriados($id);
        redirect('feriados', 'refresh');
    }


	/**
	 *Funcion detalle().
	 *
	 *Función usada para mostrar la vista detallada de la información del empleado
	 *@param int $id Identificador del empleado, usado para realizar la consulta de la información del empleado
	 *
	 *@internal Este función no esta en funcionamiento puesto que no hay datos relevantes que mostrar
	 *que no se muestren en el index del controlador.
	 *@ignore
	 */
    public function detalle($id)
    {
	$this->check_session();
    $data['fila']    =  $this->Feriados_Model->obtener_feriados($id);
	$data['titulo'] = 'CHAP - Feriados';
	$data['controlador'] = 'Detalle';
    
	$this->load->view('base/cabecera',$data);
	$this->load->view('feriados/detalle');
	$this->load->view('base/pie');
    }
	
	/**
	 *Función calendario().
	 *
	 * Esta función se encarga de mostrar la vista del calendario segun la base de datos.
	 */
	public function calendario()
	{
		$this->check_session();
		$data['titulo'] = 'CHAP - Feriados';
		$data['controlador'] = 'Calendario';
		
		$this->load->view('base/cabecera',$data);
		$this->load->view('feriados/calendario');
		$this->load->view('base/pie');
	}
	
	
	/**
	 *Función fijos().
	 *
	 *Con la función fijos listan los eventos feriados que existen en la base de datos.
	 *@param int $ano Define como se hara la consulta
	 *
	 *@internal Con el parametro $ano, se dicide como hacer la consulta en la base de datos
	 *de los envetos feriados, si el parametro es igual a 0 se consultan los feriados fijos
	 *y si es otro número se consultan los feriados movibles para todos los años, luego de esto
	 *se guarda todo en una variable que luego se convertira a formato json
	 *
	 *@internal la función lista los feriados fijos, sin un año especifico, por lo que estos al mostrarse
	 *en el calendario solo se mostraran en el año actual.
	 */
	public function fijos($ano)
	{
		
		$eventos = array();
		if($ano==0){
		$fijos = $this->Feriados_Model->fijos($ano);
		foreach($fijos->result() as $fijo){
			$eventos[] = array(
				'id' => $fijo->feriado_id,
				'title' => $fijo->descripcion,
				'start' => date('Y').'-'.$fijo->mes.'-'.$fijo->dia,
				'allDay'=> true,
			);
			}
		}
		else {
			$fijos = $this->Feriados_Model->movibles();
			foreach($fijos->result() as $fijo){
				$eventos[] = array(
					'id' => $fijo->feriado_id,
					'title' => $fijo->descripcion,
					'start' => $fijo->ano.'-'.$fijo->mes.'-'.$fijo->dia,
					'allDay'=> true,
				);
			}
		}
		echo json_encode($eventos);
		exit();
	}
}
