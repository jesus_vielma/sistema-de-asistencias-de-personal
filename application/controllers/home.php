<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 *Controlador Home.
 *
 *El controlador Home, es controlador por defecto que utliza el framework para
 *acceder al sistema.
 *
 *@author Jesús Vielma
 *@package Controllers
 */
class Home extends Login {


    public function __construct()
    { 
	    parent::__construct();
    }
    
	/**
	 *Funcion index().
	 *
	 *Función que se encarga de hacer el llamado de la vista de inicio del sistema
	 */
    public function index()
    {
		$this->check_session();
	$data['titulo'] = 'CHAP - Inicio';
	$data['controlador'] = 'inicio';
	$this->load->view('base/cabecera',$data);
	$this->load->view('base/pagina_vacia');
	$this->load->view('base/pie');	
    }

}
													