<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('login.php');
/**
 * Controlador usuario.
 *
 *
 * El controlador Estación se encarga de realizar la funciones CRUD (Create, Read, Update, Delete)
 * para el modulo de estación. Así mismo contiene los métodos necesarios para obtener datos 
 * necesarios en los formularios.
 * 
 * @author Joeinny Osorio
 * @package Controllers
 */
class Usuario extends Login {
	
    public function __construct()
    { 
	    parent::__construct();
	    
	    $this->load->model('Usuario_Model');
	    $this->load->model('Empleado_Model');
	    $this->load->library('form_validation');
    }
    
    public function index()
    {
	    $this->check_session();	
	    $data['titulo'] = 'CHAP - Usuario';
	    $data['controlador'] = 'Listar';
	    $data['filas'] = $this->Usuario_Model->obtener_todos();    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('usuario/index');
	    $this->load->view('base/pie');
	    
	    
    }
	
    
    public function insertar($phase=1)
    {
	$this->check_session();	
	$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>','</div>');

	$data['Empleados']  = $this->Empleado_Model->obtener_todos();
	$data['titulo'] = 'CHAP - Usuario';
	$data['controlador'] = 'Insertar';
	
	if ($phase==1) 
	{
	    
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('usuario/insertar');
	    $this->load->view('base/pie');
	}
	
	
	else
	{
	    $this->form_validation->set_rules('usuario', 'usuario', 'required|is_unique[usuario.usuario]');
	    $this->form_validation->set_rules('contrasena', 'contrasena', 'required|md5');
	    $this->form_validation->set_rules('confcontrasena', 'Confirmacion de contraseña', 'required|matches[contrasena]');
	    $this->form_validation->set_rules('email','email','required|valid_email|is_unique[usuario.email]');
	    $this->form_validation->set_rules('empleado_id','Empleado','required');
	    	
	    $this->form_validation->set_message('valid_email', 'El campo %s  tiene que ser una dirección de correo electronica valida');    
	    $this->form_validation->set_message('required', 'El campo %s  es requerido');
	    $this->form_validation->set_message('matches', 'El campo %s  debe coincidir');
	    $this->form_validation->set_message('is_unique', 'El %s  ya esta en la base de datos, escoja otro');
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->insertar(1);
	    }
	    else
	    {
		
		if ($this->Usuario_Model->insertar_usuario($this->input))
		{
		    redirect('usuario/index', 'refresh');
		}
		else
		{
		    redirect('usuario/insertar', 'refresh');   
		}
	    }
            
	}
    }
    
    public function editar($id, $phase=1)
    {
	$this->check_session();	
	if ($phase==1)
	{
	    $data['fila'] = $this->Usuario_Model->obtener_por_id($id);
	    $data['titulo'] = 'CHAP - Usuario';
	    $data['controlador'] = 'Editar';
	    if($data['fila']==0)
	    {
		redirect('usuario/index','refresh');
	    }
	    $this->load->view('base/cabecera',$data);
	    $this->load->view('usuario/editar');
	    $this->load->view('base/pie');
	}
	else
	{
	    $this->form_validation->set_rules('usuario', 'Usuario', 'required');
	    $this->form_validation->set_rules('contrasena', 'Contraseña', 'required|md5');
	    $this->form_validation->set_rules('confcontrasena', 'Confirmacion de contraseña', 'required|matches[contrasena]');
	    $this->form_validation->set_rules('email','email','required|valid_email');
	    $this->form_validation->set_rules('empleado_id','Empleado','required');
	    //$this->form_validation->set_rules('niveles','Niveles','required');	
	    $this->form_validation->set_message('valid_email', 'El campo %s  tiene que ser una dirección de correo electronica valida');    
	    $this->form_validation->set_message('required', 'El campo %s  es requerido');
	    $this->form_validation->set_message('matches', 'El campo %s  debe coincidir');
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->editar(1);
	    }
	    else
	    {					
		if ($this->Usuario_Model->actualizar_usuario($this->input))
		{
		    redirect('usuario/index', 'refresh');
		}
		else
		{
		    redirect('usuario/editar/'.$id.'/1', 'refresh');   
		}
	    }
	}
    }
    
    public function borrar($id)
    {
		$this->check_session();	
        $this -> Usuario_Model -> borrar_usuario($id);
        redirect('usuario/index', 'refresh');
    }
}
