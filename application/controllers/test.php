<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {


    public function __construct()
    { 
	    parent::__construct();
	    $this->load->model('Empleado_Model');
	    $this->load->model('Horario_Model');
	    $this->load->model('Asignacion_Model');
	    $this->load->model('Horario_Model');
	    $this->load->library('form_validation');
    }
    
    public function index()
    {
	$this->load->view('test/test_index');
    }
    
    public function empleado_index()
    {
	    
	    $data['filas'] = $this->Empleado_Model->obtener_todos();
    
	    $this->load->view('test/empleado_index',$data);
    }
    
    public function insertar_insertar($phase=1)
    {
	$this->load->library('form_validation');
	$data['cargos']        = $this->Cargo_Model->obtener_todos();
	$data['departamentos'] = $this->Departamento_Model->obtener_todos();
	if ($phase==1) {
	    
	    $this->load->view('test/insertar_empleado',$data);
	}
	else
	{
	    $this->form_validation->set_rules('nombre', 'Nombre', 'required');
	    $this->form_validation->set_rules('cedula', 'Cedula', 'required');
	    $this->form_validation->set_rules('fecha_nac','Fecha de Nacimiento','required');
	    $this->form_validation->set_rules('codigo_empl','Codigo del empleado','required');
   	    $this->form_validation->set_rules('cargo_id','Cargo','required');
	    $this->form_validation->set_rules('departamento_id','Departamento','required');
	    $this->form_validation->set_rules('estacion_id','Estacion','required');
	    $this->form_validation->set_message('required', 'El campo %s  es requerido');
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->load->view('test/insertar_empleado',$data,'refresh');
	    }
	    else
	    {
		
		if ($this->Empleado_Model->insertar_empleado($this->input))
		{
		    redirect('empleado/index', 'refresh');
		}
		else
		{
		    redirect('empleado/insertar', 'refresh');   
		}
	    }
            
	}
    }
    
    public function editar_empleado($id, $phase=1)
    {
    
	if ($phase==1)
	{
	    $data['fila'] = $this->Empleado_Model->obtener_por_id($id);
	    $data['cargos'] = $this->Cargo_Model->obtener_todos();
	    $data['departamentos'] = $this->Departamento_Model->obtener_todos();
	    
	    if($data['fila']==0)
	    {
		redirect('empleado_index','refresh');
	    }
	    
	    $this->load->view('test/editar_empleado',$data);
	}
	else
	{
	    if ($this->Empleado_Model->actualizar_empleado($this->input))
	    {
		redirect('empleado/index', 'refresh');
	    }
	    else
	    {
		redirect('empleado/editar_empleado/'.$id.'/1', 'refresh');   
	    }
	}
    }
    
    public function detalle_empleado($id)
    {
	$data['fila']    =  $this->Empleado_Model->obtener_empleado_con_estac_depto_cargo($id);
	$this->load->view('test/detalle_empleado',$data);
    }
    
    public function get_code()
    {
	if($this->input->post('name'))
	{
	    $name = $this->input->post('name');
	    $lastid = $this->Empleado_Model->lastid();
	    $nombre= explode(" ",$name);
	    
	    if(($nombre[0]!="")&&(isset($nombre[1]))&&(isset($nombre[2]))&&(isset($nombre[3])))
	    {
	    $ini = strtoupper($nombre[0]{0});
	    $ini = $ini.strtoupper($nombre[1]{0});
	    $ini = $ini.strtoupper($nombre[2]{0});
	    $ini = $ini.strtoupper($nombre[3]{0});
	    }
	    else 
		{if (($nombre[0]!="")&&(isset($nombre[1]))&&(isset($nombre[2])))
		{
		$ini = strtoupper($nombre[0]{0});
		$ini = $ini.strtoupper($nombre[1]{0});
		$ini = $ini.strtoupper($nombre[2]{0});
		}
		else 
		    {if (($nombre[0]!="")&&(isset($nombre[1])))
		    {
		    $ini = strtoupper($nombre[0]{0});
		    $ini = $ini.strtoupper($nombre[1]{0});
		    }
		    else 
		    {//echo "El nombre debe contener nombre y apellido";
		    $ini= "";
		    ?>
		    <script>alert("Debe ingresar el primer apellido");
		    $("#nombre").focus();
		    </script>
		    <?php
		    }
		    }
		}
	    $codigo_empl=$ini.''.($lastid[0]->id+1);
	    ?>
		<input type="text" value="<?=$codigo_empl?>" id="codigo_empl1" name="codigo_empl" readonly>
	    <?php
	}
    }
    
    public function borrar_empleado($id)
    {
        $this -> Empleado_Model -> borrar_empleado($id);
        redirect('empleado/index', 'refresh');
    }
  public function cargo_index()
    {
	    
	    $data['filas'] = $this->Cargo_Model->obtener_todos();
    
	    $this->load->view('test/cargo_index',$data);
    }
    
    public function cargo_insertar($phase=1)
    {
	$this->load->library('form_validation');
	if ($phase==1) {
	    
	    $this->load->view('test/insertar_cargo');
	}
	else
	{
	    $this->form_validation->set_rules('cargo', 'Cargo', 'required');
	    $this->form_validation->set_rules('descripcion', 'Descripción', 'required');
	    	    
	    $this->form_validation->set_message('required', 'El campo %s  es requerido');
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->load->view('test/insertar_cargo','refresh');
	    }
	    else
	    {
		
		if ($this->Cargo_Model->insertar_cargo($this->input))
		{
		    redirect('cargo/index', 'refresh');
		}
		else
		{
		    redirect('cargo/insertar', 'refresh');   
		}
	    }
            
	}
    }
    
    public function cargo_editar($id, $phase=1)
    {
    
	if ($phase==1)
	{
	    $data['fila'] = $this->Cargo_Model->obtener_por_id($id);
	    if($data['fila']==0)
	    {
		redirect('cargo_index','refresh');
	    }
	    $this->load->view('test/editar_cargo',$data);
	}
	else
	{
	    if ($this->Cargo_Model->actualizar_cargo($this->input))
	    {
		redirect('cargo/index', 'refresh');
	    }
	    else
	    {
		redirect('cargo/editar/'.$id.'/1', 'refresh');   
	    }
	}
    }

    public function borrar($id)
    {
        $this -> Cargo_Model -> borrar_cargo($id);
        redirect('cargo/index', 'refresh');
    }
 public function usuario_index()
    {
	    
	    $data['filas'] = $this->Usuario_Model->obtener_todos();
    
	    $this->load->view('test/usuario_index',$data);
    }
    
    public function usuario_insertar($phase=1)
    {
	
	$data['empleados'] = $this->Empleado_Model->obtener_todos();
	if ($phase==1) {
	    
	    $this->load->view('test/insertar_usuario',$data);
	}
	else
	{
	    $this->form_validation->set_rules('usuario', 'usuario', 'required|is_unique[usuario.usuario]');
	    $this->form_validation->set_rules('contrasena', 'contrasena', 'required|md5');
	    $this->form_validation->set_rules('confcontrasena', 'Confirmacion de contraseña', 'required|matches[contrasena]');
	    $this->form_validation->set_rules('email','email','required|valid_email');
	    $this->form_validation->set_rules('empleado_id','Empleado','required');
	    	
	    $this->form_validation->set_message('valid_email', 'El campo %s  tiene que ser una dirección de correo electronica valida');    
	    $this->form_validation->set_message('required', 'El campo %s  es requerido');
	    $this->form_validation->set_message('matches', 'El campo %s  debe coincidir');
	    $this->form_validation->set_message('is_unique', 'El %s  ya esta en la base de datos por escoja otro');
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->load->view('test/insertar_usuario',$data,'refresh');
	    }
	    else
	    {
		
		if ($this->Usuario_Model->insertar_usuario($this->input))
		{
		    redirect('usuario/index', 'refresh');
		}
		else
		{
		    redirect('usuario/insertar_usuario', 'refresh');   
		}
	    }
            
	}
    }
    
    public function usuario_editar($id, $phase=1)
    {
	$data['fila'] = $this->Usuario_Model->obtener_por_id($id);
	if ($phase==1)
	{
	    if($data['fila']==0)
	    {
		redirect('usuario/index','refresh');
	    }
	    $this->load->view('test/editar_usuario',$data);
	}
	else
	{
	    $this->form_validation->set_rules('usuario', 'Usuario', 'required');
	    $this->form_validation->set_rules('contrasena', 'Contraseña', 'required|md5');
	    $this->form_validation->set_rules('confcontrasena', 'Confirmacion de contraseña', 'required|matches[contrasena]');
	    $this->form_validation->set_rules('email','email','required|valid_email');
	    $this->form_validation->set_rules('empleado_id','Empleado','required');
	    	
	    $this->form_validation->set_message('valid_email', 'El campo %s  tiene que ser una dirección de correo electronica valida');    
	    $this->form_validation->set_message('required', 'El campo %s  es requerido');
	    $this->form_validation->set_message('matches', 'El campo %s  debe coincidir');
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->load->view('test/editar_usuario',$data,'refresh');
	    }
	    else
	    {
		if ($this->Usuario_Model->actualizar_usuario($this->input))
		{
		    redirect('usuario/index', 'refresh');
		}
		else
		{
		    redirect('usuario/editar/'.$id.'/1', 'refresh');   
		}
	    }
	}
    }
    
    public function usuario_borrar($id)
    {
        $this -> Usuario_Model -> borrar_usuario($id);
        redirect('usuario/index', 'refresh');
    }
    public function asignacion_index()
    {
	    
	    $data['filas'] = $this->Asignacion_Model->obtener_todos();
	    $this->load->view('test/asignacion_index',$data);
    }
    
    public function insertar_asignacion($phase=1)
    {
	$this->load->library('form_validation');
	$data['empleados'] = $this->Empleado_Model->obtener_todos();
	$data['horarios']   = $this->Horario_Model->obtener_todos();
	if ($phase==1) {
	    
	    $this->load->view('test/insertar_asignacion',$data);
	}
	else
	{
	    $this->form_validation->set_rules('fecha', 'Fecha', 'required');
	    $this->form_validation->set_rules('pernocta', 'Pernocta', 'required');
	    $this->form_validation->set_rules('trabajo','Trabajo','required');
   	    $this->form_validation->set_rules('dia','Día','required');
	    $this->form_validation->set_rules('turno','Turno','required');
	    $this->form_validation->set_rules('empleado_id','Empleado','required');
	    $this->form_validation->set_rules('horario_id','Horario','required');
	    $this->form_validation->set_message('required', 'El campo %s  es requerido');
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->load->view('test/insertar_asignacion',$data,'refresh');
	    }
	    else
	    {
		
		if ($this->Asignacion_Model->insertar_asignacion($this->input))
		{
		    redirect('asignacion/index', 'refresh');
		}
		else
		{
		    redirect('asginacion/insertar_asignacion', 'refresh');   
		}
	    }
            
	}
    }
    
    public function editar_asignacion($id, $phase=1)
    {
    
	if ($phase==1)
	{
	    $data['fila'] = $this->Asignacion_Model->obtener_por_id($id);
	    
	    if($data['fila']==0)
	    {
		redirect('asignacion/index','refresh');
	    }
	    
	    $this->load->view('test/editar_asignacion',$data);
	}
	else
	{
	    if ($this->Asignacion_Model->actualizar_asignacion($this->input))
	    {
		redirect('asignacion/index', 'refresh');
	    }
	    else
	    {
		redirect('asignacion/editar/'.$id.'/1', 'refresh');   
	    }
	}
    }
    
    public function detalle_asignacion($id)
    {
	$data['fila']    =  $this->Asignacion_Model->obtener_por_id($id);
	$this->load->view('test/detalle_asignacion',$data);
    }
    
    public function borrar_asignacion($id)
    {
        $this -> Asignacion_Model -> borrar_asignacion($id);
        redirect('horario', 'refresh');
    }
    
    public function get_date()
    {
	if($this->input->post('date'))
	{
	    $date = $this->input->post('date');
	    
	    $fecha = ucfirst(strftime('%A',strtotime($date)));
	}
	
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
	{
		print_r(utf8_encode($fecha));
	}
	else
		{
		    print_r($fecha);
		}
	
    }
    
        public function horario_index()
    {
	    
	    $data['filas'] = $this->Horario_Model->obtener_todos();
	    $this->load->view('test/horario_index',$data);
    }
    
    public function insertar_horario($phase=1)
    {
	$this->load->library('form_validation');
	
	if ($phase==1) {
	    
	    $this->load->view('test/insertar_horario');
	}
	else
	{
	    $this->form_validation->set_rules('hora_entrada', 'Hora de entrada', 'required');
	    $this->form_validation->set_rules('hora_salida', 'Hora de salida', 'required');
	    $this->form_validation->set_rules('descripcion','descripcion','required');
   	    $this->form_validation->set_rules('tipo','Tipo','required');
	    $this->form_validation->set_message('required', 'El campo %s  es requerido');
	    
	    if($this->form_validation->run()== FALSE )
	    {
		$this->load->view('test/insertar_horario','refresh');
	    }
	    else
	    {
		
		if ($this->Horario_Model->insertar_horario($this->input))
		{
		    redirect('horario/index', 'refresh');
		}
		else
		{
		    redirect('horario/insertar_horario', 'refresh');   
		}
	    }
            
	}
    }
    
    public function editar_horario($id, $phase=1)
    {
    
	if ($phase==1)
	{
	    $data['fila'] = $this->Horario_Model->obtener_por_id($id);
	    
	    if($data['fila']==0)
	    {
		redirect('horario/index','refresh');
	    }
	    
	    $this->load->view('test/editar_horario',$data);
	}
	else
	{
	    if ($this->Horario_Model->actualizar_horario($this->input))
	    {
		redirect('horario/index', 'refresh');
	    }
	    else
	    {
		redirect('horario/editar/'.$id.'/1', 'refresh');   
	    }
	}
    }
    
    public function detalle_horario($id)
    {
	$data['fila']    =  $this->Horario_Model->obtener_por_id($id);
	$this->load->view('detalle_horario',$data);
    }
    
    public function borrar_horario($id)
    {
        $this -> Horario_Model -> borrar_horario($id);
        redirect('horario', 'refresh');
    }
}